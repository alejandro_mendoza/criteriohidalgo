/*
 ================== Ordenar slider principal ===============
 */
var orderMainSlider = function ()
{
    $("#sortable").sortable({
        update: function () {
            var neworder = [];
            $.each($('#sortable .sortable-ch'), function (i, val) {
                neworder[i] = $(this).attr("id");
            });

            $.post("/mcPanel/slider/setOrder",{
                'order': neworder,
                "_token" : $('input[name="_token"]').val()
            }, function (data) {
                console.log(data);
            });
        }
    });
};
/*
 ================== Eliminar Slider ==================
 */
function deleteSlider(item)
{
    var formMultiSelect = $("#form-multi-select");
    // Establecemos el action del formulario
    formMultiSelect.prop('action', $(item).data("href"));

        var r = confirm("Confirme la accion: " + $(item).data("action"));
        if (r) {
            // enviamos el formulario
            formMultiSelect.submit();
        }
    return false;
};

///////
// Slider principal -EditExpress de Url
//////
var editQuit	=	$('#renderSliderMain #sortable'),
e_e_item,                                   // id del elemento
e_e_element,                                // entidad  ej. user
e_e_url,
boxEdit = $('#inputSuccess2'),
e_e_source = $('#toSource').data('source'); // entidad  ej. 1 => user
editQuit.on('dblclick', '.editQuit', function(){
        e_e_url = $(this).data("url");    // obtenemos la url del elemento seleccionado
        e_e_item = $(this).data('item'); // obtenemos el id del item
        e_e_element = $(this);          // item
        boxEdit.val(e_e_url);                      // Asignamos la url a la caja
        boxEdit.detach().appendTo($(this).parent().parent());        // Cambiamos de posicion a la caja
        boxEdit.css('visibility', 'visible');   // se hace visible
        boxEdit.focus();
});

boxEdit.focusout(function(){                // # cuando se quita el cursor de la caja
    $(this).css('visibility', 'hidden');
    $(this).css('border-color', '#3c763d');
    var to = $('#toSource');                // destino
    $(this).detach().appendTo(to);          // Cambiamos de posicion a la caja
    $.ajax({
        data:{  "item"   : e_e_item,
                "url"    : $(this).val(),
                "_token" : $('input[name="_token"]').val()},
        url: '/mcPanel/slider/update',
        dataType: 'json',
        type: 'post',
        success: function(response){
            if(response.error != ''){
                $('#msjError').detach().appendTo($(e_e_element).parent().parent().parent());
                    $('#msjError').fadeIn();
                    $('#msjError').html(response.error);
                    $('#msjError').fadeOut(5000);
            }
            else{
                $(e_e_element).attr('src',response.image);
                $(e_e_element).data("url",response.url);
                $(this).val('');
            }
        },
        error :function( error ) {
            if( error.status === 422 ) {
                $errors = error.responseJSON; 
                $.each($errors, function( key, value ) {
                    $('#msjError').detach().appendTo($(e_e_element).parent().parent().parent());
                    $('#msjError').fadeIn();
                    $('#msjError').html(value);
                    $('#msjError').fadeOut(5000);
                });
            }
        }
    });
});

boxEdit.each(function(){                    // # inicializa la caja en invisible
    $(this).css('visibility', 'hidden');
    $(this).css('border-color', '#3c763d');
});
var noSubmit = function()                   // # evitamos el submit del form
{
  boxEdit.keypress(function(){
      $('.form').keypress(function(e){
            if(e.which == 13)
                return false;
        });
        boxEdit.keypress(function(e){
            if(e.which == 13)
                return false;
        });
    });
};
/*
 * |||||||||||   F I N   D E    F U N C I O N E S   D E L   E D I T O R     E X P R E S S  |||||||||||||||||||||
 */

///////
// Slider principal -Mostrar demos
//////
$('#showSlider').on('click',function(){   
    var demo = $('#selectSlider').val();
    $('#img-demo').attr('src','/Backend/images/esquema'+demo+'.png');
    $('#img-demo').removeClass('hidden');
});

///////
// Slider principal -Agregar primer slider
//////
var select = '';
$('#addSlider').on('click',function(){   
    select = $('#selectSlider').val();
    newSlider();
});

///////
// Slider principal -Agregar slider
//////
 function newSlider(){
    var type = select;
     if(select == '')
        type = $('#typeSlider').val();
    $.ajax({
        data:{
            "_token"    : $('input[name="_token"]').val(),
            'id'        : type
        },
        url: '/mcPanel/slider/view',
        dataType: 'json',
        type: 'get',
        success: function(response){
            //select = '';
            $('#addSlider').hide();
            $('#type').html('Plantilla ' + response.type);
            $('#typeSlider').val(response.type);
            $('#msjSuccess').fadeIn('slow');
            $('#renderSliderMain #sortable').append('<div id="'+response.id+'" class="ui-sortable-handle sortable-ch" >'+response.view+'</div>');
            $('#msjSuccess').fadeOut(7000);
        }
    }); 
 }
 
 ///////
// Slider principal -Seleccionar todos los sliders
//////
var checkSelectAllS  = $(".multi-select-all-s");
var checkSelectedS   = $(".select-checkbox-s");
var multiSelectAllSlider = function(event)
{
    var selected = $(".select-checkbox-s");
    selected.each(function () {
        $(this).prop('checked',checkSelectAllS.prop("checked"));
    });
};

///////
// Slider principal -Eliminar sliders seleccionados
//////
var multiDelete   = $("#MultiDelete");
multiDelete.click( function ()
{
    var formMultiSelect = $("#form-multi-select");
    // Asignamos los nuevos valores
    var selected = $(".select-checkbox-s:checked");

    if (selected.length > 0) {
        if (confirm("Desea eliminar los " + selected.length + " elementos seleccionados?")) {
            var elements = [];
            selected.each(function () {
                elements.push(this.value);
                //formMultiSelect.append(input);
            });
            // enviamos el formulario
            formMultiSelect.prop('action', 'slider/eliminar/'+elements);
            formMultiSelect.submit();
        }
    }
    return false;
});