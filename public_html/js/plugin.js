var goTop           = $('#footerMenu #goTop');
var calendar        = $('.datePicker');
var customInput     = $('.customInput');
var customButton    = $('.customButton');


goTop.on('click', function(){
    $("html, body").animate({ scrollTop: 0 }, 1000);
    return false;
});


if (calendar.length) {
    calendar.datepicker({
        format: 'dd/mm/yyyy',
        defaultViewDate: {
            year: 2015,
            month: 0,
            day: 1
        }
    });
}

//SELECT PERSONALIZADO
if (customInput.length) {
    $('.customSelect').delegate( customInput, 'click', function(){
        console.log($(this));
        var container = $(this).closest('.customSelect');
        var options = container.find('option');
        
        $(this).find('ul').fadeIn();
        
        container.delegate('li', 'click', function(){
            var value = $(this).data('value');
            options.each (function(){
                if ($(this).val() == value) {
                    console.log($(this).text());
                    $(this).attr('selected','selected');
                    $(this).closest('.customSelect').find('.customInput').text($(this).text());
                }
            })
        })
        
        
        $(this).find('ul').on('mouseleave', function(){
            $(this).fadeOut('fast');
        });
    })
}

//BOTÓN ADJUNTAR PERSONALIZADO 
if(customButton.length){
    customButton.children('span').on('click', function(){
       $('input[name=image]').trigger('click');
    })
    $('input[name=image]').on('change', function(){
        var imagePath = ($(this).val()).split('\\');
        $('#imageFile').text(imagePath[imagePath.length-1]);
    })
}


// Encuesta  -Votar
$('.btnVotar').click(function(){
    var select;
    $('.options').each(function ()
    {
       if($(this).is(':checked')) 
           select = $(this).attr('id');
    });    
    $.ajax({
        data:{
            "_token"    : $('input[name="_token"]').val(),
            'id'        : select
        },
        url: '/survey-votar',
        //dataType: 'json',
        type: 'get',
        before: function(){
            $('#renderSurvey').html('Cargando...');
        },
        success: function(response){
            $('#renderSurvey').html(response);
        }
    });    
});

/*
 ================== Copy Paste ==================
 */
var copyPaste = function(){
    $("body").bind('copy', function (e) {
        if (typeof window.getSelection == "undefined") return; //IE8 or earlier...

        var body_element = document.getElementsByTagName('body')[0];
        var selection = window.getSelection();

        //if the selection is short let's not annoy our users
        //if (("" + selection).length < 30) return;

        //create a div outside of the visible area
        //and fill it with the selected text
        var newdiv = document.createElement('div');
        newdiv.style.position = 'absolute';
        newdiv.style.left = '-99999px';
        body_element.appendChild(newdiv);
        //newdiv.appendChild(selection.getRangeAt(0).cloneContents());

        //we need a <pre> tag workaround
        //otherwise the text inside "pre" loses all the line breaks!
        if (selection.getRangeAt(0).commonAncestorContainer.nodeName == "PRE") {
            newdiv.innerHTML = "<pre>" + newdiv.innerHTML + "</pre>";
        }

        newdiv.innerHTML += "<p> El siguiente contenido no se puede copiar por derechos de autor pero si quieres te dejamos un lindo lorem impsum: </p><br/><p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas, las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>";

        selection.selectAllChildren(newdiv);
        window.setTimeout(function () { body_element.removeChild(newdiv); }, 200);
    });
}

var redirectHome = function(){
    $(location).attr('href',"/");

}

/*
 ================== Inicio de funciones ==================
 */
$(document).ready(function() {
    //copyPaste();
    
    
    $('.dropdown-toggle').on('mouseenter', function() {
        $(this).parent('li').siblings('li').removeClass('open');
        $(this).parent('li').addClass("open");
        $('.navbar-nav').on('mouseleave', function(){
            $(this).children('li').removeClass('open');
        })
        $('.navbar-nav').delegate('.dropdown-menu', 'mouseleave', function(){
            $(this).parent('li').removeClass('open');
        });
    });
    
});

/*
 ================== Cargar mas ==================
 */
var limit = 0;
function moreAds(url,skip,count){
    if(skip == 0)
        limit = 9;
    else
        limit = skip;
    console.log(url);
        $.ajax({
            data:{
                "_token" : $('input[name="_token"]').val(),
                "skip": limit
            },
            url: url+limit,
            type: 'post',
            success: function(response){
                $('.renderMore').append(response);
                var size = $('.itemAd').size();

                if(size >= count )
                    $('#loadMoreButtonContainer').html('');
                else
                    $('#btnMore').html('<a  onclick="moreAds(\''+url+'\','+ size +','+ count +');" >CARGAR MÁS</a>');
                console.log('count: '+count+' size: '+size);
            }
        });
}


/*
 ================== Cargar mas minuto a minuto ==================
 */
var limitM;
function moreMinuteByMinute(url,skip,count){
    limitM = (skip == 0)?6:skip;
    console.log(url);
    $.ajax({
        data:{
            "_token" : $('input[name="_token"]').val(),
            "skip": limitM
        },
        url: url+limitM,
        type: 'post',
        success: function(response){
            $('#minuteByMinuteContainer').append(response);
            var size = $('#minuteByMinuteContainer>a').size();
            var time = $('#minuteByMinuteContainer>a:last>input.lastTime').data('lasttime');

            if(size >= count )
                $('.loadMoreButtonContainer').html('');
            else
                $('.btnMoreMinute').html('<a  onclick="moreMinuteByMinute(\''+url+'\','+ time +','+ count +');" >VER MÁS</a>');
            console.log('count: '+count+' size: '+size);
        }
    });
}

/*
 ================== Api lector de notas ==================
 */
function playLector(){
    var _api_link = 'http://api.voicerss.org/?key=5d4ea1b448004b7fa6df011fbdeec6fa&hl=es-mx&';
    var _text = $('#lectorApi').text();


    _text = _text.replace(/[^a-zA-Z 0-9.]+/g,' ');

    _text = _text.substring(0, 1419);
    document.getElementById("audioPlayer").src = _api_link + "src=" + _text;

}
function stopLector(){
    var _api_link = 'http://api.voicerss.org/?key=5d4ea1b448004b7fa6df011fbdeec6fa&hl=es-mx&';
    var _text = $('#lectorApi').html();
    document.getElementById("audioPlayer").src = '';

}

/*
 ================== Cargar categorias, publicar anuncios ==================
 */
$('.changeSelect').click(function (){
    var category = $(this).data('value');
        $.ajax({
            data:{
                "_token" : $('input[name="_token"]').val(),
                "id": category
            },
            url: '/anuncios-clasificados/changeselect',
            //dataType: 'json',
            type: 'post',
            success: function(response){
                $('#renderSubCategories').html(response);
            }
        });
});
