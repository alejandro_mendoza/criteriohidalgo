var start = function(){
    
    //CARROUSEL PRINCIPAL
    $('#carrousel').carouFredSel({
        responsive          : 'true',
        width               : '100%',
        auto                : false,
        prev                : '#prev',
        next                : '#next',
        mousewheel          : true,       
        swipe: {
            onMouse         : true,
            onTouch         : true
        },
        scroll:{
            items           : 1,
            duration        : 1000
        },
        items: {
            //VALOR NECESARIO 
            width           : 1,
            visible: {
                min: 1,
                max: 1
            }
        }
    });
    
    
    //CARROUSEL DESTACADOS
    $('#newCarrousel').carouFredSel({
        responsive          : 'true',
        width               : '100%',
        auto                : false,
        prev                : '#prevNew',
        next                : '#nextNew',
        mousewheel          : true,
        swipe: {
            onMouse         : true,
            onTouch         : true
        },
        scroll:{
            items           : 1,
            duration        : 1000
        },
        items: {
            //VALOR NECESARIO
            width           : 1,
            visible: {
                min: 1,
                max: 1
            }
        }
    });

    $('#ticketCarrousel').carouFredSel({
        responsive          : 'true',
        width               : '100%',
        auto                : false,
        prev                : '#prevTicket',
        next                : '#nextTicket',
        mousewheel          : true,
        swipe: {
            onMouse         : true,
            onTouch         : true
        },
        scroll:{
            items           : 1,
            duration        : 1000
        },
        items: {
            //VALOR NECESARIO
            width           : 1,
            visible: {
                min: 1,
                max: 1
            }
        }
    });
    

    
    //CARROUSEL DEPORTES
    $('.sportCarousel').carouFredSel({
        responsive          : 'true',
        width               : '100%',
        auto                : false,
        prev                : '#prevSport',
        next                : '#nextSport',
        mousewheel          : true,       
        swipe: {
            onMouse         : true,
            onTouch         : true
        },
        scroll:{
            items           : 1,
            duration        : 1000
        },
        items: {
            //VALOR NECESARIO 
            width           : 1,
            visible: {
                min: 1,
                max: 1
            }
        }
    });

    //CARROUSEL TICKET
    $('.ticketCarousel').carouFredSel({
        responsive          : 'true',
        width               : '100%',
        auto                : false,
        prev                : '#prevTicket2',
        next                : '#nextTicket2',
        mousewheel          : true,
        swipe: {
            onMouse         : true,
            onTouch         : true
        },
        scroll:{
            items           : 1,
            duration        : 1000
        },
        items: {
            //VALOR NECESARIO
            width           : 1,
            visible: {
                min: 1,
                max: 1
            }
        }
    });
    
}

$(document).on('ready', start);
