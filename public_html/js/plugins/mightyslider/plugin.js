var start = function (){
    var isTouch = !!('ontouchstart' in window),
    clickEvent = isTouch ? 'tap' : 'click';

    (function(){
        // Global slider's DOM elements
        var $gallery = $('#videoGallery'),
            $parent = $('.mightyslider_modern_skin', $gallery),
            $tabs = $('.tabs ul', $gallery),
            $frame = $('.frame', $gallery);

        // Calling mightySlider via jQuery proxy
        $frame.mightySlider({
            speed: 1000,
            autoScale: 1,
            easing: 'easeOutExpo',

            // Navigation options
            navigation: {
                slideSize: '100%'
            },

            // Thumbnails options
            thumbnails: {
                horizontal: 0,
                preloadThumbnails: 0,
                activateOn: clickEvent,
                thumbnailsBar: $tabs,
                thumbnailBuilder:
                    function (index, thumbnail) {
                        return '<li><h3>' + this.slides[index].options.title + '</h3>by ' + this.slides[index].options.owner + '</li>';
                    }
            },

            // Commands options
            commands: {
                buttons: 1
            }
        },

        // Register callbacks to the events
        {
            // Register mightySlider :active event callback
            active: function(name, index){
                var skin = this.slides[index].options.skin || '';
                $gallery.removeClass('black').addClass(skin);
            }
        });
    })();    
}

$(document).on('ready',start);