var start = function(){
    
      $('#sliderGallery').royalSlider({
        fullscreen: {
          enabled: true,
        },
        controlNavigation: 'thumbnails',        
        autoScaleSliderHeight: 600,
        autoScaleSlider: true,
        loop: false,
        imageScaleMode: 'fit',
        navigateByClick: true,
        numImagesToPreload:2,
        keyboardNavEnabled: false,
        fadeinLoadedSlide: true,
        globalCaption: true,
        globalCaptionInside: false,
        thumbs: {
          appendSpan: true,
          firstMargin: true,
          paddingBottom: 4,
        }
      });
}

$(document).on('ready', start);