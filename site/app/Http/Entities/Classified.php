<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Classified extends Model {

    protected $fillable = ['name','slug'];
    protected $table    = 'c_classified';
    public $timestamps  = false;
    /*
     * ================ Sub categorias ================
     */
    public function sub_categories()
    {
        return $this->hasMany('App\Http\Entities\Sub_Classified','category_id','id');
    }
}
