<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model {

    protected $fillable = ['sentence','current'];
    
    public function options() {
        return $this->hasMany('App\Http\Entities\Option');
    }
    
}
