<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Sub_Classified extends Model {

    protected $fillable = ['name','slug','category_id'];
    protected $table    = 'c_sub_classified';
    public $timestamps  = false;

    public function category()
    {
        return $this->belongsTo('App\Http\Entities\Classified');
    }
    
    /*
     * ================== Relacion con sus elementos ==================
     */
    public function elements()
    {
        return $this->hasMany('App\Http\Entities\Ads', 'sub_category_id','id');
    }
}
