<?php
namespace App\Http\Entities;

//use App\Http\Traits\EntityCommonScopes;

class Type extends \Eloquent {
  //  use EntityCommonScopes;
    protected $guarded = ['id'];

    public function modules()
    {
        return $this->hasMany('App\Http\Entities\Module');
    }
}