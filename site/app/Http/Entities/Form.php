<?php
namespace App\Http\Entities;


use App\Http\Backend\Traits\EntityCommonScopes;

class Form extends \Eloquent {
    protected $guarded = ['id'];

    use EntityCommonScopes;

    public function inputs() {
        return $this->hasMany('App\Http\Entities\Input');
    }

    public function responses() {
        return $this->hasMany('App\Http\Entities\Response');
    }

    public function module() {
        return $this->morphMany('App\Http\Entities\Module','modulable');
    }

    public function getMainEmailField() {
        return $this->inputs()->where('type','email')->orderBy('position','DESC')->first();
    }

    public function getMainEmailValue($responseId) {
        $email = $this->getMainEmailField();
        if($email) {
            $response = $this->responses()->find($responseId);
            if($response) {
                $values = json_decode($response->values);
                return $values->{"$email->slug"};
            }
        }
        return '';
    }
}