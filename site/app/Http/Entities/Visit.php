<?php namespace App\Http\Entities;

class Visit extends \Eloquent
{
    protected $fillable = ['article_id', 'cookie', 'date_expirate'];
    public $timestamps  = false;

}