<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Http\Backend\Helpers\Slug\Slug;

class Statics extends Model {

    protected $fillable = ['name','description','slug'];
    public $timestamps  = false;
    protected $table    = 'statics';
    
    /*
     * ================== Relacion con sus elementos ==================
     */
    public function elements()
    {
        return $this->hasMany('App\Http\Entities\Statics_element', 'static_id', 'id');
    }
    
    /*
     * ================ Formato para slug ================
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Slug::filter($value);
    }
    
}
