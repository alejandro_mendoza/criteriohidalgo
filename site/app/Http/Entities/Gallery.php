<?php namespace App\Http\Entities;

class Gallery  extends \Eloquent
{
    protected $fillable = ['name','position','active','directory'];

    public function imageable()
    {
        return $this->morphTo();
    }

    /*
     * =============== Elemnetos de una Galería ===============
     */
    public function elements()
    {
        return $this->HasMany('App\Http\Entities\GalleryElements', 'gallery_id');
    }
}