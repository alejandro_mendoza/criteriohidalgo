<?php namespace App\Http\Entities;

class Category extends \Baum\Node
{
    protected $table        = 'categories';
    protected $parentColumn = 'parent_id';
    protected $leftColumn   = 'lft';
    protected $rightColumn  = 'rgt';
    protected $depthColumn  = 'depth';
    protected $orderColumn  = 'title';

    protected $guarded      = ['id', 'parent_id', 'lft', 'rgt', 'depth'];
    protected $fillable     = ['title', 'slug', 'image', 'color', 'description', 'free', 'active','params'];

    /*
     * ============== Categoría padre ==============
     */
    public function parent()
    {
        return $this->belongsTo('App\Http\Entities\Category', 'parent_id');
    }

    /*
     * ============== Modulos de una categoría ==============
     */
    public function modules()
    {
        return $this->hasMany('App\Http\Entities\Module');
    }
    
    /*
     * =============== Articulos de una categoria  ===============
     */
    public function article()
    {
        return $this->HasMany('App\Http\Entities\Article');
    }

    /*
     * ============= Categorías asignadas a usuarios =============
     */
    public function users()
    {
        return $this->morphedByMany('App\Http\Entities\User', 'categorizable');
    }

}
