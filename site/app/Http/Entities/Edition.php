<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Edition extends Model {

    protected $fillable = ['title','image','date_publishing','active', 'category_id'];


    public function category() {
        return $this->belongsTo('App\Http\Entities\Category', 'category_id');
    }
}
