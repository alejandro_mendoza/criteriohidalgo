<?php
namespace App\Http\Entities;


class Input extends \Eloquent {
    protected $guarded = ['id'];

    public function form() {
        return $this->belongsTo('App\Http\Entities\Form');
    }
}