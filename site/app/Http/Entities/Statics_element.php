<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Statics_element extends Model {

    protected $fillable = ['title','text','link','image','static_id'];
    public $timestamps  = false;
    
    public function statics()
    {
        return $this->belongsTo('App\Http\Entities\Statics');
    }
}
