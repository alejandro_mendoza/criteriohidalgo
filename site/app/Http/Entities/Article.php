<?php namespace App\Http\Entities;

use App\Http\Frontend\Traits\EntityCommonScopes;

class Article extends \Eloquent
{
    use EntityCommonScopes;

    protected $table    = 'articles';
    protected $guarded  = ['id'];

    /*
     * ============== Categoría ==============
     */
    public function category()
    {
        return $this->belongsTo('App\Http\Entities\Category');
    }

    /*
     * ============== Autor ==============
     */
    public function authors()
    {
        return $this->belongsTo('App\Http\Entities\Author', 'author_id');
    }

    /*
     * ============== Ids ==============
     */
    public function ids()
    {
        return $this->morphMany('App\Http\Entities\Id', 'idssable');
    }

    /*
     * ============== Tags ==============
     */
    public function tags()
    {
        return $this->morphToMany('App\Http\Entities\Tag', 'taggable');
    }

    /*
     * ============== Galería ==============
     */
    public function galleries()
    {
        return $this->morphMany('App\Http\Entities\Gallery', 'imageable');
    }

    /*
     * ================ Formato para nombre ================
     */
    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = empty($value) ? substr($this->attributes['intro'], 0, 150) : $value;
    }

}
