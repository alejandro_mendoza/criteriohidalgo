<?php namespace App\Http\Entities;

class Tag extends \Eloquent
{
    protected $fillable = ['value','slug'];
    public $timestamps  = false;

    /*
     * ============= Artículos =============
     */
    public function articles()
    {
        return $this->morphedByMany('App\Http\Entities\Article', 'taggable');
    }
}