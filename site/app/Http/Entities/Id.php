<?php namespace App\Http\Entities;

class Id extends \Eloquent
{
    protected $fillable = ['article_id'];
    public $timestamps  = false;

    public function idssable()
    {
        return $this->morphTo();
    }

    /*
     * =============== Articulos  ===============
     */
    public function article()
    {
        return $this->belongsTo('App\Http\Entities\Article');
    }
}