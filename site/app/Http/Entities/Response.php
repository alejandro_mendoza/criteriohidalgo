<?php namespace App\Http\Entities;

class Response extends \Eloquent {
    protected $fillable = ['form_id','values','seen'];

    public function form() {
        return $this->belongsTo('App\Http\Entities\Form');
    }
    
    /*
     * ================ Asignar 1 a mensaje ================
     */
    public function setValuesAttribute($value)
    {
        $this->attributes['values'] = $value;
        $this->attributes['seen'] = 1;
    }
    
}