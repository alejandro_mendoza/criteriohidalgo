<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Http\Backend\Helpers\Slug\Slug;

class Author extends Model {

    protected $fillable = ['name','title','biography','photo','slug'];
    public $timestamps  = false;
    
    public function articles()
    {
        return $this->HasMany('App\Http\Entities\Article');
    }
    
    /*
     * ================ Formato para slug ================
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Slug::filter($value);
    }
    
}
