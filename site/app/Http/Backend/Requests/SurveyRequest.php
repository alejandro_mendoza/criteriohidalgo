<?php  namespace App\Http\Backend\Requests;

class SurveyRequest extends Request {
    public function authorize() {
        return true;
    }
    protected $rules = [
        'sentence'  => 'required|max:255'
    ];
    public function rules() {
        return $this->rules;
    }

    public function messages() {
        $messages = [
            'options' => trans('backend.survey.error_options'),
        ];

        return $messages;
    }


}