<?php namespace App\Http\Backend\Requests;

use Illuminate\Routing\Route;

class EditionRequest extends Request
{
    protected $rules = [
        'date_publishing'   => 'required',
        'title'             => 'required|max:255',
        'image'             => 'mimes:jpg,jpeg'
    ];

    public function rules(Route $route)
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 