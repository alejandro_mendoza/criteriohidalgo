<?php namespace App\Http\Backend\Requests;

class ArticleRequest extends Request
{
    protected $rules = [
        'title'                 => 'required|max:255',
        'category_id'           => 'required',
        'author_id'             => 'required',
        'date_publishing'       => 'required',
        'intro'                 => 'required|max:220',
        'content'               => 'required',
        'description'           => 'max:150'
    ];

    public function rules()
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 