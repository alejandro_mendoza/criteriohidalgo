<?php namespace App\Http\Backend\Requests;

use Illuminate\Routing\Route;

class Static_elementRequest extends Request
{
    protected $rules = [
        'title'      => 'required|max:255',
        'text'       => 'max:5000',
        'link'       => 'max:255',
        'static_id'  => 'required',
        'image'      => 'mimes:jpg,png,jpeg'
    ];

    public function rules(Route $route)
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 