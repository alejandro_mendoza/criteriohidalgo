<?php namespace App\Http\Backend\Requests;

use Illuminate\Routing\Route;

class HomeRequest extends Request
{
    protected $rules = [
        'category_id'      => 'required|numeric',
    ];

    public function rules(Route $route)
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 