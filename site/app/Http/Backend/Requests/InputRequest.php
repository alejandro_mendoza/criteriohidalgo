<?php
namespace App\Http\Backend\Requests;


class InputRequest extends Request {
    protected $optionables = [
        'radio','select','checkbox','captcha'
    ];

    public function authorize() {
        return true;
    }

    public function rules() {

        $type = $this->get('type');
        if($type == 'captcha') {
            $rules = [
                'type'      => ['required'],
                'position'  => ['required','integer','min:1'],
                'options'   => ['required','google_captcha_keys'],
            ];
        } else {
            $rules = [
                'name'      => ['required'],
                'type'      => ['required'],
                'position'  => ['required','integer','min:1'],
            ];

            if(in_array($type,$this->optionables)) {
                $rules['options'] = ['required','option_list'];
            }
        }

        return $rules;
    }
}