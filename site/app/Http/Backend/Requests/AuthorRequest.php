<?php namespace App\Http\Backend\Requests;

use Illuminate\Routing\Route;

class AuthorRequest extends Request
{
    protected $rules = [
        'name'      => 'required|max:255',
        'title'     => 'max:255',
        'biography' => 'required|max:5000',
        'photo'     => 'mimes:jpg,png,jpeg'
    ];

    public function rules(Route $route)
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 