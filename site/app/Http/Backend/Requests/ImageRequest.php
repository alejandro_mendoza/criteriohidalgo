<?php namespace App\Http\Backend\Requests;

class ImageRequest extends Request
{
    protected $rules = [
        'title'                 => 'size:150',

    ];

    public function rules()
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 