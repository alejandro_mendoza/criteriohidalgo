<?php namespace App\Http\Backend\Helpers\Slug;

class Slug
{
    
    /**
     * Filters a string, e.g., "Petty theft" to "petty-theft"
     */
    public static function filter ($text, $length = 60) {
        $string = htmlentities($text);
        $string = preg_replace('/\&(.)[^;]*;/', '\\1', $string);
        $string = strtolower ($string);				// convert to lowercase
        $string = mb_substr($string, 0, $length);
        $string = trim($string);
        return str_replace(' ', '-', $string);	// trim to first $length chars
    }
}