<?php

namespace App\Http\Backend\Helpers\Slug;

use Illuminate\Support\Facades\Facade;

class SlugFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Slug'; }
}