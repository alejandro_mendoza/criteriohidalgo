<?php namespace App\Http\Backend\Helpers\Util;

class Util
{
    /*
     * ============= Estado de un registro =============
     */
    public function active($value)
    {
        return empty($value) ? '<span class="glyphicon glyphicon-eye-close"></span>' : '<span class="glyphicon glyphicon-eye-open"></span>';
    }

    /*
     * ============= Artículo Destacado =============
     */
    public function featured($value)
    {
        return empty($value) ? null : '<span class="glyphicon glyphicon-bookmark"></span>';
    }

    /*
     * ============= Artículo Principal =============
     */
    public function principal($value)
    {
        return empty($value) ? null : '<span class="glyphicon glyphicon-star"></span>';
    }

    /*
     * ============= Color =============
     */
    public function color($value)
    {
        return empty($value) ? null : '<span class="glyphicon glyphicon-tint" style="color:'.$value.'"></span>';
    }

    /*
     * ============= Seleccionar opción predeterminada =============
     */
    public function selectedOption($id, $value)
    {
        return $id == $value
            ? 'selected=selected'
            : null
        ;
    }

    /*
     * ============= Deshabilitar opción de un campo =============
     */
    public function disabledOption($value, $values = array())
    {
        return !empty($values)
            ? !in_array($value, $values)
                ? 'disabled=disabled'
                : null
            : null
        ;
    }

    /*
     * ============= Tipo de Mensaje =============
     */
    public function msgType($type)
    {
        switch($type)
        {
            case 'success'  : return ['alert' => 'alert-success',    'icon' => 'glyphicon-thumbs-up',           'expression' => '¡Muy bien!'];
            case 'warning'  : return ['alert' => 'alert-warning',    'icon' => 'glyphicon-warning-sign',        'expression' => '¡Cuidado!'];
            case 'error'    : return ['alert' => 'alert-danger',     'icon' => 'glyphicon-exclamation-sign',    'expression' => '¡Whoops!'];
        }
    }

    /*
     *  ============= Rango de números en un arreglo =============
     */
    public function range($from = 1, $to = 10, $increment = 1)
    {
        for($from; $from <= $to; $from = $from + $increment)
        {
            $range[$from] = $from;
        }

        return $range;
    }

    /*
     * ============= Fecha =============
     */
    public function date($value)
    {
        setlocale(LC_ALL, 'es_ES.UTF-8');
        return  strftime('%A %d de %B de %Y', strtotime($value));
    }

    /*
     * ============= Formato de Fecha =============
     */
    public function dateFormat($value)
    {
        setlocale(LC_ALL, 'es_ES.UTF-8');
        return  strftime('%d/%B/%Y', strtotime($value));
    }

    /*
     * ============= Formato de Fecha =============
     */
    public function hourFormat($value)
    {
        return  strftime('%H:%M:%S %p', strtotime($value));
    }

    /*
     * ============= Validar que exista un archivo =============
     */
    public function fileExists($path)
    {
        if(File::exists(public_path() . "/{$path}")) return true; else return false;
    }
}