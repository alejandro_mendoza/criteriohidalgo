<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Ads;
use App\Http\Entities\Sub_Classified;

class AdsRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'   => '',
        'featuring' => null,
        'inverse'   => false,
        'items'     => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Ads();
    }

    /*
     * ================ Nombre del Panel de Control ================
     */
    public function setSectionName()
    {
        return 'Anuncios';
    }

    /*
     * ================== Listado y paginación de Usuarios ==================
     */
    public function paginate($filters)
    {
        $list = $this->model;

        #----------- Buscar palabra clave -----------
        if(!empty($filters['keyword']))
        {
            $list = $list->where(function($query) use($filters){
                $query->where('title','LIKE',"%{$filters['keyword']}%");
            });
        }

        $direction = ($filters['inverse'] == false) ? 'DESC' : 'ASC';

        $list = $list->orderBy('created_at', $direction)->with('category')->paginate($filters['items']);

        return $list;
    }

    /*
     * ================ Guardar y Actualizar Usuarios ================
     */
    public function save($item, $request)
    {   
        $data = $request->except('_token$this->separatoroptions');
 
        $item->fill($data)->save();
       
        return $item->id;
    }
    /*
     * ================ Eliminar registros ================
     */
    public function delete($idList)
    {
        $this->model
            ->whereIn('id', $idList)
            ->delete();
        $editions    = array();
        return !empty($editions) ? ['warning' => $editions] : null;
    }


}