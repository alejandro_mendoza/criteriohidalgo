<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Type;

class TypeRepo
{
    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Type();
    }

    /*
     * ================ Lista de roles de usuarios ================
     */
    public function getTypes()
    {
        return $this->setModel()
            ->orderBy('name', 'asc')
            ->whereNotIn('slug', ['formulario-contacto', 'blockLeft']);
    }

}