<?php  namespace App\Http\Backend\Repositories;
use App\Http\Entities\Survey;
use App\Http\Entities\Option;

class SurveyRepo extends BaseRepo {
    protected $defaultFilters = [
        'keyword' => '',
        'inverse'   => false,
        'items' => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel() {
        return new Survey();
    }
    
    /*
     * ================ Nombre del Panel de Control ================
     */
    public function setSectionName() {
        return 'Surveys';
    }
    
    /*
     * ================ Obtener encuesta ================
     */
    public function getItem($id)
    {
        $item = $this->model;

        if(!empty($id))
        {
            $item = $this->model->where('id',$id)->with('options')->first();
            if(!$item) abort(404);
        }

        return $item;
    }
    /*
     * ================ Guardar y Actualizar encuesta ================
     */
    public function save($item, $request) {
        if(!$item) {
            $item = new Survey();
        }
        $current = $request->get('current');
        
        $data = [
            'sentence' => trim($request->get('sentence')),
            'current' => !empty($current),
        ];

        $current = $request->get('current');
        $item->fill($data)->save();
        if($data['current']) {
            $this->model->where('id','<>',$item->id)
                        ->update(['current'=>false]);
        }
        foreach($request->get('options') as $index => $opt) {
            $option = Option::where('survey_id',$item->id)
                            ->where('index',$index)
                            ->first();
            
            if(!$option) $option = new Option();

            $counter = 0;
            if($option->exists) {
                $counter = ($opt) ? $option->counter : 0;
            }

            $dataOption = [
                'survey_id' => $item->id,
                'sentence'  => trim($opt),
                'counter'   => $counter,
                'index'     => $index,
            ];

            $option->fill($dataOption)->save();
        }

        return $item->id;
    }

    /*
     * ================== Listado y paginación de encuestas ==================
     */
    public function paginate($filters) {
       $list = $this->model;

        #----------- Buscar palabra clave -----------
        if(!empty($filters['keyword']))
        {
            $list = $list->where(function($query) use($filters){
                $query->where('sentence','LIKE',"%{$filters['keyword']}%");
            });
        }

        $direction = ($filters['inverse'] == false) ? 'DESC' : 'ASC';

        $list = $list->orderBy('sentence', $direction)->paginate($filters['items']);

        return $list;
    }

    /*
     * ================ Eliminar registros ================
     */
    public function delete($idList) 
    {
        $this->model
            ->whereIn('id', $idList)
            ->delete();
        $editions    = array();
        return !empty($editions) ? ['warning' => $editions] : null;
    }
}