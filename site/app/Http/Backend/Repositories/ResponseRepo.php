<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Response;

class ResponseRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'   => '',
        'featuring' => null,
        'inverse'   => false,
        'items'     => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Response();
    }

    /*
     * ================ Nombre del Panel de Control ================
     */
    public function setSectionName()
    {
        return 'Mensajes';
    }

    /*
     * ================== Listado y paginación de Usuarios ==================
     */
    public function paginate($filters)
    {
        $list = $this->model->orderBy('seen','desc')->orderBy('created_at','desc');

        #----------- Buscar palabra clave -----------
        if(!empty($filters['keyword']))
        {
            $list = $list->where(function($query) use($filters){
                $query->where('values','LIKE',"%{$filters['keyword']}%");
            });
        }

        $direction = ($filters['inverse'] == false) ? 'DESC' : 'ASC';

        $list = $list->orderBy('values', $direction)->paginate($filters['items']);

        return $list;
    }

    /*
     * ================ Cambiar a visto ================
     */
    public function save($item, $request)
    {   
        $msj = Response::find($item);
        $msj->seen = $request;
        $msj->save();
        return $msj->id;
    }
    /*
     * ================ Eliminar registros ================
     */
    public function delete($idList)
    {
        $this->model
            ->whereIn('id', $idList)
            ->delete();
        $editions    = array();
        return !empty($editions) ? ['warning' => $editions] : null;
    }
}