<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Article;
use App\Http\Backend\Events\Article\SaveEvent as SaveIdsTagsGallery;
use App\Http\Backend\Events\Article\DeleteEvent as DeleteIdsTagsGallery;

class ArticleRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'       => null,
        'category_id'   => null,
        'author_id'     => null,
        'principal'     => null,
        'featuring'     => null,
        'active'        => null,
        'inverse'       => false,
        'date_publishing' => null,
        'items'         => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Article();
    }

    /*
     * ================ Nombre del la sección ================
     */
    public function setSectionName()
    {
        return 'Articles';
    }

    /*
     * ================== Consultar registro por Id ==================
     */
    public function getItem($id)
    {
        $item = null;
        $ids  = [];
        $tags = [];

        if($id)
        {
            $item = $this->model->where('id', $id)->first();

            if(!$item) abort(404);

            #----------- Ids de Artículo -----------
            foreach($item->ids as $id) $ids[] = $id->article_id;
            #----------- Tags de Artículo -----------
            foreach($item->tags as $tag) $tags[] = $tag->value;

            $item->ids = implode(' , ',$ids);
            $item->tags = implode(' , ',$tags);
        }
        return $item;
    }

    /*
     * ============== Obtener datos de nota ==============
     */
    public function getArticle($articleId)
    {

        return $this->model
            ->where('id', $articleId)
            ->withGallery()
            ->whereHasCategory()
            ->withCategoryModules()
            ->with('authors', 'ids', 'tags')
            ->orderBy('date_publishing', 'desc')
            ->first();
    }

    /*
     * ================== Listado y paginación de registros ==================
     */
    public function paginate($filters)
    {
        $list = $this->model->with('category', 'authors');

        #----------- Categorías asignadas a usuarios -----------
        !empty($this->getUser()->assigned) ? $list->whereIn('category_id', $this->getUser()->assigned) : null;

        #----------- Buscar palabra clave -----------
        if(!empty($filters['keyword']))
        {
            $list = $list->where(function($query) use($filters){
                $query->where('title', 'LIKE',"%{$filters['keyword']}%");
            });
        }
        #----------- Buscar por categoría -----------
        !empty($filters['category_id']) ? $list = $list->where('category_id', $filters['category_id']) : null;

        #----------- Buscar por autor -----------
        !empty($filters['author_id']) ? $list = $list->where('author_id', $filters['author_id']) : null;

        #----------- Buscar por destacado -----------
        !empty($filters['featuring']) ? $list = $list->where('featuring', $filters['featuring']) : null;

        #----------- Buscar por principal -----------
        !empty($filters['principal']) ? $list = $list->where('principal', $filters['principal']) : null;

        #----------- Buscar por estatus -----------
        !empty($filters['active']) ? $list = $list->where('active', $filters['active'] == 'si' ? true : false) : null;

        isset($filters['date_publishing']) && !empty($filters['date_publishing']) ? $list = $list->where('date_publishing', '=', $filters['date_publishing']) : null;

        $direction = ($filters['inverse'] == true) ? 'ASC' : 'DESC';

        $list = $list->orderBy('created_at', $direction)->paginate($filters['items']);

        return $list;
    }

    /*
     * ================ Guardar y actualizar registros ================
     */
    public function save($item, $request)
    {
        if(!$item) $item = $this->setModel(); else unset($item->assets); unset($item->ids); unset($item->tags);

        $exists = $item->exists;

        $item->fill($request->except('_token', 'ids', 'tags','autocomplete-name'))->save();

        #------------ Guardar Ids, Tags y Galería ------------
        event(new SaveIdsTagsGallery($item, $request, $exists));

        return $item->id;
    }

    /*
     * ================== Eliminar Registros ==================
     */
    public function delete($idList)
    {
        #------------ Eliminar Ids, Tags y Galería ------------
        event(new DeleteIdsTagsGallery($list = $this->model->whereIn('id', $idList)->with('galleries', 'ids', 'tags')));

        $list->delete();
    }

}