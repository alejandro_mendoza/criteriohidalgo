<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Author;
use App\Http\Backend\Helpers\Files as file;

class AuthorRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'   => '',
        'featuring' => null,
        'inverse'   => false,
        'items'     => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Author();
    }

    /*
     * ================ Nombre del Panel de Control ================
     */
    public function setSectionName()
    {
        return 'Autores';
    }

    /*
     * ================== Listado y paginación de Usuarios ==================
     */
    public function paginate($filters)
    {
        $list = $this->model;

        #----------- Buscar palabra clave -----------
        if(!empty($filters['keyword']))
        {
            $list = $list->where(function($query) use($filters){
                $query->where('name','LIKE',"%{$filters['keyword']}%");
            });
        }

        $direction = ($filters['inverse'] == false) ? 'ASC' : 'DESC';

        $list = $list->orderBy('name', $direction)->paginate($filters['items']);

        return $list;
    }

    /*
     * ================ Obtener Autores ================
     */
    public function getAuthors()
    {
        return $this->model
            ->orderBy('name', 'asc')
            ->lists('name', 'id')
            ->toArray();
    }

    /*
     * ================ Guardar y Actualizar Usuarios ================
     */
    public function save($item, $request)
    {   $data = $request->except('_token$this->separatoroptions');
        $files = new file();
        $file = $files->uploadFile([
            'files'     => $request->only('photo'),
            'route'     => "images/photos",
            'entity'    => $item
        ]);
        if(!empty($file)) $data['photo'] = $file;
    
        $item->fill($data)->save();
        return $item->id;
    }
    /*
     * ================ Eliminar registros ================
     */
    public function delete($idList)
    {

        $list       = $this->model->with('articles')->whereIn('id', $idList);
        $authors    = array();

        foreach($list->get() as $author)
        {
            $author->articles->count() == 0
                ? $author->delete()
                : $authors[] = $author->name
            ;
        }

        return !empty($authors) ? ['warning' => $authors] : null;
    }

    public function getCompletes($request)
    {
        $word = $request;//->get('searchAuthor');
        $result = Author::select('name','id')->where("name", "LIKE", "%{$word}%")->get();
        $json = json_encode($result);
        return $json;
    }
}