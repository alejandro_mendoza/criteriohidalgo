<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Category;
use App\Http\Backend\Events\Category\DeleteEvent as DeleteImageModules;

class CategoryRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'   => null,
        'parent_id' => null,
        'active'    => null,
        'inverse'   => false,
        'items'     => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Category();
    }

    /*
     * ================ Nombre del la sección ================
     */
    public function setSectionName()
    {
        return 'Categories';
    }

    /*
     * ================== Listado y paginación de registros ==================
     */
    public function paginate($filters)
    {
        $list = $this->model->with('parent')->where('hidden', false);

        #----------- Buscar palabra clave -----------
        if(!empty($filters['keyword']))
        {
            $list = $list->where(function($query) use($filters){
                $query->where('title', 'LIKE',"%{$filters['keyword']}%");
            });
        }
        #----------- Buscar por sección -----------
        !empty($filters['parent_id'])
            ? $list = ($filters['parent_id'] == 'root')
                ? $list->whereNull('parent_id')
                : $list->where('parent_id', $filters['parent_id'])
            : null
        ;

        #----------- Buscar por estatus -----------
        !empty($filters['active']) ? $list = $list->where('active', $filters['active'] == 'si' ? true : false) : null;

        $direction = ($filters['inverse'] == true) ? 'DESC' : 'ASC';

        $list = $list->orderBy('title', $direction)->paginate($filters['items']);

        return $list;
    }

    /*
     * ================ Obtener secciones ================
     */
    public function getSections()
    {
        return $this->model
            ->roots()
            ->where('hidden', false)
            ->lists('title', 'id')
            ->toArray();
    }

    /*
     * ================ Obtener categoría ================
     */
    public function getCategory($categoryId)
    {
        return $this->model
            ->with(['modules' => function($module){
                $module->orderBy('position', 'asc');
            }])
            ->find($categoryId);
    }

    /*
     * ================ Obtener categorías ================
     */
    public function getCategories()
    {
        $category = array();

        foreach($this->model->roots()->where('hidden', false)->get() as $root)
        {
            //if($root->descendants()->count() > 0)
                $category[$root->id] = [
                    'id'        => $root->id,
                    'category'  => $root->title,
                    'class'     => 'option-group'
                ];
            foreach($root->descendants()->get() as $item)
                $category[$root->id]['subcategory'][] = [
                    'id'        => $item->id,
                    'category'  => $item->title,
                    'class'     => 'option-child'
                ];
        }

        return $category;
    }

    /*
     * ================ Guardar y actualizar registros ================
     */
    public function save($item, $request)
    {
        if(!$item) $item = $this->setModel();
        
        //  guardamos los banners
        $param = [];
        if($request->get('banner1') != ''){
            $param[] = ['one' => base64_encode($request->get('banner1'))];
        }
        else{
            $param[] = ['one' => ''];
        }
        if($request->get('banner2') != ''){
            $param[] = ['one' => base64_encode($request->get('banner2'))];
        }
        else{
            $param[] = ['one' => ''];
        }
        if($request->get('banner3') != ''){
            $param[] = ['one' => base64_encode($request->get('banner3'))];
        }
        else{
            $param[] = ['one' => ''];
        }
        if($request->get('banner4') != ''){
            $param[] = ['one' => base64_encode($request->get('banner4'))];
        }
        else{
            $param[] = ['one' => ''];
        }

        $item->fill($request->except('_token', 'parent_id', 'file'))->save();
        
        $item->params = json_encode($param);
        
        $item->save();

        $parent = $this->find($request->get('parent_id'));

        !empty($parent) ? $item->makeChildOf($parent) : null;

        return $item->id;
    }

    /*
     * ================ Eliminar registros ================
     */
    public function delete($idList)
    {
        $list       = $this->model->whereNotNull('parent_id')->whereIn('id', $idList);
        $categories = array();

        foreach($list->get() as $category)
        {
            ///dd($category->modules()->get());
            ///dd($this->modulesParams($category->modules()));
            if($category->article()->count() == 0 && $this->modulesParams($category->modules()))
            {
                event(new DeleteImageModules($category));
                $category->delete();
            }
            else
            {
                $categories[] = $category->title;
            }
        }

        return !empty($categories) ? ['warning' => $categories] : null;
    }

    public function modulesParams($modules){

        if($modules->count() == 0)
            return true;
        else{
            $arrayModules = $modules->get();
            foreach($arrayModules as $modul){
                if($modul->params != '') {
                    return false;
                }
            }
            return true;
        }
    }

    /*
     * Banners publicitarios
     *
     * @param slug de la categoria, posicion del banner
     * @return  codigo del banner
     */
    public function decodeBanner()
    {
        $categories = $this->model
            ->where('hidden',0)
            ->get();

        foreach($categories as $category){
            $banner = !empty($category->params) ? json_decode($category->params) : null;
            if($banner != null){
                $newDecode = [];
                foreach($banner as $ban){
                    $newDecode[] = ['one' => base64_encode($ban->one)];
                }
                $category->params = json_encode($newDecode);
                $category->save();
            }
        }
        return 'success';
    }
}

