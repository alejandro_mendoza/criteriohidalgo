<?php
namespace App\Http\Backend\Repositories;


use App\Http\Entities\Form;
use App\Http\Entities\Module;

class FormRepo extends BaseRepo {
    protected $module;
    function __construct() {
        $this->module = Module::where('type_slug','formulario_contacto')->first();//\Session::get('mcpanel.formularios.id'));
        parent::__construct();
    }

    public function setModel()
    {
        return new Form();
    }

    public function setSectionName()
    {
        return 'Forms';
    }

    public function save($item, $request) {
        if(!$item) abort(404);

        $data = $request->except('_token');
        $item->fill($data)->save();
        $this->returnToIndex = true;
        return $item->id;
    }

    public function paginate($filters)
    {
        $form = $this->module
                     ->modulable()
                     ->with([
                        'inputs'=>function($inputs) {
                            $inputs->orderBy('position');
                        }
                    ])
                    ->first();
        return $form->inputs;
    }

    public function delete($idList)
    {
        // TODO: Implement delete() method.
    }

    public function validateReport($formId) {
        $form = $this->model->where('id',$formId)
                            ->whereHas('responses',function($response){})
                            ->with([
                                'responses' => function($response) {
                                    $response->orderBy('created_at','desc');
                                }
                            ])
                            ->first();
        return $form;
    }
}