<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Module;

class ModuleRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'   => '',
        'featuring' => null,
        'inverse'   => false,
        'items'     => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Module();
    }

    /*
     * ================ Nombre del Panel de Control ================
     */
    public function setSectionName()
    {
        return 'Modulos';
    }

    /*
     * ================== Listado y paginación de Usuarios ==================
     */
    public function paginate($categoryId)
    {
        return $this->model
            ->where('category_id', $categoryId)
            ->orderBy('position', 'asc')
            ->get();
    }

    public function save($item, $request){}

    /*
     * ================ Agregar módulo ================
     */
    public function saveModule($request, $item = null)
    {
        $item = !$item ? $this->setModel() : $item;

        $item->fill($request->except('_token', 'module'))->save();
    }

    /*
     * ================= Eliminar módulos =================
     */
    public function deleteModule($categoryId, $list = [])
    {
        if(is_array($list))
        {
            $this->model->whereIn('id', $list)->delete();
            $this->ResetPositons([['category_id', '=', $categoryId]]);
        }
    }

    public function setVisibility($id)
    {
        $module = $this->model->where('id', $id)->first();

        if($module) {
            $module->visible = !$module->visible;
            $module->save();
        }
    }

    /*
     * ============= Ordenar posiciones de elementos con Ajax =============
     */
    public function order($idList)
    {
        if(!empty($idList))
        {
            foreach($idList as $position => $id) $this->model->where('id', $id)->update(['position' => $position + 1]);
        }
    }

}