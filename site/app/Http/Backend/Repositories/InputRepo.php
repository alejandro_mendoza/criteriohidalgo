<?php namespace App\Http\Backend\Repositories;


use App\Http\Entities\Input;

class InputRepo extends BaseRepo {
    public static $SEPARATOR = '||';

    public function setModel()
    {
        return new Input();
    }

    public function setSectionName()
    {
        return 'Forms';
    }

    public function save($item, $request) {
        if(!$item) $item = new Input();

        $data = $request->except('_token$this->separatoroptions');
        // Si el campo es un captcha, le agregamos un nombre genérico
        if($data['type'] == 'captcha') {
            $data['name']       = 'Captcha';
            $data['required']   = true;
        }

        $data['slug'] = $this->setSlug($data['name'],$item->id);
        $data['required'] = !empty($data['required']);
        $this->constraints = [['form_id','=',$data['form_id']]];
        $data['position'] = $this->setPosition($data['position'],$item);

        $options = $request->get('options');
            
        $options = explode(InputRepo::$SEPARATOR,$options);

        if(is_array($options)){
            foreach($options as &$option) $option = trim($option);
            $data['options'] = json_encode(array_filter($options));
        }
        //dd($data);
        $item->fill($data)->save();

        return $item->id;
    }

    public function paginate($filters)
    {
        // TODO: Implement paginate() method.
    }

    public function delete($idList) {
        $deleted  = $this->model->whereIn('id',$idList);

        foreach($deleted->get() as $input) {
            $this->constraints[] = ['form_id','=',$input->form_id];
        }

        $deleted = $deleted->delete();
        $this->resetPositons();
        //return $deleted;
        $editions    = array();
        return !empty($editions) ? ['warning' => $editions] : null;
    }

    public function typesList() {
        return [
            //'radio'     => 'Boton de radio',
            'email'     => 'Campo de email',
            'captcha'   => 'Captcha de google',
            //'checkbox'  => 'CheckBox',
            'select'    => 'Combo',
            'text'      => 'Texto (una línea)',
            'textarea'  => 'Texto (varias líneas)',
        ];
    }

    public function getItem($id)
    {
        $item = null;
        if(!empty($id)) {
            $item = $this->model->find($id);
            $options = json_decode($item->options);
            $item->options = implode(' '.InputRepo::$SEPARATOR.' ',$options);

            if(!$item) {
                abort(404);
            }
        }

        return $item;
    }


}