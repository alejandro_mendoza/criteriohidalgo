<?php

namespace App\Http\Backend\Traits;
use App\Http\Entities\User;
use App\Http\Entities\Author;
use App\Http\Backend\Requests\EditExpressRequest;

trait EditExpressTrait {

    public function editExpress(EditExpressRequest $request) {
        $id = $request->get("item");
        $campo = $request->get("state");
        $source = $request->get("source");
        $text = $request->get("text");

        if($source == 1)                    // Users 1
            $entity = User::find($id);
        elseif($source == 2)                // Authors 2
            $entity = Author::find($id);

        $entity->$campo = $text;
        $entity->save();
        return $text;
    }
}