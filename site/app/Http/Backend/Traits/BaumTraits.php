<?php
namespace App\Http\Traits;


trait BaumTraits {
    public function hasChildren() {
        $totalChildren  = $this->children()->count();
        return $totalChildren > 0;
    }
}