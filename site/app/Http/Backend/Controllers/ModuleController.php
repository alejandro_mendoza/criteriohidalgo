<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\CategoryRepo;
use App\Http\Backend\Repositories\ModuleRepo;
use App\Http\Backend\Repositories\TypeRepo;
use Illuminate\Http\Request;

class ModuleController extends BaseController
{
    function __construct(ModuleRepo $module)
    {
        parent::__construct($module);
    }

    /*
     * =============== Lista de Módulos ===============
     */
    public function getList(CategoryRepo $category, TypeRepo $type, $categoryId)
    {
        return view('Backend.Modules.index', [
            'assigned'      => [],
            'category'      => $category->getCategory($categoryId),
            'categories'    => $category->getCategories(),
            'types'         => $type->getTypes()->lists('name', 'id')->toArray()
        ]);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function addModule(Request $request, $categoryId = null)
    {
        $typeRepo   = new TypeRepo();
        $type       = $typeRepo->setModel()->find($request->get('type_id'));

        $this->repo->constraints = [['category_id', '=', $categoryId]];

        $request->merge([
            'category_id'   => $categoryId,
            'visible'       => true,
            'type_slug'     => $type->slug,
            'position'      => $this->repo->GetLastPosition()
        ]);

        $this->repo->saveModule($request);

        return redirect()->back();
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function updateModule(Request $request, $id)
    {
        // Codificar si es un banner
        array_key_exists('html', $request->get('module'))
            ? $request->merge(['module' => ['html' => base64_encode($request->get('module')['html'])]])
            : null
        ;

        $module = $this->repo->find($id);

        $request->merge(['params' => json_encode($request->get('module'))]);

        $module ? $this->repo->saveModule($request, $module) : null;

        return redirect()->back();
    }

    /*
     * =============== Eliminar módulos ===============
     */
    function deleteModule(Request $request, $id = null)
    {
        $this->repo->deleteModule($request->get('category_id'), !empty($id) ? [$id] : $request->get('multiselect'));

        return redirect()->back();
    }

    public function toogleVisibility(ModuleRepo $module, $id)
    {
        $module->setVisibility([$id]);

        return redirect()->back();
    }

    /*
     * =============== Ordenar posiciones de módulos ===============
     */
    public function setOrder()
    {
        return $this->repo->order(\Request::get('order'));
    }

}
