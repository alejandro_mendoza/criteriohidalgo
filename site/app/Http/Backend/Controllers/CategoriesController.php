<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\CategoryRepo;
use App\Http\Backend\Requests\CategoryRequest;

class CategoriesController extends BaseController
{
    private $sections;

    function __construct(CategoryRepo $category)
    {
        $this->sections = ['sections' => $category->getSections()];

        parent::__construct($category);
    }

    /*
     * =============== Panel ===============
     */
    public function getIndex($extra = null)
    {
        return parent::getIndex($this->sections);
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        return parent::getRegistro($id, $this->sections);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(CategoryRequest $request, $id = null)
    {
        $request->merge([
            'slug'      => $this->setSlug($request->get('title'), $id),
            'active'    => $request->has('active')
        ]);

        //------------ Guardar imagen ------------
        $request->hasFile('file')
            ? $request->merge(['image' => $this->saveFile($request, 'file', public_path('images/categories'), $request->get('slug').'-image')])
            : null
        ;

        return $this->doUpdate($request, $id);
    }

    public function decode(CategoryRepo $repo)
    {
        return $repo->decodeBanner();
    }

}
