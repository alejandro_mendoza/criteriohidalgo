<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Requests\UserRequest;
use App\Http\Backend\Repositories\RoleRepo;
use App\Http\Backend\Repositories\UserRepo;
use App\Http\Backend\Repositories\CategoryRepo;

class UsersController extends BaseController
{
    private $extra;

    function __construct(UserRepo $user, RoleRepo $role, CategoryRepo $category)
    {
        $this->extra = [
            'roles'         => $role->getRoles()->lists('name', 'id')->toArray(),
            'categories'    => $category->getCategories()
        ];

        parent::__construct($user);
    }

    /*
     * =============== Panel ===============
     */
    public function getIndex($extra = null)
    {
        return parent::getIndex($this->extra);
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        return parent::getRegistro($id, $this->extra);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(UserRequest $request, $id = null)
    {
        $request->merge(['active' => $request->has('active')]);

        return $this->doUpdate($request, $id);
    }

}
