<?php  namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\SurveyRepo;
use App\Http\Backend\Requests\SurveyRequest;

class SurveyController extends BaseController 
{
    function __construct(SurveyRepo $surveyRepo) 
    {
        parent::__construct($surveyRepo);
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getIndex($extra = [])
    {
        $extra = ['entity'=>1];
        return parent::getIndex($extra);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(SurveyRequest $request,$id=null) 
    {
        return $this->doUpdate($request,$id);
    }
}