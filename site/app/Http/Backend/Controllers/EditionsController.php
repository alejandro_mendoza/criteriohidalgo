<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\EditionsRepo;
use App\Http\Backend\Requests\EditionRequest;
use Illuminate\Support\Facades\DB;

class EditionsController extends BaseController
{
    function __construct(EditionsRepo $repo)
    {
        parent::__construct($repo);
    }

    /*
     * =============== Panel ===============
     */
    public function getId($id)
    {
        \Session::put('mcpanel.author.id', $id);

        return redirect()->route('mcpanel.Author');
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getIndex($extra = [])
    {
        return parent::getIndex($extra);
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        $dbCategories = DB::select('SELECT id, title FROM categories WHERE parent_id IS NULL ORDER BY title');
        $categories = [];
        foreach ($dbCategories as $category) {
            $categories[$category->id] = $category->title;
        }

        $extra = ['categories' => $categories];
        return parent::getRegistro($id, $extra);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(EditionRequest $request, $id = null)
    {   
        return $this->doUpdate($request, $id);
    }

}
