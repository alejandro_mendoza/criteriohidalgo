<?php namespace App\Http\Backend\Controllers;

class DashboardController extends Controller
{
	/*
	 * ================ Vista de panel general de administración ================
	 */
	public function index()
	{
		return view('Backend.Dashboard.home');
	}

	/*
	 * ================ 401 Acceso Denegado ================
	 */
	public function error401()
	{
		return view('Backend.Errors.401');
	}

}
