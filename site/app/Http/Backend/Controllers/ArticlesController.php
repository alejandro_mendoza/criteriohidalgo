<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\ArticleRepo;
use App\Http\Backend\Repositories\CategoryRepo;
use App\Http\Backend\Repositories\AuthorRepo;
use App\Http\Backend\Requests\ArticleRequest;
use App\Http\Frontend\Repositories\ArticlesRepo;

class ArticlesController extends BaseController
{
    private $extra;

    function __construct(ArticleRepo $article, CategoryRepo $category, AuthorRepo $author)
    {
        $this->extra = [
            'categories'    => $category->getCategories(),
            'authors'       => $author->getAuthors(),
            'assigned'      => $this->getUser()->assigned
        ];

        parent::__construct($article);
    }

    /*
     * =============== Panel ===============
     */
    public function getIndex($extra = null)
    {
        return parent::getIndex($this->extra);
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        return parent::getRegistro($id, $this->extra);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(ArticleRequest $request, $id = null)
    {
        $request->merge([
            'slug'      => $this->setSlug($request->get('title'), $id),
            'active'    => $request->has('active'),
            'featuring' => $request->has('featuring'),
            'principal' => $request->has('principal'),
            'author'    => $request->has('author')
        ]);

        return $this->doUpdate($request, $id);
    }

    /*
     * =============== Vista previa de nota ===============
     */
    public function getPreview(ArticlesRepo $repo, $articleId)
    {
        $this->notFoundUnless($article = $this->repo->getArticle($articleId));

        return view('Backend.Template.Preview.index', [
            'article'   => $article,
            'related'   => $repo->getArticlesRelated($article, $article->ids->isEmpty() ? 'tags' : 'ids')
        ]);
    }

}


