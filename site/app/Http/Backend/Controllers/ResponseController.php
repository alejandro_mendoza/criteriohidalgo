<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\ResponseRepo;
use App\Http\Backend\Repositories\FormRepo;

class ResponseController extends BaseController
{
    function __construct(ResponseRepo $resp)
    {
        parent::__construct($resp);
    }

    /*
     * =============== Panel ===============
     */
    public function getId($id)
    {
        \Session::put('mcpanel.response.id', $id);
        return redirect()->route('response');
    }

    /*
     * =============== Formulario principal===============
     */
    public function getIndex($extra = [])
    {
        $formRepo = new FormRepo;
        $form = $formRepo->validateReport(1);
        $extra = ['form'=>$form];
        return parent::getIndex($extra);
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        $respRepo = new ResponseRepo;
        $respRepo->save($id, 0);
        return parent::getRegistro($id, $extra);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(ResponseRepo $request, $id = null)
    {   
        //return $this->doUpdate($request, $id);
    }

}
