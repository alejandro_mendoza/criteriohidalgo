<?php namespace App\Http\Backend\Controllers;

use App\Http\Entities\Module;
use App\Http\Backend\Repositories\FormRepo;
use App\Http\Backend\Requests\FormRequest;
use Maatwebsite\Excel\Facades\Excel;

class FormController extends BaseController {
    private $module;

    function __construct(FormRepo $formRepo)
    {
        $this->module = Module::where('type_slug','formulario_contacto')->first();
        /*
        \Session::has('mcpanel.formularios.id')
            ? $this->module = Module::find(\Session::get('mcpanel.formularios.id'))
            : redirect('mcPanel')
        ;
         * 
         */

        parent::__construct($formRepo);
    }

    /*
     * =============== Formulario de Archivo ===============
     */
    public function getId($id)
    {
        \Session::put('mcpanel.formularios.id', $id);

        return redirect()->route('forms');
    }

    /*
     * =============== Lista de Formulario ===============
     */
    public function getIndex($extra = [])
    {
        $extra = ['module' => $this->module];
        return parent::getIndex($extra);
    }

    /*
     * =============== Guardar Archivo ===============
     */
    public function postUpdate(FormRequest $request, $id=null) {
        if(!$id) abort(404);
        return $this->doUpdate($request, $id);
    }

    public function getReporte(FormRepo $formRepo, $formId) {
        $form = $formRepo->validateReport($formId);
        if(!$form) abort(404);

        $today = date('d-m-Y-His');
        Excel::create("reporte-formulario-contacto-{$today}",function($excel) use($form) {
            $excel->setTitle("Reporte de formulario de contacto");
            $excel->setCreator(url())
                ->setCompany('');
            $excel->setDescription("Reporte de formulario de contacto");

            $excel->sheet('reporte',function($sheet)use($form) {
                $sheet->loadView("Backend.Forms.reporte",[
                    'messages'    => $form->responses,
                    'section'     => $form->module[0]->section,
                    'form'        => $form,
                ]);
            });
        })->download('xls');
    }
}