<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\SliderRepo;
use Illuminate\Http\Request;
use App\Http\Backend\Requests\UrlSliderRequest;

class SliderController extends BaseController
{
    function __construct(SliderRepo $slider)
    {
        parent::__construct($slider);
    }

    /*
     * =============== Panel ===============
     */
    public function getId($id)
    {
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getIndex($extra = [])
    {
        $extra = ['gallery' => $this->repo->getGallery()];
        return parent::getIndex($extra);
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(UrlSliderRequest $request, $id = null)
    {   
        return $this->repo->setUrl($request);
    }

    /*
     * =============== Ajax para retornar la vista demo del slider ===============
     */
    public function getView(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('id'))) {
                $slider = $this->repo->setSlider($request->get('id'));
                $data = [
                    'slider' => $this->repo->getSlider($slider->id)
                ];
                $json = ['view' => view('Backend.Slider.plantillas.demo' . $request->get('id'),$data)->render(), 
                         'id'   => $slider->id,
                         'type' => $request->get('id') ];
                return json_encode($json);
            }
        }
    }
    /*
     * =============== Eliminar Registros ===============
     */
    public function anyEliminar($id = null)
    {
        $idList = [$id];
        $this->repo->delete($idList);
        $this->repo->reorder();
        $this->repo->setMessage($this->sectionName, \Lang::get('general.deleteData'), \Backend::msgType('success'));

        return redirect()->back();
    }
    /*
     * =============== Ordenar posiciones de elementos ===============
     */
    public function setOrder()
    {
        return $this->repo->order(\Request::get('order'));
    }
}
