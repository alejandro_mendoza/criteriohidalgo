<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\DirectoryRepo;
use App\Http\Backend\Requests\DirectoryRequest;

class DirectoryController extends BaseController
{
    function __construct(DirectoryRepo $user)
    {
        parent::__construct($user);
    }

    /*
     * =============== Panel ===============
     */
    public function getId($id)
    {
        \Session::put('mcpanel.directory.id', $id);

        return redirect()->route('mcpanel.Directory');
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getIndex($extra = [])
    {
        $extra = ['entity'=>1];
        return parent::getIndex($extra);
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        $extra = [];
        return parent::getRegistro($id, $extra);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(DirectoryRequest $request, $id = null)
    {   
        return $this->doUpdate($request, $id);
    }

}
