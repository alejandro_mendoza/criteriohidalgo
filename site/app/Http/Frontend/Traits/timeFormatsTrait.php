<?php
namespace App\Http\Frontend\Traits;

trait timeFormatsTrait {
   
    /*
     * ============= Formato de Fecha =============
     */
    public function dateFormat($value)
    {
        $meses = ["","Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"];

        $value = explode('-',$value);
        $year = $value[0];
        $month = $value[1];

        $day = $value[2];
        $day = substr($day,0,2);
        if($month < 10)
            $month = str_replace('0','',$month);
        try {
            return $meses[$month] . ' ' . $day . ', ' . $year;
        }
        catch(\Exception $e)
        {
            dd($e);
        }
    }
    /*
     * ============= Formato de hora =============
     */
    public function hourFormat($value)
    {
                        //12:00 PM
        return  strftime('%l:%M %p', strtotime($value));

    }
}