<?php namespace App\Http\Frontend\Traits;

trait EntityCommonScopes
{
    public function scopeWhereVisible($query) {
        return $query->where('visible',1);
    }

    public function scopeWithSlug($query,$slug) {
        return $query->where('slug',$slug);
    }

    /*
     * ============ Galería ============
     */
    public function scopeWithGallery($query)
    {
        return $query->with(['galleries' => function($gallery){
            $gallery->with(['elements' => function($elements){
                $elements->orderBy('position', 'asc');
            }]);
        }]);
    }

    /*
     * ============ Módulos ============
     */
    public function scopeWithCategoryModules($query)
    {
        return $query->with(['category' => function($category){
            $category->with(['modules' => function($modules){
                $modules->where('visible', true)
                    ->whereNotIn('type_slug', ['blockLeft'])
                    ->orderBy('position');
            }]);
        }]);
    }

    /*
     * ============ Relacionados por Ids ============
     */
    public function scopeWithIds($query)
    {
        return $query->with(['ids' => function($ids){
            $ids->with(['article' => function($article){
                $article->withGallery();
            }]);
        }]);
    }

    /*
     * ============ Categoría visible ============
     */
    public function scopeWhereHasCategory($query)
    {
        return $query->whereHas('category', function($category){
           $category->where('active', true);
        });
    }

    /*
     * ============ Ralcionados ============
     */
    public function scopeWhereHasRelated($query, $slug, $listId, $related, $articleId = null)
    {
        if($articleId)
            $query->where('id', '<>', $articleId);
        else
            $query->where('slug', '<>', $slug);

        switch($related)
        {
            case 'ids' :
                $query->whereIn('id', $listId);
                break;
            case 'tags':
                $query->whereHas($related, function($article) use($listId){
                    $article->whereIn('id', $listId);
                });
                break;
        }

        $query->where('active', true)
        ->orderByRaw("100*(UNIX_TIMESTAMP() ^ id) & 0xffff");
    }

}