<?php

Route::group(['namespace' => 'Frontend\Controllers'], function()
{
    /*
     * ====================== Home ======================
     */
    Route::get('/', 'HomeController@index');
    Route::get('home','HomeController@index');
    /*
     * ====================== Cronjob (Depurar visitas de notas) ======================
     */
    Route::get('debug-delete-visits','Controller@deleteVisits');
    /*
     * ====================== Buscador de notas ======================
     */
    Route::post('search','ArticlesController@search');
    
    /*
     * ====================== Encuestas ======================
     */
    Route::get('survey-votar','HomeController@surveyVotar');

    /*
     * ====================== Autores ======================
     */
    Route::get('autor/{slug}','AuthorsController@index');
    Route::post('autor/{slug}/more/{skip}','AuthorsController@moreNotes');

    /*
     * ================= Formulario de Contacto =================
     */
    Route::get('contacto','FormsController@index');
    Route::post('sendContact','FormsController@sendFormContact');
    Route::get('gracias','FormsController@thanks');

    /*
     * ================= Estaticas =================
     */
    Route::get('directorio','HomeController@directory');
    Route::get('nosotros','HomeController@statics');
    Route::get('aviso-privacidad','HomeController@statics');

    /*
     * ================= Ediciones impresas =================
     */
    Route::get('edicion-impresa/{slug?}','EditionsController@index');
    Route::any('edicion-impresa/{slug}/find', ['as' => 'edicion.show', 'uses' => 'EditionsController@find']);

    /*
     * ================= Anuncios clasificados =================
     */
    Route::Group(['prefix'=>'anuncios-clasificados'],function()
    {
        Route::get('/',                         'AdsController@index');
        Route::get('publicar',                  'AdsController@published');
        Route::post('publishing',               'AdsController@publishing');
        Route::post('search',                    'AdsController@search');
        Route::post('changeselect',             'AdsController@changeSelect');
        Route::get('detalles/{id}',             'AdsController@details');
        Route::post('{skip}',                   'AdsController@moreAds');
        Route::post('detalles/{id}/{skip}',     'AdsController@details');
        Route::get('categoria/{slug}',          'AdsController@category');
        Route::post('categoria/{skip}/{slug}',  'AdsController@moreAds');


    });
    /*
     * ====================== Notas (Siempre debe ir al final de todas las rutas) ======================
     */
    Route::get('/{category}/{subcategory?}/{slug?}',  ['as' => 'notes',  'uses'  => 'ArticlesController@index']);
    Route::post('notas/more/{id}/{skip}',  'ArticlesController@moreArticles');

    Route::post('minutebyminute/more/{skip}',  'ArticlesController@moreMinuteByMinute');
});