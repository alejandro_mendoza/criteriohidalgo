<?php namespace App\Http\Frontend\Helpers\Util;

use App\Http\Frontend\Repositories\ArticlesRepo;
use App\Http\Frontend\Repositories\CategoryRepo;
use App\Http\Frontend\Repositories\SurveysRepo;
use App\Http\Frontend\Repositories\EditionsRepo;
use App\Http\Frontend\Repositories\HomeRepo;
use App\Http\Frontend\Repositories\AdsRepo;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

class Frontend
{
    /*
     * ============= Categorías =============
     */
    public function getCategories()
    {
        $category = new CategoryRepo();

        return $category->getCategories();
    }

    /*
     * ============= Subcategorías =============
     */
    public function getSubcategories($parentId)
    {
        $category = new CategoryRepo();

        return $category->getSubcategories($parentId);
    }

    /*
     * ============= Definir ruta de categoría =============
     */
    public function getRoute($item, $article = null)
    {
        $category = array();

        $item->category->parent_id
            ? $category = [$item->category->parent->slug, $item->category->slug]
            : $category = [$item->category->slug]
        ;

        return $category + [2 => $article];
    }

    /*
     * ============= Módulo encuesta =============
     */
    public function modulePoll()
    {
        $poll = new SurveysRepo();

        return $poll->getSurvey();
    }

    /*
     * ============= Módulo minuto a minuto =============
     */
    public function moduleMinuteToMinute()
    {
        $minute = new HomeRepo();

        return $minute->getMinute();
    }

    public function moduleMinuteToMinuteCount()
    {
        $minute = new HomeRepo();

        return $minute->getMinuteCount();
    }

    /*
     * ============= Módulo encuesta =============
     */
    public function moduleEdition()
    {
        $editionRepo = new EditionsRepo();
        $parameters = Route::current()->parameters();
        $slug = is_array($parameters) && isset($parameters['category'])?$parameters['category']:'home';
        if (!in_array($slug, ['sos', 'ticket', 'la-copa'])) {
            $slug = 'home';
        }

        $edition = $editionRepo->getLastEditionBySlug($slug);

        if (!$edition) {
            $edition = $editionRepo->getLastEditionBySlug();
        }

        return $edition;
    }


    /*
     * ============= Módulo artículos recientes =============
     */
    public function moduleLastArticles($module)
    {
        $article    = new ArticlesRepo();
        $category   = new CategoryRepo();
        $categoryId = !empty($module)
            ? json_decode($module)->category
            : null;
        $cat = $category->find($categoryId) ? $categoryId : false;
        $limit = 25;

        if($cat) {
            $articles = Cache::remember(sprintf('moduleLastArticles_%s_%s_%s', $module, $cat, $limit), 5, function () use ($categoryId, $article, $limit){
                return  $article->getArticlesForCategory($categoryId, $limit);
            });


            if ($articles->count() > 0)
                return ['title' => $category->findField('id', $categoryId), 'content' => $articles];
            else
                return ['title' => null, 'content' => null];
        }
        else
            return ['title' => null, 'content' => null];
    }

    /*
     * ============= Módulo artículos más vistos =============
     */
    public function moduleMostViewed()
    {
        $article = new ArticlesRepo();

        $articles = $article->getArticlesMostViewed();

        if($articles->count() > 0)
            return ['title' => 'Lo más visto', 'content' => $articles];
        else
            return ['title' => null, 'content' => null];
    }

    /*
     * ============= Módulo galería de imágenes =============
     */
    public function moduleGallery($url = null)
    {
        if(!empty($url)){
            $note = $this->findArticle($url);
            $link = explode(':', json_decode($url)->article);
        }
        else
            return ['title' => null, 'content' => null, 'link' => null];

        return !empty($note)
            ? ['title' => $note->title, 'content' => $note->galleries->first(), 'link' => end($link)]
            : ['title' => null, 'content' => null, 'link' => null];
    }

    /*
    * ============= Módulo anuncios clasificados =============
    */
    public function moduleAds()
    {
        $ads    = new AdsRepo();
        return ['title' => 'Clasificados Recientes', 'content' => $ads->getModule()];
    }

    /*
     * ============= Sustituir shortcuts =============
     */
    public function replaceShortCuts($content, $shortcut)
    {
        $find       = array();
        $replace    = array();

        switch($shortcut)
        {
            case 'video':
                $search = '/{youtube}(.*){\/youtube}/U';
                $view   = 'Frontend.Article.Modules.video';
        }

        preg_match_all($search, $content, $result);

        if(count(array_filter($result)) > 1)
        {
            foreach($result[0] as $num => $item)
            {
                $find[] = "({$item})";
                $replace[] = view($view, ['content' => $result[1][$num]])->render();
            }
        }

        return preg_replace($find, $replace, $content);
    }

    /*
     * ============= Buscar artículo por url =============
     */
    private function findArticle($url)
    {
        $article = new ArticlesRepo();

        $url = !empty($url)
            ? explode('/', json_decode($url)->article)
            : [];

        return $article->getArticle(end($url));
    }
   
    /*
     * ============= Fecha =============
     */
    public function date($value)
    {
        setlocale(LC_ALL, 'es_ES.UTF-8');
        return  strftime('%A %d de %B de %Y', strtotime($value));
    }

    /*
     * ============= Fecha de publicación =============
     */
    public function datePublishing($value)
    {
        $meses = array("","Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic");

        $value = explode('-',$value);
        $year = $value[0];
        $month = $value[1];
        $day = $value[2];
        $day = substr($day,0,2);
        if($month < 10)
            $month = str_replace('0','',$month);
        try {
            return $meses[$month] . ' ' . $day . ', ' . $year;
        }
        catch(\Exception $e)
        {
            dd($e,$value);
        }
    }

    /*
     * ============= Formato de Fecha =============
     */
    public function dateFormat($value)
    {
        $meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $value = explode('-',$value);
        $year = $value[0];
        $month = $value[1];
        $day = $value[2];
        $day = substr($day,0,2);
        if($month < 10)
            $month = str_replace('0','',$month);
        try {
            return $day.'/'.$meses[$month].'/'.$year;
        }
        catch(\Exception $e)
        {
            dd($e,$value);
        }

    }

    /*
     * ============= Formato de Fecha =============
     */
    public function hourFormat($value)
    {
        return  strftime('%H:%M:%S %p', strtotime($value));
    }

    /*
     * ============= Numero clave del clima, by Yahoo =============
     */
    public function clima()
    {
        
/*        try {
            $BASE_URL = "http://query.yahooapis.com/v1/public/yql";
            $yql_query = 'select * from weather.forecast where woeid=135484';
            $yql_query_url = $BASE_URL . "?q=" . urlencode($yql_query) . "&format=json";
            $session = curl_init($yql_query_url);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            $json = curl_exec($session);
            $phpObj = json_decode($json);

            return $phpObj->query->results->channel->item->condition->code;
        }
        catch(\Exception $e){
            return '21.png';
        }
*/
    }

    /*
     * ============= Formato de Fecha =============
     */
    public function formatMonth($value)
    {
        $meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $value = explode('-',$value);
        $year = $value[0];
        $month = $value[1];
        $day = $value[2];
        $day = substr($day,0,2);
        if($month < 10)
            $month = str_replace('0','',$month);
        try {
            return $meses[$month].' '.$year;
        }
        catch(\Exception $e)
        {
            dd($e,$value);
        }

    }
    
    /*
     * ============= Mostrar banner =============
     */
    public function bannerH($slug,$pos){
        $category   = new CategoryRepo();
        $banner = $category->getBanner($slug, $pos);
        
        return base64_decode($banner);
    }
}
