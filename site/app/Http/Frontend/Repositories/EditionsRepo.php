<?php

namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Edition;
use Illuminate\Support\Facades\DB;

class EditionsRepo extends BaseRepo
{
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Edition;
    }

    /*
     * AUTOR
     * @param $slug
     * @return Autor con notas publicadas
     */
    public  function getEdition($date)
    {
        $array = explode('/', $date);
        if(empty($array[0]))
            $array = explode('-', $date);
        $new_date = $array[2].'-'.$array[1].'-'.$array[0];

        $edit = $this->getModel()
             ->where('date_publishing',$new_date)//identificamos al autor por su slug
             ->first();
        if($edit)
            return $edit;
        else
            return $date;
        
    }

    /*
     * AUTOR
     * @param $slug
     * @return Autor con notas publicadas
     */
    public  function getEditionByDateAndSlug($date, $slug)
    {
        $new_date = \DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d');

        $edit = Edition::From('editions as e')
            ->select('e.*')
            ->join('categories as c', 'c.id', '=', 'e.category_id')
            ->where('c.slug', '=', $slug)
            ->where('e.active', 1)
            ->where('e.date_publishing', $new_date)
            ->first();

        if($edit)
            return $edit;
        else
            return $date;

    }

    /*
     * ============ Última edición creada ============
     */
    public function getLastEdition()
    {
        return $this->model
            ->where('active', true)
            ->orderBy('date_publishing', 'desc')
            ->first();
    }

    /*
    * ============ Última edición creada ============
    */
    public function getLastEditionBySlug($slug = 'home')
    {
        return $edit = Edition::From('editions as e')
            ->select('e.*')
            ->join('categories as c', 'c.id', '=', 'e.category_id')
            ->where('c.slug', '=', $slug)
            ->where('e.active', 1)
            ->orderBy('e.date_publishing', 'desc')
            ->first();
    }

    /*
     * Ultima edicion
     * @return ultima edicion
     */
    public  function getEditionRecent()
    {        
        return $this->getModel()
             ->where('active',1)->orderBy('date_publishing','desc')
             ->first();
    }

    /*
     * Ultima edicion
     * @return ultima edicion
     */
    public  function getEditionRecentByCategorySlug($slug)
    {
        return Edition::From('editions as e')
            ->select('e.*')
            ->join('categories as c', 'c.id', '=', 'e.category_id')
            ->where('c.slug', '=', $slug)
            ->where('e.active', 1)
            ->orderBy('e.date_publishing', 'desc')
            ->first();
    }
}
