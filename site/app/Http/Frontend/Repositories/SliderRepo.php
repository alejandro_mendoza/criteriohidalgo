<?php

namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Gallery;

class SliderRepo extends BaseRepo
{
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Gallery;
    }

    /*
     * SLIDER
     * @return slider con elementos
     */
    public  function getSlider()
    {
        return $this->getModel()
             ->where('name','MainSlider')
             ->with('elements')
             ->orderBy('position','asc')   
             ->get()
             ->toArray();
    }
}
