<?php namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Visit;

class VisitRepo extends BaseRepo
{
    protected $cookie;
    protected $expirate;

    function __construct()
    {
        parent::__construct();

        $this->cookie   = isset($_COOKIE['_chvisit']) ? $_COOKIE['_chvisit'] : 0;
        $this->expirate = 7;

        //$this->cleanVisits();
    }

    /*
     * ================ Instanciar modelo ================
     */
    public function getModel()
    {
        return new Visit;
    }

    /*
     * ================ Registrar visita al artículo ================
     */
    public function setVisit($article)
    {
	/*if(empty($this->cookie))
        {
            $article->views = $article->views + 1;
            $article->save();
        }
        else
        {
            if(empty($this->getVisit($article->id)))
            {
                $this->model
                    ->fill([
                        'article_id'    => $article->id,
                        'cookie'        => $this->cookie,
                        'date_expirate' => date("Y-m-d H:i:s", time())
                    ])->save();

                $article->views = $article->views + 1;
                $article->save();
            }
        }*/
    }

    /*
     * ================ Obtener visita al artículo ================
     */
    public function getVisit($articleId)
    {
        return $this->model
            ->where('article_id', $articleId)
            ->where('cookie', $this->cookie)
            ->first();
    }

    /*
     * ================ Limpiar visitas ================
     */
    public function cleanVisits()
    {
        $visits = $this->model
            ->having(\DB::raw("DATEDIFF(CURDATE(), date_expirate)"), '>', $this->expirate)
            ->limit(2000)
            ->get();

        foreach($visits as $visit)
            $visit->where('article_id', $visit->article_id)
                ->delete();
    }

}
