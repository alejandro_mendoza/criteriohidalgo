<?php namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Article;
use App\Http\Entities\Category;
use Faker\Provider\tr_TR\DateTime;
use Illuminate\Support\Facades\Cache;

class ArticlesRepo extends BaseRepo
{
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Article;
    }

    /*
     * ============== Obtener datos de artículo ==============
     */
    public  function getArticle($slug, $categoryId = null)
    {
        $article = $this->getModel()->where('active', true);

        !is_null($categoryId)
            ? $article->where('category_id', $categoryId)->where('principal', true)
            : $article->where('slug', $slug);

        $article->withGallery()
            ->whereHasCategory()
            ->withCategoryModules()
            ->with('authors', 'ids', 'tags')
            ->orderBy('date_publishing', 'desc');

        return Cache::remember(sprintf('article_%s_%s', $slug, $categoryId), 5, function () use ($article) {
            return $article->first();
        });
    }

    /*
     * ============== Obtener lista de artículos por categoría ==============
     */
    public function getArticlesForCategory($categoryId, $take, $limit = null)
    {
        $principal  = $this->getArticle(null, $categoryId);

        $articles = $this->model
            ->withGallery()
            ->with(['category' => function($category){
                $category->with('parent');
            }, 'authors', 'ids', 'tags'])
            ->where('active', true)
            ->orderBy('date_publishing', 'desc')
            ->where('date_publishing','<=',  date('Y/m/d'))
            ->skip($limit)
            ->take($take);

        #-------------- Categoría --------------
        $this->whereCategory($articles, $categoryId);

        #-------------- Omitir nota principal --------------
        !empty($principal) ? $articles->whereNotIn('id', [$principal->id]) : null;

        return Cache::remember(sprintf('articlesForCategories_%s_%s_%s', $categoryId, $take, $limit), 5, function () use ($articles) {
            return $articles->get();
        });
    }

    /*
     * ================== Categorías ==================
     */
    private function whereCategory($articles, $categoryId)
    {
        $categories = array();
        $category   = Category::find($categoryId);

        foreach($category->getDescendantsAndSelf() as $item){
            $cat   = Category::find($item->id);
            if($cat->active == 1)
                $categories[] = $item->id;
        }

        #-------------- Categoría raíz --------------
        return $category->isRoot()
            ? $articles->whereIn('category_id', $categories)
            : $articles->where('category_id', $categoryId)
        ;
    }

    /*
     * ================== Obtener Artículos Relacionados ==================
     */
    public function getArticlesRelated($article, $related = null)
    {
        $listId = array();

        foreach($article->$related as $item) $listId[] = $related == 'ids' ? $item->article_id : $item->id;

        return Cache::remember(sprintf('articlesForCategories_%s_%s', $article, $related), 5, function () use ($article, $listId, $related) {
            return $this->model
                ->withGallery()
                ->whereHasRelated($article->slug, $listId, $related, $article->id)
                ->where('date_publishing', '<=', date('Y/m/d'))
                ->orderBy('date_publishing', 'desc')
                ->limit(4)
                ->get();
        });
    }

    /*
     * ================== Obtener Artículos más vistos ==================
     */
    public function getArticlesMostViewed()
    {

        return Cache::remember('articles_most_viewed', 5, function () {
            $dateFrom = (new \DateTime('today'))->modify('-3 days');
                return $this->model
                    ->withGallery()
                    ->where('active', true)
                    ->where('date_publishing', '>=', $dateFrom)
                    ->orderBy('views', 'desc')
                    ->limit(4)
                    ->get();
        });
    }
    
    public function getCounts($categoryId)
    {
        $articles = $this->model->where('active', true);

        #-------------- Categoría --------------
        $this->whereCategory($articles, $categoryId);

        return $articles->count();
    }
    
    /*
     * BUSCADOR DE NOTAS
     * @param $request
     * @return Resultados que coinciden con titulo e introduccion
     */
    public function search($request)
    {
        $find = $request->get('search');
        $isToday = false;

        if (preg_match('/[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}/', $find)) {
            $date = \DateTime::createFromFormat('Y/m/d', $find)->setTimezone(new \DateTimeZone('America/Mexico_City'));
            if ($date->setTime(0,0,0) == new \DateTime('today')) {
                $isToday = true;
            }
        }

        $timeExpire = $isToday?5:360000;
    
        /// unimos los resultados
        $result = Cache::remember(sprintf('articles_find_%s', $find), $timeExpire, function () use ($find) {

            $result = Article::From('articles as a');
            $result
                ->select('a.*')
                ->where('a.active', '=', 1)
                ->where('a.date_expire', '>=', date('Y/m/d'))
            ;


            if (preg_match('/[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}/', $find)) {
                $date = \DateTime::createFromFormat('Y/m/d', $find)->setTimezone(new \DateTimeZone('America/Mexico_City'));
                $result->whereBetween('date_publishing', [$date->setTime(0,0,0)->format('Y-m-d H:i:s'), $date->setTime(23,59,59)->format('Y-m-d H:i:s')]);
            } else {
                $result
                    ->join('taggables as tb', 'tb.taggable_id', '=', 'a.id')
                    ->join('tags as t', 't.id', '=', 'tb.tag_id')
                    ->join('authors as au', 'au.id', '=', 'a.author_id')
                    ->where(function ($query) use ($find) {
                        $query
                            ->where('a.title', 'like', "%{$find}%")
                            ->orWhere('t.value', 'like', "%{$find}%")
                            ->orWhere('au.name', 'like', "%{$find}%");
                    })

                ;
            }

            $result = $result
                ->orderBy('a.created_at', 'desc')
                ->distinct()
                ->get();

            return $result;
        });
        
        return $result;
    }
    
    /*
     * CONSULTA PARA BUSQUEDA
     * @param $campo, $palabra clave
     * @return query
     */
    private function getQuery($campo,$find)
    {
        return $this->getModel()
            ->whereHas('category', function($category){
                $category->where('active', true);
            })
            ->where('active',1)
            ->where('date_publishing','<=',  date('Y/m/d'))
            ->where($campo,'LIKE',"%{$find}%");
    }
}
