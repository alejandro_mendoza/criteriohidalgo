<?php

namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Survey;
use App\Http\Entities\Option;

class SurveysRepo extends BaseRepo
{
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Survey;
    }

    /*
     * ENCUESTA ACTUAL
     * @return Encuesta actual con las opciones
     */
    public  function getSurvey()
    {
        return $this->getModel()
             ->where('current',true)
             ->with('options')
             ->first();
    }
    
    /*
     * ASIGNAR VOTO
     * @param $id
     * @return Encuesta con voto asignado
     */
    public function setVoto($id)
    {
        $opt = Option::find($id);
        $opt->counter = $opt->counter + 1;
        $opt->update();
        session()->put('frontend.survey.voted',true); // Sesion que se activa cuando se vota
        return $this->getModel()
                    ->where('id',$opt->survey_id)
                    ->with('options')
                    ->first();
    }
}
