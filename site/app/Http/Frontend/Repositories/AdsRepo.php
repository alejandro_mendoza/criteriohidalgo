<?php

namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Ads;
use App\Http\Entities\Classified;
use App\Http\Entities\Sub_Classified;
use App\Http\Backend\Helpers\Files as file;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Collection;

class AdsRepo extends BaseRepo
{
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Ads;
    }

    /*
     * ULTIMOS ANUNCIOS
     * @param $skip, $slug
     * @return anuncios
     */
    public function getRecents($skip,$slug)
    {
        $query = $this->getModel()
                ->where('active',1);
        if($slug != ''){
            $id = Sub_Classified::where('slug', $slug)->first();
            $query = $query->where('sub_category_id', $id->id);
        }
        
        return $query->orderBy('created_at','desc')
                ->skip($skip)
                ->take(9)
                ->get();
    }

    
    /*
     * Contador de anuncios
     * @param $slug
     * @return Total de anuncios
     */
    public function getCounts($slug)
    {
        $query = $this->getModel()
                ->where('active',1);
        if($slug != ''){    //// Entra cuando trae una categoria
            $id = Sub_Classified::where('slug', $slug)->first();
            $query = $query->where('sub_category_id', $id->id);
        }
        
        return $query->count();
    }


    /*
     * CATEGORIAS
     * @param $slug
     * @return Todas las categorias
     */
    public  function getCategories()
    {
        return Classified::all();
        
    }
    
    /*
     * GUARDA ANUNCIOS
     * @param $request de formulario
     * @return id del anuncio
     */
    public function setAds($request)
    {
        $ads = $this->getModel();   
        $data = $request->except('_token$this->separatoroptions');
        
        $files = new file();
        $file = $files->uploadFile([
            'files'     => $request->only('image'),
            'route'     => "images/ads",
            'entity'    => $ads
        ]);
        if(!empty($file)) $data['image'] = $file;
        $data['user'] = $data['name'];
        $data['active'] = '0';
        $ads->fill($data)->save();

	// redimencionamos la imagen
        $imagen = getimagesize(public_path("images/ads/{$file}"));    //Obtenemos la imagen
        $ancho = $imagen[0];              //Ancho
        $alto = $imagen[1];               //Alto
        if ($ancho > 550) {
            Image::make(public_path("images/ads/{$file}"))
                    ->resize(550, null, function ($constraint) {
                    $constraint->aspectRatio();
            })->save(public_path("images/ads/{$file}"));
        }

        return $ads->id;
    }
    
    /*
     * DETALLES DE ANUNCIO
     * @param $id
     * @return Anuncio
     */
    public function getDetails($id)
    {
        $ad = $this->getModel()
                ->where('id',$id)
                ->with('category')
                ->first();
        return $ad ? $ad : abort(404);
    }
    
    /*
     * ANUNCIOS FILTRADOS POR CATEGORIAS
     * @param $anuncio, $skip
     * @return Notas con categorias relacionadas
     */
    public function getByCategory($ad,$skip)
    {
        return Classified::where('id',$ad->category->category->id)
                ->with([
                    'sub_categories' => function($sub) use($ad,$skip){
                             $sub->with(['elements' => function($element) use ($ad,$skip){
                                 $element->where('id','<>',$ad->id)
                                         ->skip($skip)
                                         ->take(9);
                             }]);
                             }
                ])
                ->get();
    }
    
    /*
     * CONTADOR DE ANUNCIOS POR CATEGORIAS
     * @param $anuncio
     * @return total de anuncios
     */
    public function getCountsByCategory($ad)
    {
        $ads = Classified::where('id',$ad->category->category->id)
                ->with([
                    'sub_categories' => function($sub) use($ad){
                             $sub->with(['elements' => function($element) use ($ad){
                                 $element->where('id','<>',$ad->id)->count();
                             }]);
                             }
                ])
                ->first();
                $count = 0;
        foreach($ads->sub_categories as $item)
        {
            $count += (int)$item->elements->count();
        }
        return $count;
        
    }
    
    /*
     * BUSCADOR DE ANUNCIOS
     * @param $request
     * @return Resultados que coinciden con titulo y descripcion
     */
    public function search($request)
    {
        $find = $request->get('searchAds');
    /// buscamos por titulo
        $queryTitle = $this->getQuery('title',$find);
    /// buscamos por titulo, solo id
        $queryTitleIds = $this->getQuery('title',$find);

    /// obtenemos los id de los resultados
        $resultTitle = collect($queryTitleIds->select('id')->get()->toArray());
    /// unimos los ID's en un mismo array
        $resultTitle->pluck('id')->toArray();
    
    /// buscamos por descripcion
        $queryContent = $this->getQuery('description',$find);
        $resultContent = $queryContent->whereNotIn('id',$resultTitle);
    
    /// unimos los resultados
        $result = collect($queryTitle->orderBy('created_at','desc')->get());
        $result = $result->merge($queryContent->orderBy('created_at','desc')->get());
        
        
        return $result;
    }
    
    /*
     * CONSULTA PARA BUSQUEDA
     * @param $campo, $palabra clave
     * @return query
     */
    private function getQuery($campo,$find)
    {
        return $this->getModel()
                ->where('active',1)
                ->where($campo,'LIKE',"%{$find}%");
    }
    
    /*
     * SUBCATEGORIAS
     * @param $id
     * @return subcategorias
     */
    public function getSubs($id)
    {
        return Sub_Classified::where('category_id',$id)->get();
    }

    public function getModule()
    {
        $query = $this->getModel()
            ->where('active',1);

        return $query->orderBy('created_at','desc')
            ->take(5)
            ->get();
    }
}
