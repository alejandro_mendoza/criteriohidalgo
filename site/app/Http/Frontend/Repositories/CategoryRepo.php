<?php namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Category;
use Illuminate\Support\Facades\Cache;

class CategoryRepo extends BaseRepo
{
    /*
     * ================ Instanciar modelo ================
     */
    public function getModel()
    {
        return new Category();
    }

    /*
     * ================ Obtener categoría ================
     */
    public function getCategory($categoryId)
    {
        return Cache::remember(sprintf('category_%s', $categoryId), 5, function () use ($categoryId) {
            return $this->model
                ->with(['modules' => function ($modules) {
                    $modules->whereNotIn('type_slug', ['blockLeft'])
                        ->where('visible', true)
                        ->orderBy('position', 'asc');
                }], 'parent')
                ->where('id', $categoryId)
                ->where('active', true)
                ->first();
        });
    }

    /*
     * ================ Obtener categorías ================
     */
    public function getCategories()
    {
        return Cache::remember('categories', 5, function () {
            return $this->model
                ->where('depth', false)
                ->where('active', true)
                ->where('hidden', false)
                ->orderBy('position', 'asc')
                ->get();
        });
    }

    /*
     * ================ Obtener subcategorías ================
     */
    public function getSubcategories($parentId)
    {
        return Cache::remember(sprintf('subcategories_%s', $parentId), 5, function () use ($parentId) {
            return $this->model
                ->where('depth', 1)
                ->where('parent_id', $parentId)
                ->where('active', 1)
                ->where('hidden', false)
                ->orderBy('position', 'asc')
                ->get();
        });
    }

    /*
     * Banners publicitarios
     *
     * @param slug de la categoria, posicion del banner
     * @return  codigo del banner
     */
    public function getBanner($slug,$pos)
    {
        $category = $this->model
                    ->where('slug',$slug)
                    ->first();
        
        $banner = !empty($category->params) ? json_decode($category->params) : null;
        
        if($banner == null)
            return null;
        return $banner[$pos]->one;
    }
}

