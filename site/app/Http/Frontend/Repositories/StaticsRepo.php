<?php

namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Statics;

class StaticsRepo extends BaseRepo
{
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Statics;
    }

    public function getSections($slug){
        return $this->getModel()
            ->with('elements')
            ->where('slug', $slug)
            ->first()
        ;
    }
}
