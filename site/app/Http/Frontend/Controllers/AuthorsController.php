<?php namespace App\Http\Frontend\Controllers;

use App\Http\Frontend\Repositories\AuthorsRepo;
use App\Http\Frontend\Repositories\HomeRepo;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{
    /*
     * PRINCIPAL DEL AUTOR
     * @param $slug
     * @return vista
     */
    public function index(AuthorsRepo $authorRepo, HomeRepo $homeRepo,$slug, $skip = 0)
    {
        //dd($authorRepo->getAuthor($slug));
        
        //dd(session('frontend.author'));
	$data = [
            'author'        =>  $authorRepo->getAuthor($slug,$skip),//obtenemos el autor, con las notas relacionadas
            'title'         =>  'Autores',
            'modulesR'      =>  $homeRepo->getModulesR(),
            'count'         => $authorRepo->getCounts($slug),
            'skip'          => $skip
        ];
        //dd(session('frontend.authors'));
	return view('Frontend.Author.index',$data);
    }
    
    public function moreNotes(AuthorsRepo $repo, $slug, $skip)
    {
        $data = [
            'author'         => $repo->getAuthor($slug,$skip),
        ];
	return view('Frontend.Author.note',$data)->render();
    }
}