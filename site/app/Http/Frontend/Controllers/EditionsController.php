<?php namespace App\Http\Frontend\Controllers;

use App\Http\Frontend\Repositories\EditionsRepo;
use App\Http\Frontend\Requests\EditionRequest;
use Illuminate\Http\Request;

class EditionsController extends Controller
{
    /*
     * PRINCIPAL DEL AUTOR
     * @param $slug
     * @return vista
     */
    public function index(EditionsRepo $repo, $slug = 'home')
    {
        //dd($repo->getEdition($date));

        $data = [
                'edition'       =>  $repo->getEditionRecentByCategorySlug($slug),//obtenemos el autor, con las notas relacionadas
                'title'         =>  'Edicion Impresa',
            ];
        //dd(session('frontend.authors'));
	    return view('Frontend.Editions.index',$data);
    }
    
    /*
     * PRINCIPAL DEL AUTOR
     * @param $slug
     * @return vista
     */
    public function find(EditionsRepo $repo, $slug = 'home', EditionRequest $request)
    {
        //dd($request->get('date_publishing'));
        //dd($repo->getEdition($request->get('date_publishing')));

        $data = [
                'edition'       =>  $repo->getEditionByDateAndSlug($request->get('date_publishing'), $slug),//obtenemos el autor, con las notas relacionadas
                'title'         =>  'Edicion Impresa',
            ];

        return view('Frontend.Editions.index',$data);
    }
}