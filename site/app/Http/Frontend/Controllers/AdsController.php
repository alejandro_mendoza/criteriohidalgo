<?php namespace App\Http\Frontend\Controllers;

use Illuminate\Http\Request;
use App\Http\Frontend\Requests\AdsRequest;
use App\Http\Frontend\Repositories\AdsRepo;
use App\Http\Frontend\Repositories\HomeRepo;
use App\Http\Frontend\Repositories\FormsRepo;
use App\Http\Frontend\Requests\SearchAdsRequest;


class AdsController extends Controller
{
    /*
     * PRINCIPAL DE ANUNCIOS CLASIFICADOS
     * @param $skip, $slug
     * @return vista INDEX
     */
    public function index(AdsRepo $repo, HomeRepo $homeRepo, $skip = 0, $slug = '')
    {
	$data = [
            'title'         => 'Anuncios clasificados',
            'ads'           => $repo->getRecents($skip,''),
            'categories'    => $repo->getCategories(),
            'count'         => $repo->getCounts(''),
            'modulesR'      => $homeRepo->getModulesR(),
            'skip'          => $skip,
            'slug'          => $slug
        ];
        
	return view('Frontend.Ads.index',$data);
    }
    
    /*
     * PUBLICAR UN ANUNCIO
     * @return vista PUBLICAR
     */
    public function published(AdsRepo $repo,FormsRepo $formRepo)
    {
        $data = [
            'title'         => 'Publicar',
            'categories'    => $repo->getCategories(),
            'form'          => $formRepo->getForm(),
        ];
        return view('Frontend.Ads.published',$data);
    }
    
    /*
     * PROCESO: PUBLICANDO UN ANUNCIOS
     * @return vista GRACIAS
     */
    public function publishing(AdsRequest $request,AdsRepo $repo)
    {
        $repo->setAds($request);
        $data = [
            'title'         =>  'Gracias',
            'msj'           => 'Tus datos se han compartido correctamente.'
        ];
        return view('Frontend.Template.thanks',$data);
    }
    
    /*
     * DETALLES DE ANUNCIOS 
     * @param $id, $skip
     * @Ajax Cargar mas
     * @return vista DETALLES
     */
    public function details(AdsRepo $repo, HomeRepo $homeRepo, $id, $skip = 0)
    {
        $ad = $repo->getDetails($id);
        $data = [
            'title'         => 'Anuncios clasificados',
            'ad'            => $ad,
            'ads'           => $repo->getByCategory($ad,$skip),
            'categories'    => $repo->getCategories(),
            'modulesR'      => $homeRepo->getModulesR(),
            'count'         => $repo->getCountsByCategory($ad),
            'skip'          => $skip
        ];
        if(\Request::ajax())
            return view('Frontend.Ads.recents',$data)->render();
        else    
            return view('Frontend.Ads.details',$data);
    }
    
    /*
     * CARGAR MAS ANUNCIOS CLASIFICADOS
     * @param $skip, $slug
     * @Ajax Anuncios
     */
    public function moreAds(AdsRepo $repo, $skip, $slug = '')
    {
        $data = [
            'ads'         => $repo->getRecents($skip,$slug),
        ];
	return view('Frontend.Ads.ads',$data)->render();
    }
    
    /*
     * ANUNCIOS CLASIFICADOS POR CATEGORIA
     * @param $slug, $skip
     * @return vista INDEX
     */
    public function category(AdsRepo $repo, HomeRepo $homeRepo, $slug, $skip = 0)
    {
        $data = [
            'title'         => 'Anuncios clasificados',
            'ads'           => $repo->getRecents($skip,$slug),
            'categories'    => $repo->getCategories(),
            'count'         => $repo->getCounts($slug),
            'modulesR'      =>  $homeRepo->getModulesR(),
            'skip'          => $skip,
            'slug'          => $slug
        ];
	return view('Frontend.Ads.index',$data);
    }
    
    /*
     * BUSCADOR DE ANUNCIOS CLASIFICADOS
     * @return vista RESULTADOS
     */
    public function search(AdsRepo $repo,SearchAdsRequest $request)
    {
        $data = [
            'title'       => 'Anuncios clasificados',
            'ads'         => $repo->search($request),
        ];
	return view('Frontend.Ads.results',$data);
    }
    
    /*
     * SUB CATEGORIAS
     * @return vista Ajax
     */
    public function changeSelect(AdsRepo $repo,Request $request)
    { 
	$data = ['categories' => $repo->getSubs($request->get('id'))];
        return view('Frontend.Ads.categories',$data)->render();
    }
}