<?php namespace App\Http\Frontend\Controllers;

use App\Http\Frontend\Repositories\VisitRepo;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        !isset($_COOKIE['_chvisit']) ? setcookie('_chvisit', str_random(20), strtotime( '+7 days' ), '/', false) : null;
    }

    /*
     * ================== Página no encontrada ==================
     */
    public function notFoundUnless($value)
    {
        return !$value ? abort(404) : $value;
    }

    /*
     * ================== Cronjob (Depurar visitas de notas) ==================
     */
    public function deleteVisits(VisitRepo $visit)
    {
        $visit->cleanVisits();
    }
}
