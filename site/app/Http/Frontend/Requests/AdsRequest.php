<?php namespace App\Http\Frontend\Requests;

use Illuminate\Routing\Route;

class AdsRequest extends Request
{
    protected $rules = [
        'title'          => 'required|max:255',
        'description'    => 'required|max:255',
        'image'          => 'required|mimes:jpg,png,jpeg',
        'phone'          => 'required|max:25',
        'mail'           => 'required|email',
        'sub_category_id'=> 'required',
        'category'       => 'required',
        'name'           => 'required|max:255'
    ];

    public function rules(Route $route)
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 