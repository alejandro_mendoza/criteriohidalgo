<?php namespace App\Http\Frontend\Requests;

use Illuminate\Routing\Route;

class EditionRequest extends Request
{
    protected $rules = [
        'date_publishing'  => 'required',
    ];

    public function rules(Route $route)
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 