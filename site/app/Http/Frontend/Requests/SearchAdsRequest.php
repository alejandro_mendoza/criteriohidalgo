<?php namespace App\Http\Frontend\Requests;

use Illuminate\Routing\Route;

class SearchAdsRequest extends Request
{
    protected $rules = [
        'searchAds'    => 'required|max:255',
    ];

    public function rules(Route $route)
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 