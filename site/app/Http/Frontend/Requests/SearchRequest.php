<?php namespace App\Http\Frontend\Requests;

use Illuminate\Routing\Route;

class SearchRequest extends Request
{
    protected $rules = [
        'search'    => 'required|max:255',
    ];

    public function rules(Route $route)
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 