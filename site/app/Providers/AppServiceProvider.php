<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Http\Entities\Article;
use App\Http\Frontend\Helpers\WebSocket;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('email_list','App\Http\Backend\Helpers\CustomValidator@emailList');
        Validator::extend('option_list','App\Http\Backend\Helpers\CustomValidator@optionList');
        Validator::extend('google_captcha_keys','App\Http\Backend\Helpers\CustomValidator@googleCaptchaKeys');
        Validator::extend('recaptcha','App\Http\Backend\Helpers\Recaptcha@validate');
        
        /*
         * Evento para Web Socket (Minuto a minuto)
        */ 
        Article::created(function($article) {
            if($article->date_publishing <= date('Y-m-d')){          // fecha de publicacion 
                    if($article->active == 1){                       // activa 
                        WebSocket::sendMessage($article);
                    }
            }
            if($article->date_expire == ''){
               $article->date_expire = '2055-12-30';
               $article->save();
           }
        });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
