<div class="module_column">
    <h2 class="rightTitle">Columna: Juan Villoro</h2>
    <div class="grid">
        <figure class="effect-ming">
            <img src="/img/right/demo-columna.jpg" alt="Columna">
            <figcaption>
                <p>Ver columna <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> </p>
                <a href="http://www.google.com">Ver más</a>
            </figcaption>			
        </figure> 
    </div>
</div>