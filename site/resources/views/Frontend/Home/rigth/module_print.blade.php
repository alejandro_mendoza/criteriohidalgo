<div class="module_print">
    <h2 class="rightTitle">Edición Impresa</h2>
    <div class="grid print">
        <figure class="effect-ming">
            <img src="/img/right/demo-impreso.jpg" alt="Demo">
            <figcaption>
                <p>Ver edición impresa <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> </p>
                
                <a href="/edicion-impresa">Ver más</a>
            </figcaption>			
        </figure> 
    </div>
</div>