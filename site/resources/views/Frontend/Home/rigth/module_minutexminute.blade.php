<div class="module_minute" style="max-height: 480px; overflow-x: hidden; overflow-y: auto;">
    <h2 class="rightTitle">Minuto a minuto</h2>
    
    <!-- EVENTO -->
    <div id="minuteByMinuteContainer" class="module_minute">
        @include('Frontend.MinuteByMinute.more', ['minute' => $minute])
    </div>



    
</div>

<div id="loadMoreButtonContainer" class="module_notes_category loadMoreMinuteButtonContainer">
    <div id="btnMore" class="btnMoreMinute">
        <a onclick="moreMinuteByMinute('/minutebyminute/more/', '{{ $minute[count($minute) - 1]['dateP']  }}', '{{ $minuteCount }}');">VER MÁS</a>
    </div>
</div>