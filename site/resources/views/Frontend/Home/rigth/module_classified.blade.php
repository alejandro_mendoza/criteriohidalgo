<div class="module_classified">
    <div class="advertise">
        <img src="/img/right/banner-clasificados.jpg" alt="Banner">      
    </div>
    
    <div class="adds">
        <div class="row">
            <div class="col-xs-12">
                <h2>Clasificados Recientes</h2>
            </div>
        </div>
        
        <!-- ANUNCIO -->
        <div class="row add">
           <a href="#" class="addHover">
                <div class="col-xs-3 col-sm-2 col-md-3">
                    <div class="cropImg">
                        <img src="/img/videogame/batman.jpg" alt="Add">
                    </div>
                </div>
                <div class="col-xs-9 col-sm-10 col-md-9">
                    <h3>Terreno en San Antonio</h3>
                    <p>700 M2 / Hidalgo, México</p>
                </div>
            </a>
        </div>
        
        <div class="row add">
           <a href="#" class="addHover">
            <div class="col-xs-3 col-sm-2 col-md-3">
                <div class="cropImg">
                    <img src="/img/videogame/tomb.jpg" alt="Add">
                </div>
            </div>
            <div class="col-xs-9 col-sm-10 col-md-9">
                <h3>Dos lotes en tizayuca</h3>
                <p>900 M2 / Hidalgo, México</p>
            </div>
            </a>
        </div>
        
        <div class="row add">
           <a href="#" class="addHover">
            <div class="col-xs-3 col-sm-2 col-md-3">
                <div class="cropImg">
                    <img src="/img/videogame/gears4.jpg" alt="Add">
                </div>
            </div>
            <div class="col-xs-9 col-sm-10 col-md-9">
                <h3>Vendo casa de campo</h3>
                <p>Amueblada / Hidalgo, México</p>
            </div>
            </a>
        </div>
        
        <div class="row add">
           <a href="#" class="addHover">
            <div class="col-xs-3 col-sm-2 col-md-3">
                <div class="cropImg">
                    <img src="/img/destacados/car-1.jpg" alt="Add">
                </div>
            </div>
            <div class="col-xs-9 col-sm-10 col-md-9">
                <h3>TIDA 2013 seminuevo</h3>
                <p>500,000 KM / Único dueño</p>
            </div>
            </a>
        </div>
        
        <div class="row add">
           <a href="#" class="addHover">
            <div class="col-xs-3 col-sm-2 col-md-3">
                <div class="cropImg">
                    <img src="/img/destacados/car-4.jpg" alt="Add">
                </div>
            </div>
            <div class="col-xs-9 col-sm-10 col-md-9">
                <h3>Empleo / Asistente Dir.</h3>
                <p>Pachuca Hidalgo / Crecimiento</p>
            </div>
            </a>
        </div>
    </div>
</div>