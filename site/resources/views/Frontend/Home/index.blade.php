@extends('master')
@section('title'){{ $title }} - Criterio Hidalgo @endsection
@section('content')   


    
<main id="homePage">
    @include('Frontend.Home.left.module_banner', ['slug' => 'home', 'pos' => '0'])
    <!-- Slider principal  --> 
    @if(!empty($slider))
        @if(!empty($slider[0]))
            @include('Frontend.Home.Slider.demo'.$slider[0]['directory'],$slider)
        @endif
    @endif    
    
   <div id="container">
        <div class="row">
            <div class="col-xs-12" id="date">
                PACHUCA, HIDALGO - {{ strtoupper(\Frontend::formatMonth(date('Y-m-d'))) }}
            </div>
            
            <div class="row">
                <!-- CONTENIDO PRINCIPAL HOME (IZQUIERDO)-->
                <div  class="col-xs-12 col-md-8" id="mainContent">

                        
                   @include('Frontend.Home.left.module_banner', ['slug' => 'home', 'pos' => '1'])
                   
                   <!-- notas Destacadas -->
                   @if(!empty($featuring))
                        @include('Frontend.Home.left.module_main',$featuring)
                        
                   @endif
                   
                  <!-- notas Recientes Ticket -->
                   @if(!empty($recent[0]))
                        @include('Frontend.Home.left.module_ticket',$recent[0])
                   @endif
                
                   @include('Frontend.Home.left.module_banner', ['slug' => 'home', 'pos' => '2'])

                   <!-- notas Recientes Estilo Fem -->
                   @if(!empty($recent[1]))
                       @include('Frontend.Home.left.module_style',$recent[1])
                   @endif

                   @include('Frontend.Home.left.module_banner', ['slug' => 'home', 'pos' => '3'])

                   <!-- notas Recientes Galeria -->
                   @if(!empty($recent[2]))
                        @include('Frontend.Home.left.module_gallery')
                   @endif

                   <!-- notas Recientes Deportes -->
                   @if(!empty($recent[3]))
                        @include('Frontend.Home.left.module_sport')
                   @endif
                   
                </div>


                <!-- CONTENIDO PRINCIPAL HOME (DERECHA)-->
                  
                <div class="col-xs-12 col-md-3 col-md-offset-1 col-lg-4 col-lg-offset-0" id="rightContent">
                  @if($modulesR != null)
                    @if(!$modulesR->modules->isEmpty())
                        @foreach($modulesR->modules as $item)
                           @include("Frontend.Category.Modules.{$item->type_slug}")
                        @endforeach
                     @endif
                  @endif
                </div>
            </div>
        </div>
    </div>
    
    
      
    
</main>

@include('Frontend.Template.footer')
        
    <!-- CAROUSEL PLUGINS -->
    <script src="../js/plugins/carouFred/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script src="../js/plugins/carouFred/helper/jquery.mousewheel.min.js"></script>
    <!--<script src="../js/plugins/carouFred/helper/jquery.touchSwipe.min.js"></script>-->
    <script src="../js/plugins/carouFred/helper/jquery.ba-throttle-debounce.min.js"></script>
    <script src="../js/plugins/carouFred/plugin.js"></script>
    
    <!-- RADIO BUTTONS -->
    <script src="../js/plugins/iCheck/icheck.min.js"></script>
    <script src="../js/plugins/iCheck/plugin.js"></script>
    
@endsection
