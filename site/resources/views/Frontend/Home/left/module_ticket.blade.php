<!-- MODULO DE NOTICIAS DESTACADAS 89 -->
<div id="module_ticket">
   
    <div class="moduleTop">
        @if(!empty($recent[0][0][0][0]))
        <h2>{{ $recent[0][0][0][0]['categoryT'] }}</h2>
        @endif
        <div>
            <a id="prevTicket" class="prev" href="#">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            </a>
            <a id="nextTicket" class="next" href="#">
             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            </a>
        </div>
    </div>
   
   <!-- SLIDER DE TICKET -->
    <div class="row" id="ticketContainer">
        <div id="ticketCarrousel">
         @if(!empty($recent[0]))
           @foreach($recent[0] as $colum)
            <!-- SLIDER -->
            <div class="slide">
                @foreach($colum as $notes)
                <div class="row">
                @if(!empty($notes[0]))
                @foreach($notes as $note)    
                   <div class="col-xs-12 col-sm-6">


                       <?php $route = $note['subcategory'] ? [$note['category'],$note['subcategory'],$note['slug']] : [$note['category'],$note['slug']]; ?>
                       <a class="ticketHover" href="{{ route('notes', $route) }}"> 
                        <div class="cropImg">
                            @if($note['img'] == '')
                                <img src="/Backend/images/generica.jpg" alt="car1">
                            @else
                                <img src="{{$note['dir'] }}/{{ $note['img'] }}" alt="car1"> 
                            @endif
                        </div>
                        <span class="category"><img src="../images/categories/{{ $note['icon'] }}" alt="Categoria"></span>
                        <section>
                            <h2>{{ strtoupper($note['title']) }}</h2>
                            <p><span class="calendar"></span> {{ $note['date'] }}</p>
                        </section> 
                       </a>  
                    </div>
                @endforeach
                @endif
                </div>
                @endforeach
            </div>
           @endforeach
         @endif
        </div>
    </div>
</div>

