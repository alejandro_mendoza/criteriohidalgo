<!-- MODULO INFERIOR HOME -->
<div id="module_sport">

    <div class="moduleTop">
        <h2>{{ $recent[2][0][0]['category'] }}</h2>
    </div>

    <div class="row" id="sportCarrouselContainer">

        <div id="sportCarrousel" class="ticketCarousel">
        @foreach($recent[2] as $element)
            <!-- SLIDE -->
                <div class="slide">
                    <!-- NOTICIA PRINCIPAL -->
                    <?php $first = array_shift($element);?>
                    <?php $route = $first['subcategory'] ? [$first['category'],$first['subcategory'],$first['slug']] : [$first['category'],$first['slug']]; ?>
                    <a class="mainImageHover" href="{{ route('notes', $route) }}">
                        <div class="mainImage row">
                            <div class="cropImg">
                                @if($first['img'] == null)
                                    <img src="/Backend/images/generica.jpg" alt="car1">
                                @else
                                    <img src="{{$first['dir'] }}/{{ $first['imgFirst'] }}" alt="car1">
                                @endif
                            </div>
                            <span class="category"><img src="../images/categories/{{ $first['icon'] }}" alt="Categoria"></span>
                            <section>
                                <h2>{{ strtoupper($first['title']) }}</h2>
                                <p><span class="calendar"></span>{{ $first['date'] }}</p>
                            </section>
                        </div>
                    </a>

                    <!-- NOTICIAS SECUNDARIAS-->
                    <div id="sportSec" class="row">
                        @foreach($element as $item)
                            <div class="col-xs-6 col-sm-3">
                                <?php $route = $item['subcategory'] ? [$item['category'],$item['subcategory'],$item['slug']] : [$item['category'],$item['slug']]; ?>
                                <a class="sportsSecHover" href="{{ route('notes', $route) }}">
                                    <div class="cropImg">
                                        @if($item['img'] == null)
                                            <img src="/Backend/images/generica.jpg" alt="Random">
                                        @else
                                            <img src="{{$item['dir'] }}/{{ $item['img'] }}" alt="Random">
                                        @endif
                                    </div>
                                    <span class="category"><img src="../images/categories/{{ $item['icon'] }}" alt="Categoria"></span>
                                    <div class="info">
                                        <h3>{{ $item['title'] }}</h3>
                                        <p>{{ $item['date'] }}</p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
        @endforeach
        <!-- SLIDE -->

        </div>


        <div id="sportNav">
            <a id="prevTicket2" class="prev" href="#">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            </a>
            <a id="nextTicket2" class="next" href="#">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>