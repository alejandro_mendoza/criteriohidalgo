<div class="module_print">
    @if(!empty($edition = \Frontend::moduleEdition()))
        <h2 class="rightTitle">Edición Impresa</h2>
        <div class="grid print">
            <figure class="effect-ming">
            @if(!empty($edition->image))
                <img src="{{ asset("images/editions/{$edition->image}") }}" alt="{{ $edition->title }}">
            @endif
                <figcaption>
                    <p>Ver edición impresa <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> </p>

                    <a href="/edicion-impresa/{{ @(in_array($edition->category->slug, ['ticket', 'sos','la-copa'])?$edition->category->slug:'') }}">Ver más</a>
                </figcaption>
            </figure>
        </div>
    @else
        Edición Impresa: No existe la edición.    
    @endif
</div>