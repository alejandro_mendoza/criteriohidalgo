@extends('master')
@section('title'){{ $title }}@endsection
@section('content')   

<main>
    <div id="module_tittle_category">
        <div id="content">
            <h1>ENVÍO EXITOSO</h1>
        </div>
    </div>

    <div id="container">
        <div class="row">
            <div id="contact" class="col-xs-12 col-sm-10">
                <h2>¡Envío Exitoso!</h2>
                @if(empty($msj))
                    <p>Tus datos se han compartido correctamente.</p>
                @else
                    <p>{{ $msj }}</p>
                @endif
                <hr>
                
                <div>
                    <p>Agradecemos tus comentarios, es importante y lo tomaremos en cuenta para mejora de la difusión de la información</p>
                </div>
                
                <p>Continúa informándote de las noticias más relevantes <a href="/">aquí</a></p>
            </div>
        </div>
    </div>
</main>
    

@include('Frontend.Template.footer')
         
@endsection
