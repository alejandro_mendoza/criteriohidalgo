<header>
    <!--<div id="top" class="row">
        <div class="col-xs-12 col-xs-offset-0 col-sm-4 col-sm-offset-8">
            <div>
                <a href="/anuncios-clasificados">Anuncios clasificados</a>
            </div>
            <div id="social">
                <a id="facebook" target="_blank" href="https://www.facebook.com/criteriohidalgo"></a>
                <a id="twitter" target="_blank" href="https://twitter.com/criteriohidalgo"></a>
                <a id="instagram" target="_blank" href="https://www.instagram.com/criteriohidalgo/"></a>
            </div>
        </div>
    </div>-->

    <div class="row" id="middle">

        <div class="col-xs-12 col-sm-3">
            <div id="slogan">
                <h2>
                    @if(count(app('request')->segments()) > 0)
                        @if(app('request')->segments()[0] == 'edicion-impresa')
                            <a href="/edicion-impresa/{{ @(in_array(app('request')->segments()[1], ['ticket', 'sos','la-copa'])?app('request')->segments()[1]:'') }}">Edición Impresa</a>
                        @else
                            <a href="/edicion-impresa/{{ @(in_array(app('request')->segments()[0], ['ticket', 'sos','la-copa'])?app('request')->segments()[0]:'') }}">Edición Impresa</a>
                        @endif
                    @else
                        <a href="/edicion-impresa">Edición Impresa</a>
                    @endif
                </h2>
            </div>

            <div id="weather">
                <div>
                    <h3>Pachuca, HGO.</h3>
                    <p>{{ \Frontend::datePublishing(date('Y-m-d')) }}</p>
                </div>
                <div>
                    <img src="/img/clima/21.png">
                </div>
            </div>
        </div>

        <div id="logo" class="col-xs-12 col-sm-5">
            <a href="/"><img src="{{ asset('/img/logo.png') }}" alt="Logo" /></a>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div id="top">
                <a href="/anuncios-clasificados">Anuncios clasificados</a>
                <div id="social">
                    <a id="facebook" target="_blank" href="https://www.facebook.com/criteriohidalgo"></a>
                    <a id="twitter" target="_blank" href="https://twitter.com/criteriohidalgo"></a>
                    <a id="instagram" target="_blank" href="https://www.instagram.com/criteriohidalgo/"></a>
                </div>
            </div>
            <div id="search">
                @if(@$title != 'Anuncios clasificados')
                    <!-- buscador -->
                    {!!Form::Open(['url' => url('search'),'method' => 'post', 'class' => "form-horizontal"])!!}

                        <div class="form-group">
                            <div class='input-group date datetimepicker' id='datetimepicker1'>
                                {!!Form::text('search',null,['class'=>'form-control', 'placeholder' => 'Buscar...'])!!}
                                <span class="input-group-addon" style="border-left: none; border-right: 2px solid #CCC;">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <span class="input-group-btn">
                                   <button class="btn btn-default" type="submit">
                                       <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                   </button>
                                 </span>
                            </div>
                        </div>

                   {!!Form::Close()!!}
               @endif
            </div>

        </div>
    </div>


   <nav class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="/" class="navbar-brand">Inicio <span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
        </div>
        <div class="collapse navbar-collapse" id="main-menu">
            <ul class="nav navbar-nav">
                <li id="home"><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                @foreach(\Frontend::getCategories() as $category)
                    <?php $count = \Frontend::getSubcategories($category->id)->count() ?>
                    <li>
                        <?php $active = \Request::segment(1) == $category->slug ? 'active' : null ?>
                        <a class="dropdown-toggle" href="{{ route('notes', [$category->slug]) }}" class="{{ $active }}">{{ $category->title }}</a>
                        @if($count > 0)
                            <ul class="dropdown-menu">
                                @foreach(\Frontend::getSubcategories($category->id) as $subcategory)
                                <li><a href="{{ route('notes', [$category->slug, $subcategory->slug]) }}">{{ $subcategory->title }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </nav>

</header>
