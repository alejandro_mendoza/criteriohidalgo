@foreach($author->articles as $num => $article)   
                                <!-- NOTA -->
                                <div class="row note itemAd">
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="cropImg">
                                            @if(count($article->galleries) > 0)
                                            @if(count($article->galleries[0]->elements) > 0)
                                                <img src="/{{ $article->galleries[0]->directory}}/{{ $article->galleries[0]->elements[0]->value  }}" alt="Nike">
                                            @else
                                                <img src="/Backend/images/generica.jpg" alt="Nike">
                                            @endif
                                            @else
                                                <img src="/Backend/images/generica.jpg" alt="Nike">
                                            @endif
                                            
                                            <span class="category"><img src="../images/categories/{{ $article->category->image }}" alt="Categoria"></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                       <div class="info">
                                            <h3>{{ $article->title }}</h3>
                                            <span>{{ strftime('%B %d, %Y', strtotime($article->date_publishing)) }}</span>
                                            <p>{{ $article->intro }}</p>
                                        </div>
                                    </div>
                                </div> 
                            @endforeach