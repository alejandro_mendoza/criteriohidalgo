@extends('master')
@section('title'){{ $title }}@endsection
@section('content')   

<main>

    <div id="module_tittle_category" class="author_tittle">
        <div id="content">
            <h1>{{ $author->name }} <span>{{ @$author->title }}</span></h1>
        </div>
    </div>

    <div id="container">
        <div class="row">
            
            <div class="row">
                <!-- CONTENIDO PRINCIPAL HOME (IZQUIERDO)-->
                <div  class="col-xs-12 col-md-8" id="mainContent">
                 
                 <div id="author">
                    @if($author)
                      
                       <div id="module_author_info">
                           <div class="row">
                               <div class="col-xs-12 col-sm-6">
                                        <figure>
                                            @if(!empty($author->photo))
                                                <img src="{{ asset("/images/photos/{$author->photo}") }}"/>
                                            @else
                                                <img src="/Backend/images/generica.jpg" alt="Autor">
                                            @endif
                                            <figcaption>
                                                <h3>{{ $author->name }}</h3>
                                                <p>{{ @$author->title }}</p>
                                            </figcaption>
                                        </figure>
                                </div>
                                
                                <div class="col-xs-12 col-sm-6">
                                    <section>
                                        <p>{{ @$author->biography }}</p>
                                    </section>
                                </div>
                            </div>   
                        </div>

                        <div id="module_author_notes" class="renderMore">
                            @foreach($author->articles as $num => $article)   
                                <!-- NOTA -->
                                <div class="row note itemAd">
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="cropImg">
                                            @if(count($article->galleries) > 0)
                                            @if(count($article->galleries[0]->elements) > 0)
                                                <img src="/{{ $article->galleries[0]->directory}}/{{ $article->galleries[0]->elements[0]->value  }}" alt="Nike">
                                            @else
                                                <img src="/Backend/images/generica.jpg" alt="Nike">
                                            @endif
                                            @else
                                                <img src="/Backend/images/generica.jpg" alt="Nike">
                                            @endif
                                            
                                            <span class="category"><img src="../images/categories/{{ $article->category->image }}" alt="Categoria"></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                       <div class="info">
                                            <h3>{{ $article->title }}</h3>
                                            <span>{{ strftime('%B %d, %Y', strtotime($article->date_publishing)) }}</span>
                                            <p>{{ $article->intro }}</p>
                                        </div>
                                    </div>
                                </div> 
                            @endforeach
                           <div id="renderMore">
                           </div>
                           
                           @if($count > 9)
                           <div id="loadMoreButtonContainer">
                              <div id="btnMore">
                                <a  onclick="moreAds('/autor/{{ $author->slug }}/more/',{{ $skip }},{{ $count }});">CARGAR MÁS</a>
                              </div>
                            </div>    
                           @endif
                           
                           
                           
                        </div>
                        {!!Form::Open(['url' => '','method' => 'post', 'class' => "form-horizontal"])!!}

                        {!!Form::Close()!!}
                    @else
                        <div id="module_author_info">
                            <p>El autor no tiene notas publicadas</p>
                        </div>
                    @endif
                    
                  </div>
                </div>


                <!-- CONTENIDO PRINCIPAL HOME (DERECHA)-->

                <div class="col-xs-12 col-md-3 col-md-offset-1 col-lg-4 col-lg-offset-0" id="rightContent">
                    @if(!$modulesR->modules->isEmpty())
                    @foreach($modulesR->modules as $item)
                    @include("Frontend.Category.Modules.{$item->type_slug}")
                    @endforeach
                    @endif
                </div>

            </div>
        </div>
    </div>


</main>

@include('Frontend.Template.footer')
    
@endsection