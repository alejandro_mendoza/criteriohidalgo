
    @foreach($ads as $item)
        <div class="itemAd">
            <a class="newsHover" href="/anuncios-clasificados/detalles/{{ $item->id }}">
                <section class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="cropImg">
                            <img src="{{ asset("/images/ads/{$item->image}") }}" alt="Generica">
                            <span class="category">{{ $item->category->category->name }}</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="info">
                            <h3>{{ $item->title }}</h3>
                            <span>{{ $item->category->name }}</span>
                            <p>{{ $item->description }}</p>
                        </div>
                    </div>
                </section>
            </a>
        </div>
    @endforeach