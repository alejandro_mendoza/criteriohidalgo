
@foreach($ads[0]->sub_categories as $category)
    @foreach($category->elements as $ads)
        <div class="itemAd">
            <a class="newsHover" href="/anuncios-clasificados/detalles/{{ $ads->id }}">
                <section class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="cropImg">
                            <img src="/images/ads/{{ $ad->image }}" >
                            <span class="category">{{ $ads->category->category->name }}</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="info">
                            <h3>{{ $ads->title }}</h3>
                            <span>{{ $ads->category->name }}</span>
                            <p>{{ $ads->description  }}</p>
                        </div>
                    </div>
                </section>
            </a>
        </div>
    @endforeach
@endforeach