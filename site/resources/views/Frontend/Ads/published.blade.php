@extends('master')
@section('title'){{ $title }}@endsection
@section('content')  
<main>

    <div id="module_tittle_category" class="author_tittle">
        <div id="content">
            <h1>Publica tu anuncio</h1>
        </div>
    </div> 

    <div id="container">
        <div class="row">
            <div id="contact" class="col-xs-12 col-sm-10">

                {!!Form::Open(['url' => url('anuncios-clasificados/publishing'),'method' => 'post', 'files' => true, 'id' => "addsForm", 'role'=>'form'])!!}
                    <h2>¿Qué quieres anunciar?</h2>
                    <p>Selecciona la categoría y subcategoría que corresponda a la publicación de tu arcículo/servicio</p>
                      
                    <!-- CATEGORIA -->
                    <div class="customSelect">  
                        <label class="select" for="category">Categoría</label>
                        <div>
                            <p class="customInput">Seleccionar</p>
                            <ul>
                                @foreach($categories as $element)
                                <li data-value="{{ $element->id }}" class='changeSelect' >{{ $element->name }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <select id="category" name="category">
                            @foreach($categories as $element)
                            <option value="{{ $element->id }}" >{{ $element->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- SUB CATEGORIA -->
                    <div class="customSelect" id="renderSubCategories">  
                        <label class="select" for="subcategory">Subcategoría</label>
                        <div>
                            <p class="customInput">Seleccionar</p>
                            <ul>
                                
                            </ul>
                        </div>
                        
                    </div>
                    {!!$errors->first('sub_category_id','<div class="text-danger">:message</div>')!!}
                    <hr>
                    
                    <h2>Detalla tu publicación</h2>
                    <p>Rellena los campos correspondientes de tu publicación</p>
                   
                    <label class="big" for="tittle">Título del anuncio <sup>*</sup></label>
                    {!!Form::text('title',null,['class'=>'', 'id'=>'tittle'])!!}
                    {!!$errors->first('title','<div class="text-danger">:message</div>')!!}
                    
                    <label class="big" for="description">Descripción <sup>*</sup></label>
                    {!!Form::textarea('description',null,['class'=>'','id'=>'description'])!!}
                    {!!$errors->first('description','<div class="text-danger">:message</div>')!!}
                    
                    <div class="customButton">
                        <label class="big" for="image">Foto <span>Elige la foto de tu producto o servicio</span></label>
                        <p id="imageFile">Sin archivo...</p>
                        <span>Examinar</span>
                        {!!Form::file('image',null,['class'=>'','id'=>'image'])!!}
                        {!!$errors->first('image','<div class="text-danger">:message</div>')!!}
                    </div>
                    
                    
                    <label class="big" for="name">Nombre</label>
                    {!!Form::text('name',null,['id'=>'name'])!!}</p>                                                 {!!$errors->first('name','<div class="text-danger">:message</div>')!!}
                    
                    <div class="halfInput">
                        <label class="big" for="email">Email <sup>*</sup></label>
                        {!!Form::text('mail',null,['id'=>'email'])!!}</p>
                        {!!$errors->first('phone','<div class="text-danger">:message</div>')!!}
                    </div>
                    
                    <div class="halfInput" id="telInput">
                        <label class="big" for="tel">Teléfono</label>
                        {!!Form::text('phone',null,['id'=>'tel'])!!}</p>
                        {!!$errors->first('mail','<div class="text-danger">:message</div>')!!}
                        {!!Form::hidden('active','0',['id'=>'active'])!!}
                    </div>
                    
                    @foreach($form->inputs as $input)
                        @if($input->type == 'captcha')
                            @include("Frontend.Forms.formulario.{$input->type}",['input' =>$input,'errors'=>$errors])
                        @endif   
                    @endforeach
                    
                    <span id="terms">Al hacer clic en "Publicar Gratis" acepta los <a href="#">Términos y Condiciones</a></span>

                    <button id="send" type="submit">Publicar Gratis</button>
                {!!Form::Close()!!}
            </div>
        </div>  
    </div>  
    
</main>


@include('Frontend.Template.footer')
    
@endsection
