@extends('master')
@section('title'){{ $title }}@endsection
@section('content')   

<main>

    <div id="module_tittle_category" class="author_tittle">
        <div id="content">
            <h1>Portada Edicion Impresa</h1>
        </div>
    </div>

    <div id="container">
        <div class="row">
            <div id="printContainer" class="col-xs-12">
               
               <div class="moduleTop">
                    <h2>Resultado:</h2>
                    <?php 
                    if(!empty($edition->date_publishing)){
                        $array = explode('-', $edition->date_publishing);
                            if(empty($array[1]))
                                $array = explode('-', $date);
                            $new_date = $array[2].'/'.$array[1].'/'.$array[0]; 
                    }else
                        $new_date = $edition;
                    ?>
                    {!!Form::Open(['url' => route('edicion.show', @(in_array(app('request')->segments()[1], ['ticket', 'sos','la-copa'])?app('request')->segments()[1]:'home')), 'class' => "form-inline", 'role'=>'form'])!!}
                       <div class="form-group">
                            <label for="date">Fecha:</label>
                            {!!Form::text('date_publishing', @$new_date,  ['class'=>'datePicker form-control', 'id'=>'date', 'method' => 'post', 'readonly' ])!!}
                            {!!$errors->first('date_publishing','<div class="text-danger">:message</div>')!!}
                       </div>

                        <button type="submit" class="btn btn-success"> Filtrar</button>
                    {!!Form::Close()!!}
                </div>
                
            @if(!empty($edition->date_publishing))
                <img src="/images/editions/{{ $edition->image }}" alt="Edición Impresa">
            @else
                <p>No se encontraron resultados</p>
            @endif
            </div>
        
        </div>
    </div>

</main>

@include('Frontend.Template.footer')

   <script src="/js/plugins/bootstrap/datepicker.js"></script>
    
@endsection