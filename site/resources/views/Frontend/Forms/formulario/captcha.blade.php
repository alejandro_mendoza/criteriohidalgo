<div class="block">
    @if(@$errors)
        {!!@$errors->first(\App\Http\Backend\Helpers\ContactFormValidator::$CAPTCHA_GOOGLE_NAME,'<p class="error">:message</p>') !!}
    @endif
    <div class="g-recaptcha" data-sitekey="{{$input->optionsArray[0]}}"></div>
</div>