<div class="block">
    <label for="{{$input->slug}}">{{$input->name}}{{($input->required) ? '':''}}</label>
    @if(@$errors)
        {!!@$errors->first($input->slug,'<p class="error">:message</p>') !!}
    @endif
    {!! Form::select($input->slug,array_combine(json_decode($input->options),json_decode($input->options)),'',['id'=>$input->slug]) !!}
</div>