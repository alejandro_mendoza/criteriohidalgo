{!! Form::label($input->name) !!}
@foreach(json_decode($input->options) as $option)
    {!! Form::checkbox("{$input->slug}[]") !!} {{$option}}
@endforeach