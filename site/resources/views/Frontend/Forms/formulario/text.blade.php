<div class="block">
    <label for="{{$input->slug}}">{{$input->name}}{{($input->required) ? '':''}}</label>
    @if(@$errors)
        {!!@$errors->first($input->slug,'<p class="error">:message</p>') !!}
    @endif
    @if($input->required)
        {!! Form::text($input->slug,'',['required'=>'','aria-reqired'=>'true']) !!}
    @else
        {!! Form::text($input->slug,'') !!}
    @endif
</div>