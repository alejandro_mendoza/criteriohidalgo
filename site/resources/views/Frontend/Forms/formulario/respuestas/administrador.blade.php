<div style="font-family: tahoma;">
    <div style="background: #FFF; border-bottom: 4px solid #329d2e;; text-align: center; padding: 10px 0;"><img src="http://criteriohidalgo.desarrollosmasclicks.com/img/logo.png" alt="Manuel Vallejo" /></div>
    <div style="padding: 20px; background: #fff; color: #333; overflow: auto;">
        <h2 style="color:#619c3b; margin: 4px 0;">Administrador</h2>
        <p>Un usuario se ha puesto en contacto contigo, dejando los siguientes datos:</p>
        <ul>
            @foreach($data as $key => $value)
                <li> {{$key}} : {{$value}}</li>
            @endforeach
        </ul>
    </div>
    <div style="background: #000; color: #fff; padding: 8px 0 5px; clear: both;">
        <div style="padding-top: 10px; border-top: 1px dashed #fff;">
            <p style="font-family: arial; margin: 0; text-align: center;">Criterio Hidalgo</p>
        </div>
    </div>
</div>