@extends('master')
@section('title'){{ $title }}@endsection
@section('content')   

<main>
    <div id="module_tittle_category">
        <div id="content">
            <h1>CONTACTO</h1>
        </div>
    </div>

    <div id="container">
        <div class="row">
            <div id="contact" class="col-xs-12 col-sm-10">
                <h2>Contáctanos</h2>
                <p>Nos interesa saber tu opinión. Envíanos tus comentarios, queremos seguir mejorando para ti.</p>
                
                <hr>
                
                {!! Form::open(['url' => "/sendContact"]) !!}
                    <?php $errors = \Session::get('forms.errors'); ?>

                    @foreach($form->inputs as $input)
                        @include("Frontend.Forms.formulario.{$input->type}",['input' =>$input,'errors'=>@$errors])
                    @endforeach

                    <div class="send">
                        <input type="submit" value="{{$form->submit}}" />
                    </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    
</main>

@include('Frontend.Template.footer')


@endsection