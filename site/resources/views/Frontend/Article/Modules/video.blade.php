@if($content)
<div id="module_video">
  @if(count($elements = explode(',', $content)) > 0)

    <div id="videoGalleryContainer">
        <div id="videoGallery">
            <div>
             <div class="tabs">
                 <ul></ul>
             </div>
            </div>
            <div class="mightyslider_modern_skin">
             <div class="frame">
                 <div class="slide_element">
                    <!--VIDEO-->
                 @foreach($elements as $item)
                     <div class="slide" data-mightyslider="
                       type:    'iframe',
                       source: 'http://www.youtube.com/embed/{{ $item }}?autoplay=1&autohide=1&border=0&wmode=opaque',
                       cover:   'http://img.youtube.com/vi/{{ $item }}/0.jpg'
                     "></div>
                 @endforeach
                 </div>
             </div>
            </div>
        </div>
     </div>

   @endif
</div>
@endif

