<div id="module_tittle_category" @if($category->color) style="background-color: {{ $category->color }}" @endif>
    <div id="content">
        <div id="logoC">
            @if($category->image)
            <img src="{{ asset("images/categories/{$category->image}") }}" alt="{{ $category->title }}">
            @endif
        </div>
        <div id="breadcrumb">
            @if($category->parent_id)
                {{ $category->parent->title }} >
            @endif
            <h1>{{ $category->title }}</h1>
        </div>
    </div>
</div>