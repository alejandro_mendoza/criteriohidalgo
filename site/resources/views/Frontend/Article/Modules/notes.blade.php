<a class="searchHover" href="{{ route('notes', \Frontend::getRoute($item, $item->slug)) }}">
    <div class="module_notes_category itemAd">
        <!-- NOTA -->
        <?php $gallery = $item->galleries->first() ?>
        <div class="row note">
            <div class="col-xs-12 col-sm-3">
                <div class="cropImg">
		@if($gallery != null)
                    @if(!$gallery->elements->isEmpty())
                        <?php $val = $gallery->elements->first()->subtitle ? $gallery->elements->first()->subtitle : $gallery->elements->first()->value; ?>
                    <img src="{{ asset("{$gallery->directory}/{$val}") }}" alt="{{ $item->title }}" />
                    @else
                    <img src="{{ asset('img/generica.jpg') }}" alt="{{ $item->title }}" />
                    @endif
		@endif
                </div>
                @if($item->category->image)
                <span class="category"><img src="{{ asset("images/categories/{$item->category->image}") }}" alt="{{ $category->title }}"></span>
                @endif
            </div>

            <div class="col-xs-12 col-sm-9">
                <div class="info">
                    <h3>{!! $item->title !!}</h3>
                    <span>{!! \Frontend::datePublishing($item->date_publishing) !!}</span>
                    <p>{!! $item->intro !!}</p>
                </div>
            </div>
        </div>
    </div>
</a>
