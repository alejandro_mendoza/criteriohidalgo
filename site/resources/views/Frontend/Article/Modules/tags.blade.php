@if(!$article->tags->isEmpty())
<div id="module_tags">
    <h3>Tags</h3>
    <ul>
        @foreach($article->tags as $tag)
        <li>{{ $tag->value }}</li>
        @endforeach
    </ul>
</div>
@endif