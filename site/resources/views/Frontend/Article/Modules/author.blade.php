<div id="module_author">
    <p><span class="calendarBlack"></span> {{ \Frontend::dateFormat($article->date_publishing) }}</p>
    <p><span class="profile"></span> {{ $article->authors->name }}</p>
    @if($article->author)
    <section>
        {{--@if(!empty($article->authors->photo))--}}
            {{--<img src="{{ asset("/images/photos/{$article->authors->photo}") }}"/>--}}
        {{--@else--}}
            {{--<img src="/Backend/images/generica.jpg" alt="Autor">--}}
        {{--@endif--}}
        <a href="/autor/{{ $article->authors->slug }}"><h2>{{ $article->authors->name }}</h2> </a>
    </section>
    
    <section>
        <p id="bio">{{ $article->authors->biography }}</p>
    </section>
    @endif
</div>
