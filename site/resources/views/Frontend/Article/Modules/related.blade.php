@if(!$related->isEmpty())
<div id="module_related">
    <h3>Relacionados</h3>
    @foreach($related as $item)
    <!-- NOTA RELACIONADA -->
    <?php $gallery = $item->galleries->first() ?>
    <a class="hoverNoteRelated" href="{{ route('notes', \Frontend::getRoute($item, $item->slug)) }}">
        <div class="note">
            <div class="cropImg">
                @if(!$gallery->elements->isEmpty())
                    <?php $val = $gallery->elements[0]->subtitle ? $gallery->elements[0]->subtitle : $gallery->elements[0]->value; ?>
                <img src="{{ asset("{$gallery->directory}/{$val}") }}" alt="{{ $gallery->elements[0]->title }}">
                @else
                <img src="{{ asset('img/generica.jpg') }}" alt="{{ $item->title }}" />
                @endif
            </div>
            @if($article->category->image)
            <span class="category"><img src="{{ asset("images/categories/{$article->category->image}") }}" alt="{{ $article->category->title }}"></span>
            @endif
            <h3>{{ $item->title }}</h3>
            <p>{{ \Frontend::datePublishing($item->date_publishing) }}</p>
        </div>
    </a>
    @endforeach
</div>
@endif