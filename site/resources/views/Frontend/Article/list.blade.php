@extends('master')
@section('title'){{ $category->title }} - Criterio Hidalgo @endsection
@section('metadescription'){{ $category->description }}@endsection
@section('content')

<main>   

  @include('Frontend.Category.Modules.titulo', ['category' => $category])

    <div id="container">
        <div class="row">

            <!-- CONTENIDO PRINCIPAL HOME (IZQUIERDO)-->
            <div  class="col-xs-12 col-md-8" id="mainContent">

                @include('Frontend.Article.Modules.principal')
                @if(!$list->isEmpty())
                <?php  $aux = 0; ?>
                    <div class="renderMore">
                        @foreach($list as $item)
                            <?php  $aux++; ?>
                            @include('Frontend.Article.Modules.notes')
                            @if($aux == 5)
                                @include('Frontend.Home.left.module_banner', ['slug' => $category->slug, 'pos' => '0'])
                            @elseif($aux == 10)
                                @include('Frontend.Home.left.module_banner', ['slug' => $category->slug, 'pos' => '1'])
                            @endif      
                        @endforeach
                        
                        @if($aux < 10 && $aux > 5)
                            @include('Frontend.Home.left.module_banner', ['slug' => $category->slug, 'pos' => '1'])
                        @endif    
                    </div>

                    
                        <!-- BOTÓN -->
                        @if($count > 9)
                        <div id="loadMoreButtonContainer" class="module_notes_category">
                            <div id="btnMore">
                                <a onclick="moreAds('/notas/more/{{ $category->id }}/',{{ $skip }},{{ $count }});" >CARGAR MÁS</a>
                            </div>
                        </div>
                        @endif

                        


                @else
                    <div id="contact" class="col-xs-12 col-sm-10">
                        <h2>¡Whoops!</h2>
                        <hr>
                        <div><p>Al parecer no hay notas en este momento.</p></div>
                        <p>Puedes ir al inicio del sitio <a href="/">aquí</a></p>
                    </div>
                @endif
            </div>
                
                <!-- CONTENIDO PRINCIPAL HOME (DERECHA)-->
                <div class="col-xs-12 col-md-3 col-md-offset-1 col-lg-4 col-lg-offset-0" id="rightContent">
                    @if(!$category->modules->isEmpty())
                        @foreach($category->modules as $item)
                            @include("Frontend.Category.Modules.{$item->type_slug}")
                        @endforeach
                    @endif
                </div>
                
            </div>

    </div>

</main>

@include('Frontend.Template.footer')
    
@endsection



