@extends('master')
@section('title'){{ $title }}@endsection
@section('content') 

<main>

    <div id="module_tittle_category" class="author_tittle">
        <div id="content">
            <h1>Resultados de búsqueda</h1>
        </div>
    </div>
    
    <div id="container">
        <div class="row">
           
            <!-- CONTENIDO PRINCIPAL HOME (IZQUIERDO)-->
            <div  class="col-xs-12 col-md-8" id="mainContent">
                @if(count($articles) > 0)
                    @foreach($articles as $item)
                      <?php $route = $item->category->parent ? [$item->category->slug,$item->category->parent->slug,$item->slug]: [$item->category->slug,$item->slug] ?>
                       <a class="searchHover" href="{{ route('notes', $route) }}">
                        <div class="module_notes_category">
                             <div class="row note">
                                <div class="col-xs-12 col-sm-3">
                                    <div class="cropImg">
                                        <?php $gallery = $item->galleries->first() ?>
                                        @if($gallery != '')
                                        @if(!$gallery->elements->isEmpty())
                                                    <?php $val = $gallery->elements->first()->subtitle ? $gallery->elements->first()->subtitle : $gallery->elements->first()->value; ?>
                                        <img src="{{ asset("{$gallery->directory}/{$val}") }}" alt="Nike">
                                        @endif
                                        @endif
                                    </div>
                                    <span class="category">{{ $item->category->name }}</span>
                                 </div>
                                 <div class="col-xs-12 col-sm-9">
                                   <div class="info">
                                        <h3>{{ $item->title }}</h3>
                                        <span>{{ \Frontend::datePublishing($item->date_publishing) }}</span>
                                        <p>{{ $item->intro }}</p>
                                   </div>
                                 </div>
                            </div>
                        </div> 
                        </a>  
                    @endforeach
                @else
                    <p>No se encontraron resultados</p>
                @endif 
            </div>
            
            
            

            <!-- CONTENIDO PRINCIPAL HOME (DERECHA)-->

            <div class="col-xs-12 col-md-3 col-md-offset-1 col-lg-4 col-lg-offset-0" id="rightContent">
                @if(!$modulesR->modules->isEmpty())
                    @foreach($modulesR->modules as $item)
                        @include("Frontend.Category.Modules.{$item->type_slug}")
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    
</main>
<!-- Resultados -->


@include('Frontend.Template.footer')
    
@endsection
