@extends('master')
@section('title'){{ $title }}@endsection
@section('content')   

<main>
    <div id="module_tittle_category">
        <div id="content">
            <h1>DIRECTORIO</h1>
        </div>
    </div>

    <div id="container">
        <div class="row">
            <div id="directory" class="col-xs-12 col-sm-10">
                <div class="row">
                    @foreach($directory as $column)
                    <div class="col-xs-12 col-sm-6">
                        @foreach($column as $member)
                            <div class="employee">
                                <h2>
                                    {{ $member['name'] }}
                                    @if($member['mail'])
                                        <a class="mail" href="mailto:{{ $member['mail'] }}"></a>
                                    @endif    
                                </h2>
                                <p>{{ $member['title'] }}</p>
                            </div>    
                        @endforeach
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    
</main>

@include('Frontend.Template.footer')


@endsection