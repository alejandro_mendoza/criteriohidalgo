@extends('master')
@section('title'){{ $title }}@endsection
@section('content')


<main>

    <div id="module_tittle_category" class="author_tittle">
        <div id="content">
            <h1>{!! $section->elements[0]->title !!}</h1>
        </div>
    </div>

    <div id="container">
        <div class="row">
        
            <div id="static" class="col-xs-12 col-sm-10">
                <h2>{!! $section->elements[0]->title !!}</h2>
                
                <hr>
                
                <!--CONTENIDO-->
                <div>
                   <h3>{!! $section->elements[1]->title !!}</h3>
                   {!! $section->elements[1]->text !!} 
                   
                   <h3>{!! $section->elements[2]->title !!}</h3>
                   {!! $section->elements[2]->text !!} 
                </div>
                
            </div>
        </div>
    </div>
    

</main>

@include('Frontend.Template.footer')
    
@endsection