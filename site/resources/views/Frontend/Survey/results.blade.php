<div class="module_test" id='renderSurvey'>
    <h2 class="rightTitle">Encuesta</h2>

    <div class="test">
        <h3>{{$survey->sentence}}</h3>

        <table class="table table-condensed">
            <tbody>
                @foreach($survey->options as $option)
                    @if($option->sentence)
                        <tr>
                            <td> {{$option->sentence}}
                                <div class="progress" style="width: 100%;">
                                    @if($survey->options->sum('counter') > 0)
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ round($option->counter * 100/ $survey->options->sum('counter') ,2 )}}" aria-valuemin="0" aria-valuemax="100" style="width: {{ round($option->counter * 100/ $survey->options->sum('counter') ,2 )}}%;">
                                       {{ round($option->counter * 100/ $survey->options->sum('counter') ,2 )}}%
                                    @else
                                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                    @endif        
                                </div>
                            </td>
                            </div>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>