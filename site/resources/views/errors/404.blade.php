@extends('master')

@section('content')  

<main>
    <div id="module_tittle_category">
        <div id="content">
            <h1>Error 404</h1>
        </div>
    </div>

    <div id="container">
        <div class="row">
            <div id="contact" class="col-xs-12 col-sm-10">
                <h2>¡Error de Navegación!</h2>
            
                <hr>
                
                <div>
                    <p>Al parecer la página a la que deseas acceder no existe, te invitamos a utilizar el menú de navegación para rederigirte a la sección deseada</p>
                </div>
                
                <p>Te estamos redireccionando al <a href="/">Home</a></p>
            </div>
        </div>
    </div>
</main>

@include('Frontend.Template.footer')
<script>
setTimeout(function(){
    redirectHome();
},7000);
</script>    
@endsection
