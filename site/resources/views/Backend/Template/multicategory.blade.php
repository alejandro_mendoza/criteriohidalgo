<select multiple id="categories" name="categories[]" class="form-control" size="15">
    @foreach($list as $root)
        <?php $selected = in_array($root['id'], $values ? $values : [], true) ? 'selected="selected"' : null ?>
        <option value="{{ $root['id'] }}" class="{{ $root['class'] }}" {{ $selected }}>{{ $root['category'] }}</option>
        @if(!empty($root['subcategory']))
            @foreach($root['subcategory'] as $item)
                <?php $selected = in_array($item['id'], $values ? $values : [], true) ? 'selected="selected"' : null ?>
                <option value="{{ $item['id'] }}" class="{{ $item['class'] }}" {{ $selected }}>{{ $item['category'] }}</option>
            @endforeach
        @endif
    @endforeach
</select>