@if(count($errors) > 0)
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-danger text-center">
                    <strong><i class="glyphicon glyphicon-exclamation-sign"></i> Whoops!</strong> Hubo algunos problemas con los campos.
                </div>
            </div>
        </div>
    </div>
@endif