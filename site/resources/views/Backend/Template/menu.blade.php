    <nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand">
					<img src="{{ asset('/img/logo.png') }}" alt="Criterio Hidalgo"/>
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    @if (!Auth::guest())
				<ul class="nav navbar-nav">
					<li><a href="/mcPanel"><span class="glyphicon glyphicon-th-large"></span> Dashboard</a></li>
					@if(\Auth::user()->role->type <> 'Editor')
					<li><a href="{{ route('homeLeft') }}"><span class="glyphicon glyphicon-list-alt"></span> Home</a></li>
					<li><a href="{{ route('users') }}"><span class="glyphicon glyphicon-user"></span> Usuarios</a></li>
					@endif
					<li><a href="{{ route('autores') }}"><span class="glyphicon glyphicon-tasks"></span> Autores</a></li>
					@if(\Auth::user()->role->type <> 'Editor')
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-text-background">
							</span> Secciones estaticas <span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ route('statics.edit',1) }}"> Nosotros</a></li>
							<li><a href="{{ route('directorio') }}"> Directorio</a></li>
							<li><a href="{{ route('statics.edit',3) }}"> Aviso de privacidad</a></li>
							<li><a href="{{ route('forms') }}"> Contacto</a></li>
						</ul>
					</li>
					<li><a href="{{ route('categories') }}"><span class="glyphicon glyphicon-list"></span> Categorías</a></li>
					@endif
					<li><a href="{{ route('articles') }}"><span class="glyphicon glyphicon-file"></span> Notas</a></li>
					@if(\Auth::user()->role->type <> 'Editor')
					<li><a href="{{ route('surveys') }}"><span class="glyphicon glyphicon-stats"></span> Encuestas</a></li>
					<li><a href="{{ route('slider') }}"><span class="glyphicon glyphicon-th"></span> Slider</a></li>
                                        <li><a href="{{ route('edicion') }}"><span class="glyphicon glyphicon-th"></span> Edicion impresa</a></li>
                                        <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-th"></span> Anuncios Clasificados</a></li>
					@endif
				</ul>
				@endif

				<ul class="nav navbar-nav navbar-right">
					@if (!Auth::guest())
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-off"></span> {{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ route('user.logout') }}"><span class="glyphicon glyphicon-log-out"></span> Salir</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
