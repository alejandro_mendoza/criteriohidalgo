<select id="category_id" name="category_id" class="form-control">
    <option value="">Seleccionar</option>
    @foreach($list as $root)
        <option value="{{ $root['id'] }}" class="{{ $root['class'] }}" {{ \Backend::disabledOption($root['id'], $assigned) }} {{ \Backend::selectedOption($root['id'], $value) }}>{{ $root['category'] }}</option>
        @if(!empty($root['subcategory']))
            @foreach($root['subcategory'] as $item)
                <option value="{{ $item['id'] }}" class="{{ $item['class'] }}" {{ \Backend::disabledOption($item['id'], $assigned) }} {{ \Backend::selectedOption($item['id'], $value) }}>{{ $item['category'] }}</option>
            @endforeach
        @endif
    @endforeach
</select>