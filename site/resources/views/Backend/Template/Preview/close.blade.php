        <!-- MAIN -->
        <script src="{{ asset('/js/jquery.js') }}"></script>

        <script src="{{ asset('/js/plugins/royalslider/jquery.royalslider.min.js') }}"></script>
        <script src="{{ asset('/js/plugins/royalslider/plugin.js') }}"></script>
        
        
         <!-- MightySlider -->
        <script src="{{ asset('/js/jquery.easing.js') }}"></script>
        <script src="{{ asset('/js/plugins/mightyslider/tweenlite.js') }}"></script>
        <script src="{{ asset('/js/plugins/mightyslider/mightyslider.min.js') }}"></script>
        <script src="{{ asset('/js/plugins/mightyslider/plugin.js') }}"></script>

        <!-- ADD THIS -->
        <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-561ece83320535d1" async="async"></script>

        <!-- PLUGINS -->
        <script src="{{ asset('/js/plugins/bootstrap/transition.js') }}"></script>
        <script src="{{ asset('/js/plugins/bootstrap/collapse.js') }}"></script>
        <script src="{{ asset('/js/plugins/bootstrap/dropdown.js') }}"></script>
        <script src="{{ asset('/js/plugin.js') }}"></script>
        
        <!-- WEBSOCKET -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="/js/autobahn.js"></script>  <!--webSocket -->
        <script src="{{ asset('/js/websocket.js') }}"></script>
        
        
    </body>
</html>