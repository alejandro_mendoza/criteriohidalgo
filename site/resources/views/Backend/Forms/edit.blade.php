@extends('Backend.Template.layout')

@section('title') Contacto @stop

@section('content')
    <div class="container">
        <h2>{{$title}}</h2>

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <p class="text-center">Ocurrieron algunos errores</p>
            </div>
        @endif

        @if(!empty($message))
            <div class="alert alert-success">
                <p class="text-center">{{$message}}</p>
            </div>
        @endif

        {!!Form::Open(['url'=>route('modulos.contacto.actualizar',$id),'class'=>"form-horizontal",'role'=>'form'])!!}
        <input type="hidden" value="{{$module->modulable->id}}" name="form_id"/>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                {!!Form::Label('type','Tipo de campo:',['class'=>'control-label'])!!}
                {!!Form::select('type',$types,@$item->type,['class'=>'form-control'])!!}
                {!!$errors->first('type','<div class="text-danger">:message</div>')!!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                {!!Form::Label('name','Nombre:',['class'=>'control-label'])!!}
                {!!Form::text('name',@$item->name,['class'=>'form-control'])!!}
                {!!$errors->first('name','<div class="text-danger">:message</div>')!!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                {!!Form::Label('position','Posición:',['class'=>'control-label'])!!}
                {!!Form::input('number','position',(empty($item->position)) ? $last : $item->position,['class'=>'form-control','min'=>1])!!}
                {!!$errors->first('position','<div class="text-danger">:message</div>')!!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                {!!Form::Label('options','Ingrese las opciones',['class'=>'control-label'])!!}
                <div class="alert alert-info">
                    <p>Para botones de radio, checkbox y combos ingrese las opciones separadas  "{{\App\Http\Backend\Repositories\InputRepo::$SEPARATOR}}". Ejemplo: opción 1 {{\App\Http\Backend\Repositories\InputRepo::$SEPARATOR}} opción 2</p>
                    <p>Para captcha, ingrese la clave del sitio seguida de la clave secreta separadas por "{{\App\Http\Backend\Repositories\InputRepo::$SEPARATOR}}". Ejemplo 6Lf4gwsTAAAAAO1kZ5BZYEfzL-udCI72mAUe9PBO {{\App\Http\Backend\Repositories\InputRepo::$SEPARATOR}} 6Lf4gwsTAAAAABhBmlENiWznq-rKnnVKx_7C0kB-</p>
                </div>
                {!!Form::text('options',@$item->options,['class'=>'form-control'])!!}
                {!!$errors->first('options','<div class="text-danger">:message</div>')!!}
            </div>
        </div>
        <div class="form-group">
            <div class="checkbox col-sm-8 col-sm-offset-2">
                <label>{!!Form::checkbox('required','1',!empty($item->required))!!} Campo Requerido</label>
            </div>
        </div>
        <div class="form-group">
            <div class="text-center">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                @if($item)
                    <a class="btn btn-primary" href="{{route('modulos.contacto.edicion')}}"><i class="fa fa-plus"></i> Nuevo Campo</a>
                @endif
                <a class="btn btn-danger" href="{{route('forms')}}"><i class="fa fa-mail-reply"></i> Regresar</a>
            </div>
        </div>
        {!!Form::Close()!!}
    </div>
@stop