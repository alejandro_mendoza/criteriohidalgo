<html>
<body>
{{--Tabla de filtros--}}
<table>
    <tr>
        <th>Reporte de formulario de contacto</th>
        <th>Generado el {{date('d-m-Y H:i:s')}}</th>
    </tr>
</table>

<table>
    <thead>
    <tr>
        <th>Secci&oacute;n</th>
    </tr>
    </thead>
    <tbody>
    
    </tbody>
</table>

<table>
    <tr>
        <th>N&uacute;mero de resultados</th>
        <th>{{$messages->count()}}</th>
    </tr>
</table>


<table>
    <thead>
    <tr>
        <th>Fecha</th>
        <th>Correo Electr&oacute;nico</th>
        <th>Datos</th>
    </tr>
    </thead>
    <tbody>
    @foreach($messages as $message)
        <tr>
            <td>{{htmlentities($message->created_at)}}</td>
            <td>{{htmlentities($form->getMainEmailValue($message->id))}}</td>
            <td>
                @foreach(json_decode($message->values) as $field => $value)
                    {{htmlentities($field)}}: {{htmlentities($value)}} <br />
                @endforeach
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>