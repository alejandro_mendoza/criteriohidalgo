<div class="container">
    <div class="col-sm-8 col-sm-offset-2">
        {!! Form::open(['url'=>route('forms.update',$module->modulable->id),'class'=>"form-horizontal",'role'=>'form']) !!}
            <div class="form-group">
                {!!Form::Label('subject','Asunto:',['class'=>'control-label'])!!}
                {!!Form::text('subject',$module->modulable->subject,['class'=>'form-control'])!!}
                {!!$errors->first('subject','<div class="text-danger">:message</div>')!!}
            </div>
            <div class="form-group">
                {!!Form::Label('to','Lista de destinatarios (emails separados por comas):',['class'=>'control-label'])!!}
                {!!Form::text('to',$module->modulable->to,['class'=>'form-control'])!!}
                {!!$errors->first('to','<div class="text-danger">:message</div>')!!}
            </div>
            <div class="form-group">
                {!!Form::Label('submit','Texto del botón enviar:',['class'=>'control-label'])!!}
                {!!Form::text('submit',$module->modulable->submit,['class'=>'form-control'])!!}
                {!!$errors->first('submit','<div class="text-danger">:message</div>')!!}
            </div>
            <div class="form-group">
                {!!Form::Label('thanks','Liga de la página de gracias:',['class'=>'control-label'])!!}
                {!!Form::text('thanks',$module->modulable->thanks,['class'=>'form-control'])!!}
                {!!$errors->first('thanks','<div class="text-danger">:message</div>')!!}
            </div>
            <div class="form-group">
                {!!Form::Label('response','Mensaje del email:',['class'=>'control-label'])!!}
                {!!Form::textArea('response',$module->modulable->response,['class'=>'form-control text_rich'])!!}
                {!!$errors->first('response','<div class="text-danger">:message</div>')!!}
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                <a class="btn btn-primary" href="{{route('mensajes')}}">
                    @if($module->modulable->responses->sum('seen') > 0)
                        <span class="glyphicon glyphicon-envelope"></span> Mensajes <span class="badge">{{ $module->modulable->responses->sum('seen') }}</span>
                    @else
                        <span class="glyphicon glyphicon-envelope"></span> Mensajes
                    @endif
                </a>
                <a class="btn btn-primary" href="{{route('forms.reporte',1)}}">
                    <span class="glyphicon glyphicon-download-alt"></span> Descargar Reporte
                </a>
            </div>
            
        {!! Form::close() !!}
    </div>
</div>
