@extends('Backend.Template.layout')

@section('title') Contacto @stop

@section('content')

    {!!$message!!}

    <div class="container">
        <h1>Formulario de Contacto</h1>
        <hr />

        @include('Backend.Forms.edicion_formulario')
        <hr />
    </div>

    <div class="container">
        <h2>Campos del formulario</h2>

        <div class="form-group">
            <a class="btn btn-primary" href="{{route('modulos.contacto.edicion')}}">
                <span class="glyphicon glyphicon-plus"></span> Nuevo Campo
            </a>
        </div>

        <hr/>

        @if(!$list->isEmpty())
            total: {{$list->count()}}

            <table class="table table-striped table-hover">
                <thead>
                    <tr class="text-center">
                        <th>Posición</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Requerido</th>
                        <th colspan="2">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($list as $item)
                        <tr  class="text-center">
                            <td>{{$item->position}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{ $item->type }}</td>
                            <td>{{($item->required)? 'SI' : 'NO'}}</td>
                            <td><a href="{{route('modulos.contacto.edicion',$item->id)}}"><span class="glyphicon glyphicon-pencil"></span> </a> </td>
                            <td><a href="{{route('modulos.contacto.eliminar',$item->id)}}"><span class="glyphicon glyphicon-remove"></span> </a> </td>    
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h2>No hay campos</h2>
        @endif
    </div>

@stop