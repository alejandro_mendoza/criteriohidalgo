    <?php $params = json_decode($module->params) ?>
    <div class="panel panel-primary">
        <div class="panel-heading"><strong>{{ $module->type->name }}</strong></div>
        <div class="panel-body">
            {!! Form::open(['route'=>['module.update', $module->id], 'files' => true, 'role' => 'form']) !!}
                <div class="module">
                    @if(in_array($module->type_slug, ['anuncios-clasificados']))
                        <label>Muestra un listado de los anuncios más recientes.</label>
                    @endif
                    @if(in_array($module->type_slug, ['minuto-minuto']))
                        <label>Muestra un listado de las notas más recientes en tiempo real.</label>
                    @endif
                    @if(in_array($module->type_slug, ['encuesta']))
                        <label>Muestra un formulario con la encuesta previamente creada.</label>
                    @endif
                    @if(in_array($module->type_slug, ['edicion-impresa']))
                        <label>Muestra la última edición creada.</label>
                    @endif
                    @if(in_array($module->type_slug, ['mas-vistas']))
                        <label>Muestra las notas más vistas del día.</label>
                    @endif
                    @if(in_array($module->type_slug, ['notas-recientes']))
                    <div class="form-group">
                        {!!Form::label('category_id', 'Categoría:', ['class' => 'control-label'])!!}
                        @include('Backend.Modules.category', ['list' => $categories, 'value' => @$params->category])
                    </div>
                    @endif
                    @if(in_array($module->type_slug, ['galeria-imagenes', 'columna']))
                        <div class="form-group">
                            {!!Form::label('article', 'Url de la nota:', ['class' => 'control-label'])!!}
                            {!!Form::text('module[article]', @$params->article, ['class'=>'form-control']) !!}
                        </div>
                    @endif
                    @if(in_array($module->type_slug, ['banner']))
                        <div class="form-group">
                            {!!Form::label('html', 'Código HTML:', ['class' => 'control-label'])!!}
                            {!!Form::textarea('module[html]', @base64_decode($params->html),['class'=>'form-control', 'rows' => 4])!!}
                        </div>
                    @endif
                </div>

                <div class="btn-group btn-group-justified">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default">
                            {!!Form::checkbox('', $module->id, false, ['class'=>'multiselect'])!!}<span class="glyphicon glyphicon-ok"></span>
                        </label>
                    </div>
                    <div class="btn-group">
                        <a href="{{route('module.visibility', $module->id)}}" class="btn btn-default">{!! \Backend::active($module->visible) !!}</a>
                    </div>
                    @if(!in_array($module->type_slug, ['anuncios-clasificados', 'encuesta', 'minuto-minuto']))
                    <div class="btn-group">
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span></button>
                    </div>
                    @endif
                    <div class="btn-group">
                        <a class="btn btn-danger tobe_eliminated" href="{{route('module.delete', $module->id)}}"><span class="glyphicon glyphicon-remove-sign"></span></a>
                    </div>
                </div>
                {!! Form::hidden('category_id', $category->id) !!}
            {!!Form::close()!!}
        </div>
    </div>