@extends('Backend.Template.layout')

@section('title') Edición de módulos @stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ul class="nav nav-tabs nav-tabs-form">
                    <li><a href="{{ url("mcPanel/categorias/registro/{$category->id}") }}"><h4><span class="glyphicon glyphicon-list-alt"></span> Categories</h4></a></li>
                    <li class="active"><a href="#"><h4><span class="glyphicon glyphicon-th-large"></span> Módulos</h4></a></li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-body">

                        {!!Form::open(['route'=>'module.delete.multi','class'=>'formMultiSelect'])!!}
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger tobe_eliminated_submit"><span class="glyphicon glyphicon-remove-sign"></span> Eliminar</button>
                        </div>

                        <div class="form-group">
                            <input type="checkbox" id="multiSelect">
                            <label for="multiSelect">Seleccionar todos</label>
                        </div>
                        {!!Form::close()!!}

                        <hr>

                        <div class="container-fluid" id="gallery-container">
                            <div class="row">
                                @if(count($category->modules))
                                    <ul id="sortable" data-content="modulos">
                                        @foreach($category->modules as $module)
                                            <li id="{{ $module->id }}" class="col-sm-6 col-md-4">@include('Backend.Modules.element')</li>
                                        @endforeach
                                    </ul>
                                @else
                                    <h4>No se han agregado módulos.</h4>
                                @endif
                            </div>
                        </div>

                        <hr>

                        {!!Form::open(['url' =>  route('module.add', ['id' => $category->id])])!!}
                            <label class="label label-primary">Selecciona el módulo a insertar.</label>
                            <div class="input-group">
                                {!! Form::select('type_id', $types, null, ['class' => 'form-control']) !!}
                                <span class="input-group-btn"><button class="btn btn-success " type="submit">Agregar módulo</button></span>
                                <div class="col-lg-12"></div>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop