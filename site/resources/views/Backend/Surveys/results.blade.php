
<h2>Resultados de la encuesta</h2>

<p>Pregunta : {{$item->sentence}}</p>
<p>Encuestas contestadas: {{ $item->options->sum('counter') }}</p>

<table class="table table-condensed">
    <thead>
        <tr>
            <th>Opciones </th>            
        </tr>
    </thead>
    <tbody>
        
        @foreach($item->options as $option)
            @if($option->sentence)
                <tr>
                    <td> {{$option->sentence}}
                        <div class="progress" style="width: 100%;">
                            @if($item->options->sum('counter') > 0)
                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ round($option->counter * 100/ $item->options->sum('counter') ,2 )}}" aria-valuemin="0" aria-valuemax="100" style="width: {{ round($option->counter * 100/ $item->options->sum('counter') ,2 )}}%;">
                                ({{$option->counter}}) {{ round($option->counter * 100/ $item->options->sum('counter') ,2 )}}%
                            @else
                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                    (0)
                            @endif        
                        </div>
                    </td>
                    </div>
                </tr>

            @endif
            
        @endforeach
    </tbody>
</table>