@extends('Backend.Template.layout')

@section('title') Notas @stop

@section('content')

    <div class="container panel panel-default">
        <h2>Notas</h2>

        <div class="form-group">
            <a class="btn btn-primary" href="{{ route('articles.edit') }}">
                <span class="glyphicon glyphicon-plus"></span> Nueva Nota
            </a>
        </div>

        <hr />

        {!! $filters !!}

        <hr/>

        {!! $message !!}

        @if(!$list->isEmpty())
            Total: {{$list->total()}}
            {!!Form::Open(['class' => 'form', 'id' => 'form-multi-select'])!!}
                <div class="text-left form-group">
                    <label class="control-label">Elementos Seleccionados: </label>
                    <a data-href="{{route('articles.status', 'active')}}" data-action="CAMBIAR ESTATUS" class="btn btn-info task-multi-select" id="visibility"><span class="glyphicon glyphicon-eye-open"></span> Cambiar Estatus</a>
                    <a data-href="{{route('articles.status', 'featuring')}}" data-action="CAMBIAR DESTACADO" class="btn btn-info task-multi-select" id="visibility"><span class="glyphicon glyphicon-bookmark"></span> Cambiar Destacado</a>
                    <a data-href="{{route('articles.status', 'principal')}}" data-action="CAMBIAR PRINCIPAL" class="btn btn-info task-multi-select" id="visibility"><span class="glyphicon glyphicon-star"></span> Cambiar Principal</a>
                    <a data-href="{{route('articles.delete')}}" data-action="ELIMINAR" class="btn btn-danger task-multi-select" id="delete"><i class="glyphicon glyphicon-remove-sign"></i> Eliminar</a>
                </div>
            {!!Form::close()!!}

            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th><input type="checkbox" class="multi-select-all"></th>
                                <th>Id</th>
                                <th class="col-sm-2">Título</th>
                                <th>Categoría</th>
                                <th>Autor</th>
                                <th>Visible</th>
                                <th>Dest.</th>
                                <th>Ppal.</th>
                                <th>Fecha</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                    @foreach($list as $item)
                        <tr>
                            <td><input type="checkbox" class="select-checkbox" name="selected[]" value="{{$item->id}}" /></td>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ @$item->category->title }}</td>
                            <td>{{ @$item->authors->name }}</td>
                            <td>{!! \Backend::active($item->active) !!}</td>
                            <td>{!! \Backend::featured($item->featuring) !!}</td>
                            <td>{!! \Backend::principal($item->principal) !!}</td>
                            <td>{!! $item->date_publishing !!}</td>
                            <td>
                                <?php $route = !is_null($item->parent_id) ? 'articles.edit' : 'articles.edit'  ?>
                                <a class="btn btn-primary" href="{{route($route, $item->id)}}"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                <a class="btn btn-default" href="{{ route('gallery', ['id' => $item->galleries->first(), 'type' => 'blog']) }}"><span class="glyphicon glyphicon-picture"></span> Galería</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                    </table>
                </div>
            </div>

            <div align="center">{!!$list->render()!!}</div>

        @else
            <h4>No hay resultados</h4>
        @endif

    </div>

@stop