<div class="container-fluid">
    {!!Form::open(['url' => route('users.filter'),'class'=>'form form-vertical','role'=>'form'])!!}
        <div class="form-group col-sm-3">
            {!!Form::label('keyword','Buscar')!!}
            {!!Form::text('keyword', $filters['keyword'], ['placeholder'=>'Nombre de usuario','class'=>'form-control'])!!}
        </div>
        <div class="form-group col-sm-3">
            {!!Form::label('role_id','Tipo Usuario')!!}
            {!!Form::select('role_id', ['' => 'Seleccionar'] + $extra['roles'], $filters['role_id'],['class'=>'form-control'])!!}
        </div>
        <div class="form-group col-sm-3">
            {!!Form::label('active','Estatus')!!}
            {!!Form::select('active', ['' => 'Seleccionar', 'si' => 'Habilitado', 'no' => 'Bloqueado'], $filters['active'],['class'=>'form-control'])!!}
        </div>
        <div class="form-group col-sm-3">
            {!!Form::label('items','Mostrar')!!}
            {!!Form::select('items',['5'=>'5','10'=>'10','20'=>'20','50'=>'50'], $filters['items'], ['class'=>'form-control'])!!}
        </div>
        <div class="checkbox col-sm-12">
            <label>{!!Form::checkbox('inverse','1', $filters['inverse'])!!} Orden Inverso</label>
        </div>
        <div class="text-left form-group btn-group col-sm-12">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Filtrar</button>
            <a class="btn btn-warning" href="{{route('users.filter')}}"><i class="glyphicon glyphicon-refresh"></i> Limpiar Filtros</a>
        </div>
    {!!Form::close()!!}
</div>