@extends('Backend.Template.layout')

@section('title') {{$title}} @stop

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                {!! $message !!}

                <ul class="nav nav-tabs nav-tabs-form">
                    <li class="active"><a href="#"><h4><span class="glyphicon glyphicon-list-alt"></span> {{ $title }}</h4></a></li>
                </ul>

                <div class="panel panel-default panel-form">
                    <div class="panel-body">
                        {!!Form::Open(['url' => route('users.update',$id), 'files' => true, 'class' => "form-horizontal", 'role'=>'form'])!!}
                        <div class="form-group">
                            {!!Form::Label('name','Nombre:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                {!!Form::text('name',@$item->name,['class'=>'form-control'])!!}
                                {!!$errors->first('name','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('email','Correo electrónico:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                {!!Form::email('email',@$item->email,['class'=>'form-control'])!!}
                                {!!$errors->first('email','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('password','Contraseña:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                {!!Form::password('password', ['class'=>'form-control'])!!}
                                {!!$errors->first('password','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('password_confirmation','Confirmar contraseña:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                {!!Form::password('password_confirmation', ['class'=>'form-control'])!!}
                                {!!$errors->first('password_confirmation','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('role_id','Tipo de usuario:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                {!!Form::select('role_id', ['' => 'Seleccionar'] + $roles, @$item->role_id, ['class'=>'form-control user-role'])!!}
                                {!!$errors->first('role_id','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('active', 'Visible:',['class'=>'control-label col-md-3'])!!}
                            <div class="col-md-8">
                                {!!Form::checkbox('active', '1', !empty($item->active))!!}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group user-categories">
                            {!!Form::Label('categories','Asignar categorías:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                <label class="label label-primary">Para seleccinoar multiples opciones, debes mantener presionada la tecla Ctrl y dar click en cada opción.</label>
                                @include('Backend.Template.multicategory', ['list' => $categories, 'values' => @$item->categories])
                                {!!$errors->first('categories','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group text-center">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                <a href="{{ route('users') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Regresar</a>
                            </div>
                        </div>
                        {!!Form::Close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop