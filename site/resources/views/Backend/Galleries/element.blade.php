
    <div class="thumbnail">
        <img src="{{asset($gallery->directory.'/'.$element->value)}}" alt="imagen galeria"  />
        <div class="caption">
            {!!Form::open(['route'=>['gallery.elemento.actualizar',$element->id],'files'=>true,'role'=>'form'])!!}
            <div class="form-group">
                {!!Form::label('title','Título',['class'=>'control-label'])!!}
                {!!Form::text('title',$element->title,['class'=>'form-control'])!!}
            </div>
            {{--
            <div class="form-group">
                {!!Form::label('text','Descripción',['class'=>'control-label'])!!}
                {!!Form::text('subtitle',$element->subtitle, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group">
                {!!Form::label('link_href','Liga del link',['class'=>'control-label'])!!}
                {!!Form::text('link_href',$element->link_href,['class'=>'form-control'])!!}
            </div>
            --}}
            <div class="btn-group btn-group-justified">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default">
                        {!!Form::CheckBox('',$element->id, false, ['class'=>'multiselect'])!!}<span class="glyphicon glyphicon-ok"></span>
                    </label>
                </div>
                <div class="btn-group">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span></button>
                </div>
                <div class="btn-group">
                    <a class="btn btn-danger tobe_eliminated" href="{{route('gallery.elemento.eliminar',$element->id)}}"><span class="glyphicon glyphicon-remove-sign"></span></a>
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>