@extends('Backend.Template.layout')

@section('content')

    <div class="container-fluid">
        <div class="row" id="dashboard">
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-warning"><strong><span class="glyphicon glyphicon-warning-sign"></span> ¡Cuidado!,</strong> No es posible mostrar la sección.</div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2>¡Hola {{ Auth::user()->name }}!</h2>

                        <hr />

                        <p>Lo sentimos mucho, pero al parecer, no cuentas con los permisos suficientes para acceder a está sección.</p>

                        <hr />

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
