@extends('Backend.Template.layout')

@section('title') Anuncios Clasificados @stop

@section('content')

    <div class="container">
        <h1>Anuncios Clasificados </h1>

        <hr />

        {!! $filters !!}

        <hr/>

        {!! $message !!}

        {!! $warning !!}

        @if(!$list->isEmpty())
            Total: {{$list->total()}}
            {!!Form::Open(['class'=>'form','id'=>'form-multi-select'])!!}
                <div class="text-left form-group">
                    <label class="control-label">Elementos Seleccionados: </label>
                    <a data-href="{{route('anuncios.status', 'active')}}" data-action="CAMBIAR ESTATUS" class="btn btn-info task-multi-select" id="visibility"><span class="glyphicon glyphicon-eye-open"></span> Cambiar Estatus</a>
                    <a data-href="{{route('anuncios.delete')}}" data-action="ELIMINAR" class="btn btn-danger task-multi-select" id="delete"><i class="glyphicon glyphicon-remove-sign"></i> Eliminar</a>
                </div>
            {!!Form::close()!!}

            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><input type="checkbox" class="multi-select-all"> </th>
                        <th>Titulo</th>
                        <th>Description</th>
                        <th>Usuario</th>
                        <th>Categoria</th>
                        <th>Visible</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($list as $item)
                        <tr class="allRow" data-item="{{ $item->id }}">
                            <td><input type="checkbox" class="select-checkbox" name="selected[]" value="{{$item->id}}" /></td>
                            <td><label class="editQuit" data-state="name">{{ $item->title }}</label></td>
                            <td><label class="editQuit" data-state="title">{{ $item->description }}</label></td>
                            <td><label class="editQuit" data-state="name">{{ $item->user }}</label></td>
                            <td><label class="editQuit" data-state="name">{{ $item->category->name }}</label></td>
                            <td>{!! \Backend::active($item->active) !!}</td>
                            <td><a class="btn btn-primary" href="{{route('anuncios.edit',$item->id)}}"><span class="glyphicon glyphicon-edit"></span> Detalles</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
                </div>
            </div>
            {!!$list->render()!!}
        @else
            <h2>No hay resultados</h2>
        @endif
    </div>

@stop