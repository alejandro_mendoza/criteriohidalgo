@extends('Backend.Template.layout')

@section('title') Anuncios Clasificados @stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! $message !!}
                <div class="panel panel-default">
                    <div class="panel-heading orange"><h4><span class="glyphicon glyphicon-list-alt"></span> {{ $title }} </h4></div>
                    <div class="panel-body">

                        {!!Form::Open(['url' => route('edicion.update',$id), 'files' => true, 'class' => "form-horizontal", 'role'=>'form'])!!}
                            <div class="form-group">
                                {!!Form::Label('name','Titulo:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('title',@$item->title,['class'=>'form-control','readonly'])!!}
                                    {!!$errors->first('title','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            
                            <div class="form-group">
				{!!Form::Label('photo','Imagen:',['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-8">
				@if(!empty($item->image))
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                                <img src="{{ asset("/images/ads/{$item->image}") }}" class="img-thumbnail" />
                                        </div>
                                    </div>
				@endif
				</div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('description','Descripcion:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::textarea('description',@$item->description,['class'=>'form-control','readonly'])!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('user','Usuario:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('user',@$item->user,['class'=>'form-control','readonly'])!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('phone','Telefono:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('phone',@$item->phone,['class'=>'form-control','readonly'])!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('mail','Correo:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('mail',@$item->mail,['class'=>'form-control','readonly'])!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('category','Categoria:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('category',@$item->category->name,['class'=>'form-control','readonly'])!!}
                                </div>
                            </div>
                             <div class="form-group">
                                {!!Form::Label('active', 'Visible:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                {!!Form::checkbox('active', '1', !empty($item->active),['readonly'])!!}
                                </div>
                             </div>
                            <div class="form-group text-center">
                                <div class="btn-group">
                                    <a href="{{ route('anuncios') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Regresar</a>
                                </div>
                            </div>
                        {!!Form::Close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop