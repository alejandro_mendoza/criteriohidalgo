@extends('Backend.Template.layout')

@section('title') Categorías @stop

@section('content')

    <div class="container panel panel-default">
        <h2>Categorías</h2>

        <div class="form-group">
            <a class="btn btn-primary" href="{{ route('categories.edit') }}">
                <span class="glyphicon glyphicon-plus"></span> Nueva Subcategoría
            </a>
        </div>

        <hr />

        {!! $filters !!}

        <hr/>

        {!! $message !!}

        {!! $warning !!}

        @if(!$list->isEmpty())
            Total: {{$list->total()}}
            {!!Form::Open(['class' => 'form', 'id' => 'form-multi-select'])!!}
                <div class="text-left form-group">
                    <label class="control-label">Elementos Seleccionados: </label>
                    <a data-href="{{route('categories.active')}}" data-action="CAMBIAR ESTATUS" class="btn btn-info task-multi-select" id="visibility"><span class="glyphicon glyphicon-eye-open"></span> Cambiar Estatus</a>
                    <a data-href="{{route('categories.delete')}}" data-action="ELIMINAR" class="btn btn-danger task-multi-select" id="delete"><i class="glyphicon glyphicon-remove-sign"></i> Eliminar</a>
                </div>
            {!!Form::close()!!}

            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th><input type="checkbox" class="multi-select-all"></th>
                                <th>Título</th>
                                <th>Categoría</th>
                                <th>Visible</th>
                                <th>Color</th>
                                <th>Creado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                    @foreach($list as $item)
                        <tr>
                            <td><input type="checkbox" class="select-checkbox" name="selected[]" value="{{$item->id}}" /></td>
                            <td>{{ $item->title }}</td>
                            <td>{{ @$item->parent->title }}</td>
                            <td>{!! \Backend::active($item->active) !!}</td>
                            <td>{!! \Backend::color($item->color) !!}</td>
                            <td>{{ \Backend::dateFormat($item->created_at) }}</td>
                            <td>
                                <a class="btn btn-primary" href="{{route('categories.edit', $item->id)}}"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                <a class="btn btn-default" href="{{ route('module.list', $item->id) }}"><span class="glyphicon glyphicon-th-large"></span> Módulos</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                    </table>
                </div>
            </div>

            <div align="center">{!!$list->render()!!}</div>

        @else
            <h4>No hay resultados</h4>
        @endif

    </div>

@stop