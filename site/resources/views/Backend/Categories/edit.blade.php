@extends('Backend.Template.layout')

@section('title') {{$title}} @stop

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                {!! $message !!}

                <ul class="nav nav-tabs nav-tabs-form">
                    <li class="active"><a href="#"><h4><span class="glyphicon glyphicon-list-alt"></span> {{ $title }}</h4></a></li>
                    @if(@$item->exists)
                    <li><a href="{{ route('module.list', $id) }}"><h4><span class="glyphicon glyphicon-th-large"></span> Módulos</h4></a></li>
                    @endif
                </ul>

                <div class="panel panel-default panel-form">
                    <div class="panel-body">
                        {!!Form::Open(['url' => route('categories.update', $id), 'files' => true, 'class' => "form-horizontal", 'role'=>'form'])!!}
                            <div class="form-group">
                                {!!Form::Label('title', 'Título:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('title', @$item->title,['class'=>'form-control'])!!}
                                    {!!$errors->first('title','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            @if(empty($id) || !is_null($item->parent_id))
                            <div class="form-group">
                                {!!Form::Label('parent_id', 'Categoría:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::select('parent_id', ['' => 'Seleccionar'] + $sections, @$item->parent_id, ['class'=>'form-control'])!!}
                                    {!!$errors->first('parent_id','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            @else
                                {!! Form::hidden('parent_id', 'root') !!}
                            @endif
                            <div class="form-group">
                                {!!Form::Label('color','Color:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    <div class="input-group colorPicker">
                                        {!!Form::text('color', @$item->color,['class'=>'form-control'])!!}
                                        <span class="input-group-addon"><i></i> <span class="glyphicon glyphicon-tint"></span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('image','Logo:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8 image-container">
                                   <span class="label label-primary">Medidas recomendadas de 200 X 50</span>
                                    @if(!empty($item->image))
                                        <div class="thumbnail show">
                                            <img src="{{asset("images/categories/{$item->image}")}}" />
                                        </div>
                                    @else
                                        <div class="thumbnail">
                                            <img src="" />
                                        </div>
                                    @endif
                                    {!!Form::file('file', ['class'=>'form-control'])!!}
                                    {!!$errors->first('file','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('active', 'Visible:',['class'=>'control-label col-md-3'])!!}
                                <div class="col-md-8">
                                    {!!Form::checkbox('active', '1', !empty($item->active))!!}
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                {!!Form::Label('description', 'Meta descripción:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::textarea('description', @$item->description, ['class' => 'form-control', 'rows' => 2])!!}
                                    {!!$errors->first('description','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            
                            <div class="form-group">
                                {!!Form::Label('banner1', 'Primer banner:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    <?php $b1 = !empty($item->params) ? json_decode($item->params) : null; ?>
                                    {!!Form::textarea('banner1', @base64_decode($b1[0]->one), ['class' => 'form-control', 'rows' => 2])!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('banner2', 'Segundo banner:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::textarea('banner2', @base64_decode($b1[1]->one), ['class' => 'form-control', 'rows' => 2])!!}
                                </div>
                            </div>
                            
                            @if($item->slug == 'home')
                                <div class="form-group">
                                    {!!Form::Label('banner3', 'Tercer banner:',['class'=>'col-md-3 control-label'])!!}
                                    <div class="col-md-8">
                                        {!!Form::textarea('banner3', @base64_decode($b1[2]->one), ['class' => 'form-control', 'rows' => 2])!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::Label('banner4', 'Cuarto banner:',['class'=>'col-md-3 control-label'])!!}
                                    <div class="col-md-8">
                                        {!!Form::textarea('banner4', @base64_decode($b1[3]->one), ['class' => 'form-control', 'rows' => 2])!!}
                                    </div>
                                </div>
                            @endif
                            
                            <hr>
                            <div class="form-group text-center">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                    <a href="{{ route('categories') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Regresar</a>
                                </div>
                            </div>
                        {!!Form::Close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop