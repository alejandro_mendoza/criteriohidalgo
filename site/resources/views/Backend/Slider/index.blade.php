@extends('Backend.Template.layout')

@section('title') Slider @stop

@section('content')

    <div class="container">
        <h1>Slider</h1>
        {!!Form::Open(['class'=>'form','id' => 'form-multi-select'])!!}
        <div class="form-inline">  
            <label>Selecciona una plantilla</label>
            {!! Form::select('selectSlider', [
                '' => 'Ninguno',
                '1' => 'Plantilla 1',
                '2' => 'Plantilla 2',
                '3' => 'Plantilla 3',
                '4' => 'Plantilla 4'],
                @$gallery[0]->directory,
                ['id' => "selectSlider",'class'=>'form-control']
             ) !!}
             <a id="showSlider" class="btn btn-success"><span class="glyphicon glyphicon-triangle-bottom"></span>Ver</a>
 
            <div id="renderExample">
                <img class="img-thumbnail hidden" id="img-demo" src="">
                @if(!count($gallery) > 0)
                    <a id="addSlider" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>Agregar</a>
                @endif 
            </div>
             <p>Instrucciones:</p>
             <p>1.- Selecciona una plantilla</p>
             <p>2.- Da doble clic sobre la imagen y copia la URL de la noticia</p>
             <p>Ej. http://www.criteriolocal.euac/notas/deportes/nueva-noticia-demo</p>
        </div>
        <br>
        <h3 id="type"> Plantilla {{ @$gallery[0]->directory }}</h3>
        <input type="checkbox" class="multi-select-all-s">Seleccionar todo
        <a data-href="{{route('slider.delete',1)}}" id="MultiDelete" class="btn btn-mini btn-danger"><i class="glyphicon glyphicon-remove-sign"></i> Eliminar</a>
        <br>
        <div id="renderSliderMain" class="col-md-7">
            <div id="msjSuccess" class="alert alert-success" role="alert" style="display: none"><strong>Slider agregado correctamente</strong></div>
            <div id="msjError" class="alert alert-danger" role="alert" style="display: none"></div>
            <div id="sortable" >
                @foreach($gallery as $slider)
                    <div id="{{ $slider->id }}" class="sortable-ch alert alert-info">@include('Backend.Slider.plantillas.demo'.$slider->directory,$slider)</div>
                @endforeach
            </div>
            <div id="toSource" class="form-group has-success" data-source="1">
                <input type="text" id="inputSuccess2" class="form-control" >
                <input type="hidden" value="{{ @$gallery[0]->directory }}" id="typeSlider" >
            </div>
        </div>

        {!!Form::close()!!}
    </div>


@stop