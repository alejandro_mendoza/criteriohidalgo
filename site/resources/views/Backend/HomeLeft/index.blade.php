@extends('Backend.Template.layout')

@section('title') Home @stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading orange"><h4><span class="glyphicon glyphicon-list-alt"></span> Home</h4></div>
                    <div class="panel-body">
                        <?php $x = 1; ?>
                        @foreach($list as $item)
                        {!!Form::Open(['url' => route('homeLeft.update',$item->id), 'files' => true, 'class' => "form-horizontal", 'role'=>'form'])!!}
                            <div class="form-group">
                                <label class="col-md-3 control-label">Bloque {{ $x }}</label>
                                <div class="col-md-8">
                                    {!!Form::select('category_id', ['' => 'Seleccionar'] + $categories, @$item->category_id, ['class'=>'form-control'])!!}
                                    {!!$errors->first('category_id','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                </div>
                            </div>
                        <?php $x++; ?>
                        {!!Form::Close()!!}
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop