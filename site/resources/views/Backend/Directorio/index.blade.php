@extends('Backend.Template.layout')

@section('title') Directorio @stop

@section('content')

    <div class="container">
        <h1>Directorio</h1>

        <div class="form-group">
            <a class="btn btn-primary" href="{{ route('directorio.edit') }}">
                <span class="glyphicon glyphicon-plus"></span> Nuevo Miembro
            </a>
        </div>

        <hr />

        {!! $filters !!}

        <hr/>

        {!! $message !!}

        @if(!$list->isEmpty())
            Total: {{$list->total()}}
            {!!Form::Open(['class'=>'form','id'=>'form-multi-select'])!!}
                <div class="text-left form-group">
                    <label class="control-label">Elementos Seleccionados: </label>
                    <a data-href="{{route('directorio.delete')}}" data-action="ELIMINAR" class="btn btn-danger task-multi-select" id="delete"><i class="glyphicon glyphicon-remove-sign"></i> Eliminar</a>
                </div>
            {!!Form::close()!!}

            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><input type="checkbox" class="multi-select-all"> </th>
                        <th>Nombre</th>
                        <th>Titulo</th>
                        <th>Correo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($list as $item)
                        <tr class="allRow" data-item="{{ $item->id }}">
                            <td><input type="checkbox" class="select-checkbox" name="selected[]" value="{{$item->id}}" /></td>
                            <td><label class="editQuit" data-state="name">{{ $item->name }}</label></td>
                            <td><label class="editQuit" data-state="title">{{ $item->title }}</label></td>
                            <td>{{ $item->mail }}</td>
                            <td><a class="btn btn-primary" href="{{route('directorio.edit',$item->id)}}"><span class="glyphicon glyphicon-edit"></span> Editar</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
                </div>
            </div>
            {!!$list->render()!!}
        @else
            <h2>No hay resultados</h2>
        @endif
    </div>

@stop