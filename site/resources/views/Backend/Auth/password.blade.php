@extends('Backend.Template.layout')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Recuperar Contraseña</h1>
			<div class="panel panel-default">
				<div class="panel-body">
					{!! Form::open(['route' => 'user.reset', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form']) !!}
						<div class="form-group">
							<label class="col-md-3 control-label">Correo Electrónico</label>
							<div class="col-md-offset-1 col-md-7">
								{!! Form::text('email', null,['class'=>"form-control"]) !!}
							</div>
						</div>

						<hr>

						<div class="form-group" align="center">
						    <div class="btn-group">
                                <button type="submit" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-envelope"></span> Enviar
                                </button>
                                <a href="{{ url('mcPanel') }}" class="btn btn-default">
                                    <span class="glyphicon glyphicon-arrow-left"></span> {{ Lang::get('validation.attributes.back') }}
                                </a>
                            </div>
						</div>

					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
