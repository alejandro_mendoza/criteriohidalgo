@extends('Backend.Template.layout')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Acceso al Administrador</h1>
			<div class="panel panel-default">
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('user.access') }}">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}">

						<div class="form-group">
							<label class="col-md-3 control-label">{{ Lang::get('validation.attributes.email') }}</label>
							<div class="col-md-offset-1 col-md-7">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">{{ Lang::get('validation.attributes.password') }}</label>
							<div class="col-md-offset-1 col-md-7">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<hr />

						<div class="form-group">
							<div class="col-md-12 logInActions">
								<label>
									<!--<input type="checkbox" name="remember"> {{ Lang::get('validation.attributes.remember') }}-->
									<input type="checkbox" name="remember"> Recuérdame
								</label>
								
								<div>
									<button type="submit" class="btn btn-primary">
										<span class="glyphicon glyphicon-off"></span> Login
									</button>
									<a href="{{ route('user.recovery') }}">{{ Lang::get('validation.attributes.forgot_password') }}</a>
								</div>

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
