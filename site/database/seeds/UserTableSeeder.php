<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id'   => 1,
            'name'      => 'Eric',
            'email'     => 'eric.guerrero@masclicks.com.mx',
            'password'  => bcrypt('root'),
        ]);
        DB::table('users')->insert([
            'role_id'   => 1,
            'name'      => 'Uzziel',
            'email'     => 'uzziel.alonso@masclicks.com.mx',
            'password'  => bcrypt('root'),
        ]);
    }
}
