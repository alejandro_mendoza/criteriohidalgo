<?php

use Illuminate\Database\Seeder;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            [
                'id'    => 1,
                'name'  => 'Anuncios Clasificados',
                'slug'  => 'anuncios-clasificados'
            ],
            [
                'id'    => 2,
                'name'  => 'Banner',
                'slug'  => 'banner'
            ],
            [
                'id'    => 3,
                'name'  => 'Columna',
                'slug'  => 'columna'
            ],
            [
                'id'    => 4,
                'name'  => 'Edición Impresa',
                'slug'  => 'edicion-impresa'
            ],
            [
                'id'    => 5,
                'name'  => 'Encuesta',
                'slug'  => 'encuesta'
            ],
            [
                'id'    => 6,
                'name'  => 'Formulario de Contacto',
                'slug'  => 'formulario-contacto'
            ],
            [
                'id'    => 7,
                'name'  => 'Galería de Imágenes',
                'slug'  => 'galeria-imagenes'
            ],
            [
                'id'    => 8,
                'name'  => 'Minuto a Minuto',
                'slug'  => 'minuto-minuto'
            ],
            [
                'id'    => 9,
                'name'  => 'Notas Recientes',
                'slug'  => 'notas-recientes'
            ],
            [
                'id'    => 10,
                'name'  => 'Notas Más Vistas',
                'slug'  => 'mas-vistas'
            ],
        ]);
    }

}
