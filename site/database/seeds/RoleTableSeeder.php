<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'id'    => 1,
                'name'  => 'Super Administrador',
                'type'  => 'SuperAdmin'
            ],
            [
                'id'    => 2,
                'name'  => 'Administrador',
                'type'  => 'Admin'
            ],
            [
                'id'    => 3,
                'name'  => 'Editor',
                'type'  => 'Editor'
            ]
        ]);
    }
}
