<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Http\Entities\Classified;
use App\Http\Entities\Sub_Classified;
use App\Http\Entities\Module;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ClassifiedTableSeeder::class);
        $this->call(BlockLeftTableSeeder::class);

        Model::reguard();
    }

}

class BlockLeftTableSeeder extends Seedes{
    public function run(){
        Module::create([
            'category_id'  => '10',
            'params'     => '12',
            'type_slug'    => 'blockLeft',
            'type_id'      => '1',
            'visible'      => '1',
        ]);
        Module::create([
            'category_id'  => '37',
            'params'     => '7',
            'type_slug'    => 'blockLeft',
            'type_id'      => '1',
            'visible'      => '1',
        ]);
        Module::create([
            'category_id'  => '41',
            'params'     => '3',
            'type_slug'    => 'blockLeft',
            'type_id'      => '1',
            'visible'      => '1',
        ]);
        Module::create([
            'category_id'  => '42',
            'params'     => '15',
            'type_slug'    => 'blockLeft',
            'type_id'      => '1',
            'visible'      => '1',
        ]);
    }
}

class ClassifiedTableSeeder extends Seeder {
    public function run() {
        $ads = Classified::create([
            'name'      => 'Inmuebles',
            'slug'     => 'inmuebles',
        ]);
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Casa / Departamento (Venta)',
            'slug' => 'casa-departamento-venta',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Terrenos (Venta)',
            'slug' => 'terrenos-venta',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Locales / Bodegas (Venta)',
            'slug' => 'locales-bodegas-venta',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Casa / Departamento (Renta)',
            'slug' => 'casa-departamento-renta',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Terrenos (Renta)',
            'slug' => 'terrenos-renta',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Locales / Bodegas (Renta)',
            'slug' => 'locales-bodegas-renta',
            'category_id'  => $ads->id
        ]));

        $ads = Classified::create([
            'name'      => 'Empleos',
            'slug'     => 'empleos',
        ]);
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Profesionista',
            'slug' => 'profesionista',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Administrativo / Oficina',
            'slug' => 'administrativo-oficina',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Operativo / Técnico',
            'slug' => 'operativo-tecnico',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Ventas / Comercial',
            'slug' => 'ventas-comercial',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Servicios (Empleos)',
            'slug' => 'servicios-empleos',
            'category_id'  => $ads->id
        ]));

        $ads = Classified::create([
            'name'      => 'Vehiculos',
            'slug'     => 'vehiculos',
        ]);
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Autos',
            'slug' => 'autos',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Camionetas',
            'slug' => 'camionetas',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Motocicletas',
            'slug' => 'motocicletas',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Accesorios y servicios',
            'slug' => 'accesorios-servicios',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Servicios Vehiculos',
            'slug' => 'servicios-vehiculos',
            'category_id'  => $ads->id
        ]));

        $ads = Classified::create([
            'name'      => 'Varios',
            'slug'     => 'varios',
        ]);
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Animales',
            'slug' => 'animales',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Aparatos-electrónicos',
            'slug' => 'aparatos-electronicos',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Educación / Cursos',
            'slug' => 'educacion-cursos',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Maquinaria (Comercio e Industrial)',
            'slug' => 'maquinaria-comercio-industrial',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Materiales para construcción',
            'slug' => 'materiales-construccion',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Muebles',
            'slug' => 'muebles',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Todo para fiestas',
            'slug' => 'todo-fiestas',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Varios',
            'slug' => 'varios',
            'category_id'  => $ads->id
        ]));
        $ads->sub_categories()->save(new Sub_Classified([
            'name'  => 'Oferta de Servicios Varios',
            'slug' => 'oferta-servicios-varios',
            'category_id'  => $ads->id
        ]));
    }
}
