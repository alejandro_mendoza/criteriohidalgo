<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            [
                'id'            => 1,
                'category_id'   => 18,
                'type_id'       => 1,
                'position'      => 1,
                'type_slug'     => 'formulario_contacto',
                'modulable_id'  => 1,
                'modulable_type'=> 'App\Http\Entities\Form'
            ],
            [
                'id'            => 2,
                'category_id'   => 10,
                'type_id'       => 12,
                'position'      => 1,
                'type_slug'     => 'blockLeft',
                'modulable_id'  => null,
                'modulable_type'=> null
            ],
            [
                'id'            => 3,
                'category_id'   => 37,
                'type_id'       => 7,
                'position'      => 1,
                'type_slug'     => 'blockLeft',
                'modulable_id'  => null,
                'modulable_type'=> null
            ],
            [
                'id'            => 4,
                'category_id'   => 41,
                'type_id'       => 3,
                'position'      => 1,
                'type_slug'     => 'blockLeft',
                'modulable_id'  => null,
                'modulable_type'=> null
            ],
            [
                'id'            => 5,
                'category_id'   => 42,
                'type_id'       => 15,
                'position'      => 1,
                'type_slug'     => 'blockLeft',
                'modulable_id'  => null,
                'modulable_type'=> null
            ],
        ]);
    }
}
