<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->engine = 'innoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('image');
            $table->text('description');
            $table->integer('sub_category_id')->nullable()->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('visible');
            $table->date('validity');
            $table->timestamps();

            $table->foreign('sub_category_id')->references('id')->on('c_sub_classified');
            $table->foreign('user_id')->references('id')->on('ads_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads');
    }
}
