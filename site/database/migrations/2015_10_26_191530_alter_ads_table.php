<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads',function(Blueprint $table) {
            $table->engine = 'innoDB';
            $table->string('user')->after('description');
            $table->string('phone')->after('user');
            $table->string('mail')->after('phone');
            $table->string('active')->after('sub_category_id');
            $table->dropForeign('ads_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('visible');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
