<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles',function(Blueprint $table)
        {
            $table->integer('views')->after('free');
        });
    }

    /**?
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
