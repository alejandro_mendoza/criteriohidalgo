<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticsElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('statics_elements', function (Blueprint $table) {
           $table->engine = 'innoDB';
            $table->increments('id');
            $table->integer('static_id')->nullable()->unsigned();
            $table->string('title');
            $table->text('text');
            $table->string('image');
            $table->string('link');
            
            $table->foreign('static_id')->references('id')->on('statics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('statics_elements');
    }
}
