<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Tabla de galerias
        Schema::Create('galleries', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name');
            $table->integer('position');
            $table->boolean('active');
            $table->text('directory');

            // Campos para la relación polimorfica
            $table->integer('imageable_id');
            $table->string('imageable_type');

            $table->timestamps();
        });

        // Tabla de elementos
        Schema::Create('gallery_elements', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('value');
            $table->integer('gallery_id')->unsigned();
            $table->enum('type',['image', 'video']);
            $table->string('title');
            $table->string('subtitle');
            $table->string('link_text');
            $table->string('link_href');
            $table->integer('position');

            // clave foránea
            $table->foreign('gallery_id')->references('id')->on('galleries')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galleries');
        Schema::drop('gallery_elements');
    }
}
