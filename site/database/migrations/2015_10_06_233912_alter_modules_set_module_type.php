<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterModulesSetModuleType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('types',function(Blueprint $table) {
            $table->engine = 'innoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');

            $table->timestamps();
        });

        Schema::table('modules',function(Blueprint $table) {
            $table->engine = 'innoDB';
            $table->integer('type_id')
                    ->unsigned()
                    ->after('category_id');

            $table->foreign('type_id')
                    ->references('id')
                    ->on('types')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('modules',function(Blueprint $table) {
            $table->dropForeign('modules_type_id_foreign');
            $table->dropColumn('type_id');
        });

        Schema::drop('types');
    }
}
