<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //Tabla de módulos
        Schema::create('modules',function(Blueprint $table) {
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('position')->unsigned();
            $table->boolean('visible');
            $table->string('type_slug');
            $table->integer('modulable_id')->unsigned();
            $table->string('modulable_type');
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::Drop('modules');
    }
}
