<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ids',function(Blueprint $table)
        {
            $table->integer('article_id')->before('id')->unsigned();
            $table->integer('idssable_id')->after('article_id')->unsigned();
            $table->string('idssable_type')->after('idssable_id');
            $table->dropColumn('id');
            $table->dropColumn('value');
        });

        Schema::drop('idssables');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
