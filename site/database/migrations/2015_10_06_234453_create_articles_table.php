<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Create('articles',function(Blueprint $table)
        {
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('author_id')->unsigned()->nullable();
            $table->string('title');
            $table->mediumText('intro');
            $table->text('content');
            $table->string('slug');
            $table->string('issuu', 750);
            $table->mediumText('description');
            $table->integer('position')->unsigned();
            $table->boolean('active')->default(1);
            $table->boolean('author')->default(0);
            $table->boolean('principal')->default(0);
            $table->boolean('featuring')->default(0);
            $table->boolean('free')->default(1);

            $table->date('date_publishing');
            $table->date('date_expire');

            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('author_id')->references('id')->on('authors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
